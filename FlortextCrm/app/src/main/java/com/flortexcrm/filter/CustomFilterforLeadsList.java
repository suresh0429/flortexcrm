package com.flortexcrm.filter;

import android.widget.Filter;

import com.flortexcrm.Response.LeadsResponse;
import com.flortexcrm.adapter.AllLeadsAdapter;

import java.util.ArrayList;

public class CustomFilterforLeadsList extends Filter {

    AllLeadsAdapter adapter;
    ArrayList<LeadsResponse.DataBean> filterList;

    public CustomFilterforLeadsList(ArrayList<LeadsResponse.DataBean> filterList, AllLeadsAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<LeadsResponse.DataBean> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++)
            {
                if (filterList.get(i).getLead_code().toUpperCase().contains(constraint)
                        || filterList.get(i).getStatus().toUpperCase().contains(constraint)
                        || filterList.get(i).getCustomer_name().toUpperCase().contains(constraint)
                        || filterList.get(i).getSubject().toUpperCase().contains(constraint)
                        || filterList.get(i).getCreated_by_employee_name().toUpperCase().contains(constraint)
                        || filterList.get(i).getSource().toUpperCase().contains(constraint)
                        || filterList.get(i).getIndustry().toUpperCase().contains(constraint)
                        || filterList.get(i).getRating().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.allLeadslistModels = (ArrayList<LeadsResponse.DataBean>) results.values;
        adapter.notifyDataSetChanged();
    }
}

