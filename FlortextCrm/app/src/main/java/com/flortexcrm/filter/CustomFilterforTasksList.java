package com.flortexcrm.filter;

import android.widget.Filter;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.adapter.AllTaskAdapter;

import java.util.ArrayList;

public class CustomFilterforTasksList extends Filter {

    AllTaskAdapter adapter;
    ArrayList<TaskResponse.DataBean> filterList;

    public CustomFilterforTasksList(ArrayList<TaskResponse.DataBean> filterList, AllTaskAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<TaskResponse.DataBean> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++)
            {
                if (filterList.get(i).getLeadCode().toUpperCase().contains(constraint)
                        || filterList.get(i).getStatus().toUpperCase().contains(constraint)
                        || filterList.get(i).getTaskSubject().toUpperCase().contains(constraint)
                        || filterList.get(i).getTaskDetails().toUpperCase().contains(constraint)
                        || filterList.get(i).getPriority().toUpperCase().contains(constraint)
                        || filterList.get(i).getEmployeeName().toUpperCase().contains(constraint)
                        || filterList.get(i).getType().toUpperCase().contains(constraint)
                        || filterList.get(i).getCreatedOn().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.allTasklistModels = (ArrayList<TaskResponse.DataBean>) results.values;
        adapter.notifyDataSetChanged();
    }
}

