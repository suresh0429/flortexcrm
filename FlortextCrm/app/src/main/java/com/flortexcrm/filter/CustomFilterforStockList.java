package com.flortexcrm.filter;

import android.widget.Filter;

import com.flortexcrm.Response.StockResponse;
import com.flortexcrm.adapter.AllStocksAdapter;

import java.util.ArrayList;
public class CustomFilterforStockList extends Filter {

    AllStocksAdapter adapter;
    ArrayList<StockResponse.DataBean> filterList;

    public CustomFilterforStockList(ArrayList<StockResponse.DataBean> filterList, AllStocksAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<StockResponse.DataBean> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++)
            {
                if (filterList.get(i).getProduct_name().toUpperCase().contains(constraint)
                     || filterList.get(i).getGroupName().toUpperCase().contains(constraint)
                        || filterList.get(i).getBalance_qty().toUpperCase().contains(constraint)
                        || filterList.get(i).getPendingSOQty().toUpperCase().contains(constraint)
                        || filterList.get(i).getPendingPOQty().toUpperCase().contains(constraint)
                        || filterList.get(i).getBlockQty().toUpperCase().contains(constraint)
                        || filterList.get(i).getTotalQty().toUpperCase().contains(constraint)
                        || filterList.get(i).getTotal().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.allStockslistModels = (ArrayList<StockResponse.DataBean>) results.values;
        adapter.notifyDataSetChanged();
    }
}
