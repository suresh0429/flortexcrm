package com.flortexcrm.filter;

import android.widget.Filter;
import com.flortexcrm.adapter.FilterCustomerAdapter;
import com.flortexcrm.model.CustomerModel;
import java.util.ArrayList;

public class FilterForCustomerList extends Filter {

    FilterCustomerAdapter adapter;
    ArrayList<CustomerModel> filterList;

    public FilterForCustomerList(ArrayList<CustomerModel> filterList, FilterCustomerAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CustomerModel> filteredPlayers=new ArrayList<CustomerModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.spinnerModels = (ArrayList<CustomerModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
