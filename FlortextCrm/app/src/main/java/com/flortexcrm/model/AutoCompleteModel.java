package com.flortexcrm.model;

public class AutoCompleteModel {
    private String id;
    private String name;
    private String packing;

    public AutoCompleteModel(String id, String name, String packing) {
        this.id = id;
        this.name = name;
        this.packing = packing;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }
}
