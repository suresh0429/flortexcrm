package com.flortexcrm.model;

public class CalendarModel {

    String event_data;
    String t_id;
    String updatedDate;
    String status;

    public CalendarModel(String event_data, String t_id, String updatedDate, String status) {
        this.event_data = event_data;
        this.t_id = t_id;
        this.updatedDate = updatedDate;
        this.status = status;
    }

    public String getEvent_data() {
        return event_data;
    }

    public void setEvent_data(String event_data) {
        this.event_data = event_data;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
