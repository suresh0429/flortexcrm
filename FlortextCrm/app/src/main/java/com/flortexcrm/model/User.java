package com.flortexcrm.model;

import org.json.JSONObject;

public class User {

    public String userId, name, email, mobile;

    public User(JSONObject jsonObject) {
        try {
            if (jsonObject.has("user_id") || !jsonObject.isNull("user_id"))
                this.userId = jsonObject.getString("user_id");
            if (jsonObject.has("user_name") || !jsonObject.isNull("user_name"))
                this.name = jsonObject.getString("user_name");
            if (jsonObject.has("email") || !jsonObject.isNull("email"))
                this.email = jsonObject.getString("email");
            if (jsonObject.has("mobile") || !jsonObject.isNull("mobile"))
                this.mobile = jsonObject.getString("mobile");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
