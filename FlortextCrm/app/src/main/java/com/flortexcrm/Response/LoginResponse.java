package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NTQ4OTAzNjIsImp0aSI6IlRVMDNUWEpYV2paeVUyVTRTMmxIYURkdFlWRjRWRWQxVTBoNU4wMXFOVXc9IiwiaXNzIjoiaHR0cDpcL1wvZmxvcnRleC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU1NDg5MDM2MywiZXhwIjoxNTU3NDgyMzYzLCJkYXRhIjp7InVzZXJfaWQiOiIyIiwidXNlcl9uYW1lIjoiVmlqYXkiLCJlbWFpbCI6InZpamF5QGlubmFzb2Z0LmluIiwibW9iaWxlIjoiODAxOTQ0ODA5OCIsInNlc3Npb25faWQiOiJUVTAzVFhKWFdqWnlVMlU0UzJsSGFEZHRZVkY0VkVkMVUwaDVOMDFxTlV3PSJ9fQ.143-LKTZQozf4Kk9mpoR2nGUvqCP87pgMSiamv7S6YRZ0RlnHRhfMF-vBWYxlEn0jf38hvuEpAr3O3N9V7Lxeg","user_id":"2","user_name":"Vijay","email":"vijay@innasoft.in","mobile":"8019448098"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NTQ4OTAzNjIsImp0aSI6IlRVMDNUWEpYV2paeVUyVTRTMmxIYURkdFlWRjRWRWQxVTBoNU4wMXFOVXc9IiwiaXNzIjoiaHR0cDpcL1wvZmxvcnRleC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU1NDg5MDM2MywiZXhwIjoxNTU3NDgyMzYzLCJkYXRhIjp7InVzZXJfaWQiOiIyIiwidXNlcl9uYW1lIjoiVmlqYXkiLCJlbWFpbCI6InZpamF5QGlubmFzb2Z0LmluIiwibW9iaWxlIjoiODAxOTQ0ODA5OCIsInNlc3Npb25faWQiOiJUVTAzVFhKWFdqWnlVMlU0UzJsSGFEZHRZVkY0VkVkMVUwaDVOMDFxTlV3PSJ9fQ.143-LKTZQozf4Kk9mpoR2nGUvqCP87pgMSiamv7S6YRZ0RlnHRhfMF-vBWYxlEn0jf38hvuEpAr3O3N9V7Lxeg
         * user_id : 2
         * user_name : Vijay
         * email : vijay@innasoft.in
         * mobile : 8019448098
         */

        @SerializedName("jwt")
        private String jwt;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile")
        private String mobile;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
