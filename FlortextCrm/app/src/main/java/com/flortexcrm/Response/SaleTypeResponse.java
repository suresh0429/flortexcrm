package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaleTypeResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"1","name":"B2C LARGE","created_on":"2018-11-26 16:35:53","updated_on":"0000-00-00 00:00:00","is_active":"1"},{"id":"2","name":"B2C SMALL","created_on":"2018-11-26 16:35:53","updated_on":"0000-00-00 00:00:00","is_active":"1"},{"id":"3","name":"FLORTEX SALE","created_on":"2018-11-26 16:35:53","updated_on":"0000-00-00 00:00:00","is_active":"1"},{"id":"4","name":"INTER STATE B2B","created_on":"2018-11-26 16:35:53","updated_on":"0000-00-00 00:00:00","is_active":"1"},{"id":"5","name":"INTERA STATE B2B","created_on":"2018-11-26 16:35:53","updated_on":"0000-00-00 00:00:00","is_active":"1"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : B2C LARGE
         * created_on : 2018-11-26 16:35:53
         * updated_on : 0000-00-00 00:00:00
         * is_active : 1
         */

        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("is_active")
        private String isActive;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }
    }
}
