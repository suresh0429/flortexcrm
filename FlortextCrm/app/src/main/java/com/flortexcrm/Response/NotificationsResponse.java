package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"718","task_id":"213","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/213","is_read":"0","created_on":"2019-04-19 15:08:57","display_text":"assigned new task to you"},{"id":"710","task_id":"212","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/212","is_read":"0","created_on":"2019-04-19 15:07:49","display_text":"assigned new task to you"},{"id":"702","task_id":"211","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/211","is_read":"0","created_on":"2019-04-19 15:01:46","display_text":"assigned new task to you"},{"id":"694","task_id":"210","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/210","is_read":"0","created_on":"2019-04-19 15:01:44","display_text":"assigned new task to you"},{"id":"686","task_id":"209","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/209","is_read":"0","created_on":"2019-04-19 15:01:38","display_text":"assigned new task to you"},{"id":"633","task_id":"204","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/204","is_read":"0","created_on":"2019-04-19 14:32:33","display_text":"commented on task"},{"id":"629","task_id":"204","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/204","is_read":"0","created_on":"2019-04-19 14:32:14","display_text":"commented on task"},{"id":"625","task_id":"204","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/204","is_read":"0","created_on":"2019-04-19 14:28:53","display_text":"assigned new task to you"},{"id":"610","task_id":"198","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/198","is_read":"0","created_on":"2019-04-19 11:48:05","display_text":"assigned new task to you"},{"id":"609","task_id":"197","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/197","is_read":"0","created_on":"2019-04-19 11:42:34","display_text":"assigned new task to you"},{"id":"597","task_id":"191","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/191","is_read":"0","created_on":"2019-04-19 11:29:31","display_text":"assigned new task to you"},{"id":"596","task_id":"190","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/190","is_read":"0","created_on":"2019-04-19 11:20:58","display_text":"assigned new task to you"},{"id":"595","task_id":"189","employee_name":"Suresh","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"7","message":"Suresh assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/189","is_read":"0","created_on":"2019-04-19 11:18:12","display_text":"assigned new task to you"},{"id":"578","task_id":"183","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/183","is_read":"0","created_on":"2019-04-19 11:00:11","display_text":"assigned new task to you"},{"id":"571","task_id":"179","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/179","is_read":"0","created_on":"2019-04-19 10:03:10","display_text":"commented on task"},{"id":"569","task_id":"179","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/179","is_read":"0","created_on":"2019-04-19 10:03:01","display_text":"commented on task"},{"id":"567","task_id":"179","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/179","is_read":"0","created_on":"2019-04-19 10:02:31","display_text":"commented on task"},{"id":"565","task_id":"179","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/179","is_read":"0","created_on":"2019-04-19 09:51:08","display_text":"commented on task"},{"id":"563","task_id":"179","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/179","is_read":"0","created_on":"2019-04-19 09:46:41","display_text":"assigned new task to you"},{"id":"559","task_id":"178","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/178","is_read":"0","created_on":"2019-04-19 09:42:35","display_text":"commented on task"},{"id":"558","task_id":"178","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/178","is_read":"0","created_on":"2019-04-19 09:41:56","display_text":"assigned new task to you"},{"id":"459","task_id":"147","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/147","is_read":"0","created_on":"2019-04-18 10:58:30","display_text":"commented on task"},{"id":"453","task_id":"147","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/147","is_read":"0","created_on":"2019-04-18 10:38:21","display_text":"commented on task"},{"id":"449","task_id":"147","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/147","is_read":"0","created_on":"2019-04-18 10:32:45","display_text":"assigned new task to you"},{"id":"448","task_id":"146","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/146","is_read":"0","created_on":"2019-04-18 10:31:48","display_text":"assigned new task to you"},{"id":"224","task_id":"101","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin added attachments","flag":"TASK_ATTACHMENT","url":"admin/tasks/view/101","is_read":"0","created_on":"2019-03-22 15:51:08","display_text":"added attachments"},{"id":"221","task_id":"99","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/99","is_read":"0","created_on":"2019-03-22 11:19:35","display_text":"commented on task"},{"id":"220","task_id":"99","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/99","is_read":"0","created_on":"2019-03-22 11:19:30","display_text":"commented on task"},{"id":"217","task_id":"100","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin added attachments","flag":"TASK_ATTACHMENT","url":"admin/tasks/view/100","is_read":"0","created_on":"2019-03-22 10:13:32","display_text":"added attachments"},{"id":"214","task_id":"96","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin added attachments","flag":"TASK_ATTACHMENT","url":"admin/tasks/view/96","is_read":"0","created_on":"2019-03-21 16:55:08","display_text":"added attachments"},{"id":"213","task_id":"96","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/96","is_read":"0","created_on":"2019-03-21 16:54:46","display_text":"commented on task"},{"id":"195","task_id":"74","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/74","is_read":"0","created_on":"2019-03-21 15:34:10","display_text":"assigned new task to you"},{"id":"138","task_id":"69","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/69","is_read":"0","created_on":"2019-03-21 11:37:35","display_text":"commented on task"},{"id":"129","task_id":"71","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/71","is_read":"0","created_on":"2019-03-21 11:23:03","display_text":"commented on task"},{"id":"128","task_id":"71","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin added attachments","flag":"TASK_ATTACHMENT","url":"admin/tasks/view/71","is_read":"0","created_on":"2019-03-21 11:22:49","display_text":"added attachments"},{"id":"102","task_id":"63","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/63","is_read":"0","created_on":"2019-03-20 18:07:35","display_text":"assigned new task to you"},{"id":"65","task_id":"49","employee_name":"TEST USER","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"5","message":"TEST USER commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/49","is_read":"0","created_on":"2019-03-09 19:57:31","display_text":"commented on task"},{"id":"64","task_id":"49","employee_name":"TEST USER","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"5","message":"TEST USER assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/49","is_read":"0","created_on":"2019-03-09 19:57:26","display_text":"assigned new task to you"},{"id":"58","task_id":"48","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/48","is_read":"0","created_on":"2019-03-09 17:06:15","display_text":"assigned new task to you"},{"id":"57","task_id":"47","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/47","is_read":"0","created_on":"2019-03-09 17:06:15","display_text":"assigned new task to you"},{"id":"54","task_id":"43","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/43","is_read":"0","created_on":"2019-02-25 02:22:26","display_text":"assigned new task to you"},{"id":"51","task_id":"42","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/42","is_read":"0","created_on":"2019-02-12 00:14:22","display_text":"assigned new task to you"},{"id":"11","task_id":"20","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/20","is_read":"0","created_on":"2019-01-16 02:01:24","display_text":"commented on task"},{"id":"10","task_id":"20","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/20","is_read":"0","created_on":"2019-01-16 01:59:09","display_text":"commented on task"},{"id":"9","task_id":"20","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin commented on task","flag":"TASK_COMMENT","url":"admin/tasks/view/20","is_read":"0","created_on":"2019-01-16 01:58:46","display_text":"commented on task"},{"id":"6","task_id":"31","employee_name":"Admin","fcm_token":"dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm","action_employee_id":"1","message":"Admin assigned new task to you","flag":"TASK_ASSIGNED","url":"admin/tasks/view/31","is_read":"0","created_on":"2019-01-16 01:17:33","display_text":"assigned new task to you"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 718
         * task_id : 213
         * employee_name : Suresh
         * fcm_token : dV8pbptIhvA:APA91bH3qKWM1PfrZ4oIhaMM2BFzOyUWxUrGdUHeMwWpShFq18LgDdADVElEHs7rxHwfgRsHlYvP2vtTkc29GTYh51BCn0kKsIYTcP5mEv2iMKWqzSbIimem8WkWzMN4fodNHmxUgbtm
         * action_employee_id : 7
         * message : Suresh assigned new task to you
         * flag : TASK_ASSIGNED
         * url : admin/tasks/view/213
         * is_read : 0
         * created_on : 2019-04-19 15:08:57
         * display_text : assigned new task to you
         */

        @SerializedName("id")
        private String id;
        @SerializedName("task_id")
        private String taskId;
        @SerializedName("employee_name")
        private String employeeName;
        @SerializedName("fcm_token")
        private String fcmToken;
        @SerializedName("action_employee_id")
        private String actionEmployeeId;
        @SerializedName("message")
        private String message;
        @SerializedName("flag")
        private String flag;
        @SerializedName("url")
        private String url;
        @SerializedName("is_read")
        private String isRead;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("display_text")
        private String displayText;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(String fcmToken) {
            this.fcmToken = fcmToken;
        }

        public String getActionEmployeeId() {
            return actionEmployeeId;
        }

        public void setActionEmployeeId(String actionEmployeeId) {
            this.actionEmployeeId = actionEmployeeId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getIsRead() {
            return isRead;
        }

        public void setIsRead(String isRead) {
            this.isRead = isRead;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getDisplayText() {
            return displayText;
        }

        public void setDisplayText(String displayText) {
            this.displayText = displayText;
        }
    }
}
