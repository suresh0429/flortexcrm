package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerSpinnerResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"customers_types":[{"id":"1","name":"Architecture"},{"id":"5","name":"Client"},{"id":"2","name":"Contractor"},{"id":"3","name":"Dealer"},{"id":"4","name":"Other Source"}],"salutations":[{"id":"1","name":"M/S."},{"id":"2","name":"Mr."},{"id":"3","name":"Mrs."},{"id":"4","name":"Ms"}],"states":[{"id":"31","name":"ANDAMAN AND NICOBAR"},{"id":"1","name":"ANDHRA PRADESH "},{"id":"3","name":"ARUNACHAL PRADESH"},{"id":"2","name":"ASSAM"},{"id":"4","name":"BIHAR"},{"id":"30","name":"CHANDIGARH"},{"id":"34","name":"CHATTISGARH"},{"id":"29","name":"DADRA AND NAGAR HAVELI"},{"id":"28","name":"DAMAN & DIU"},{"id":"36","name":"DELHI"},{"id":"25","name":"GOA"},{"id":"5","name":"GUJRAT"},{"id":"6","name":"HARYANA"},{"id":"7","name":"HIMACHAL PRADESH"},{"id":"8","name":"JAMMU & KASHMIR"},{"id":"33","name":"JHARKHAND"},{"id":"9","name":"KARNATAKA"},{"id":"10","name":"KERALA"},{"id":"27","name":"LAKSHDWEEP"},{"id":"11","name":"MADHYA PRADESH"},{"id":"12","name":"MAHARASHTRA"},{"id":"13","name":"MANIPUR"},{"id":"14","name":"MEGHALAYA"},{"id":"15","name":"MIZORAM"},{"id":"16","name":"NAGALAND"},{"id":"17","name":"ODISHA"},{"id":"26","name":"PONDICHERY"},{"id":"18","name":"PUNJAB"},{"id":"19","name":"RAJASTHAN"},{"id":"20","name":"SIKKIM"},{"id":"21","name":"TAMIL NADU"},{"id":"35","name":"TELANGANA"},{"id":"22","name":"TRIPURA"},{"id":"23","name":"UTTAR PRADESH"},{"id":"37","name":"UTTARAKHAND"},{"id":"32","name":"UTTARANCHAL"},{"id":"24","name":"WEST BENGAL"}],"customer_code":234248}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * customers_types : [{"id":"1","name":"Architecture"},{"id":"5","name":"Client"},{"id":"2","name":"Contractor"},{"id":"3","name":"Dealer"},{"id":"4","name":"Other Source"}]
         * salutations : [{"id":"1","name":"M/S."},{"id":"2","name":"Mr."},{"id":"3","name":"Mrs."},{"id":"4","name":"Ms"}]
         * states : [{"id":"31","name":"ANDAMAN AND NICOBAR"},{"id":"1","name":"ANDHRA PRADESH "},{"id":"3","name":"ARUNACHAL PRADESH"},{"id":"2","name":"ASSAM"},{"id":"4","name":"BIHAR"},{"id":"30","name":"CHANDIGARH"},{"id":"34","name":"CHATTISGARH"},{"id":"29","name":"DADRA AND NAGAR HAVELI"},{"id":"28","name":"DAMAN & DIU"},{"id":"36","name":"DELHI"},{"id":"25","name":"GOA"},{"id":"5","name":"GUJRAT"},{"id":"6","name":"HARYANA"},{"id":"7","name":"HIMACHAL PRADESH"},{"id":"8","name":"JAMMU & KASHMIR"},{"id":"33","name":"JHARKHAND"},{"id":"9","name":"KARNATAKA"},{"id":"10","name":"KERALA"},{"id":"27","name":"LAKSHDWEEP"},{"id":"11","name":"MADHYA PRADESH"},{"id":"12","name":"MAHARASHTRA"},{"id":"13","name":"MANIPUR"},{"id":"14","name":"MEGHALAYA"},{"id":"15","name":"MIZORAM"},{"id":"16","name":"NAGALAND"},{"id":"17","name":"ODISHA"},{"id":"26","name":"PONDICHERY"},{"id":"18","name":"PUNJAB"},{"id":"19","name":"RAJASTHAN"},{"id":"20","name":"SIKKIM"},{"id":"21","name":"TAMIL NADU"},{"id":"35","name":"TELANGANA"},{"id":"22","name":"TRIPURA"},{"id":"23","name":"UTTAR PRADESH"},{"id":"37","name":"UTTARAKHAND"},{"id":"32","name":"UTTARANCHAL"},{"id":"24","name":"WEST BENGAL"}]
         * customer_code : 234248
         */

        @SerializedName("customer_code")
        private int customerCode;
        @SerializedName("customers_types")
        private List<CustomersTypesBean> customersTypes;
        @SerializedName("salutations")
        private List<SalutationsBean> salutations;
        @SerializedName("states")
        private List<StatesBean> states;

        public int getCustomerCode() {
            return customerCode;
        }

        public void setCustomerCode(int customerCode) {
            this.customerCode = customerCode;
        }

        public List<CustomersTypesBean> getCustomersTypes() {
            return customersTypes;
        }

        public void setCustomersTypes(List<CustomersTypesBean> customersTypes) {
            this.customersTypes = customersTypes;
        }

        public List<SalutationsBean> getSalutations() {
            return salutations;
        }

        public void setSalutations(List<SalutationsBean> salutations) {
            this.salutations = salutations;
        }

        public List<StatesBean> getStates() {
            return states;
        }

        public void setStates(List<StatesBean> states) {
            this.states = states;
        }

        public static class CustomersTypesBean {
            /**
             * id : 1
             * name : Architecture
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class SalutationsBean {
            /**
             * id : 1
             * name : M/S.
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class StatesBean {
            /**
             * id : 31
             * name : ANDAMAN AND NICOBAR
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
