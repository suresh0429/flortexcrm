package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadDocumentResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"document_type_name":"EXTRA DOC","document_name":"test","document_path":"uploads/lead_documents/2286e-screen-shot-2019-03-19-at-9.59.07-am.png"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * document_type_name : EXTRA DOC
         * document_name : test
         * document_path : uploads/lead_documents/2286e-screen-shot-2019-03-19-at-9.59.07-am.png
         */

        @SerializedName("document_type_name")
        private String documentTypeName;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("document_path")
        private String documentPath;

        public String getDocumentTypeName() {
            return documentTypeName;
        }

        public void setDocumentTypeName(String documentTypeName) {
            this.documentTypeName = documentTypeName;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getDocumentPath() {
            return documentPath;
        }

        public void setDocumentPath(String documentPath) {
            this.documentPath = documentPath;
        }
    }
}
