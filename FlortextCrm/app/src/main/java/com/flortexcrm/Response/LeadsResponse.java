package com.flortexcrm.Response;

import java.util.List;

public class LeadsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"84","lead_code":"09032019002","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"226","customers_type_id":"4","source_person_id":"225","assigned_to":"0","subject":"KL University","industry_id":"14","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-03-09 10:05:23","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Kl University","source":"Other Source","source_person_name":"Kl University","assigned_employee_name":null,"industry":"SCHOOL","rating":"FOLLOW UP"},{"id":"83","lead_code":"09032019001","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"224","customers_type_id":"4","source_person_id":"223","assigned_to":"0","subject":"vertex homes -siris signa","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-03-09 09:59:06","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Vertex Homes","source":"Other Source","source_person_name":"Vertex Homes","assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"82","lead_code":"05032019003","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"164","customers_type_id":"1","source_person_id":"34","assigned_to":"0","subject":"HPL AND THERMOPINE","industry_id":"12","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-03-05 13:30:18","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Praveen Reddy","source":"Architect","source_person_name":"Prasad","assigned_employee_name":null,"industry":"RESIDENCE","rating":"HOT ENQUIRY"},{"id":"81","lead_code":"05032019002","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"169","customers_type_id":"5","source_person_id":"169","assigned_to":"0","subject":"CARPET AND BLINDS","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-03-05 12:07:05","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Incor Properties","source":"Client","source_person_name":"Incor Properties","assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"80","lead_code":"05032019001","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"164","customers_type_id":"1","source_person_id":"152","assigned_to":"0","subject":"Thermopine 26X118","industry_id":"15","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-03-05 11:28:20","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Praveen Reddy","source":"Architect","source_person_name":"Mahesh Goud","assigned_employee_name":null,"industry":"VILLAS","rating":"HOT ENQUIRY"},{"id":"79","lead_code":"28022019005","created_by":"15","status":"OPEN","mode_id":"1","customer_id":"167","customers_type_id":"5","source_person_id":"167","assigned_to":"0","subject":"WOODEN FLOORING","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-28 19:07:16","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Asif","mode":"EXISTING CUSTOMER","customer_name":"Stone Henge","source":"Client","source_person_name":"Stone Henge","assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"78","lead_code":"28022019004","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"164","customers_type_id":"1","source_person_id":"34","assigned_to":"0","subject":"THERMOPINE AND HPL","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-28 16:10:18","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Praveen Reddy","source":"Architect","source_person_name":"Prasad","assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"77","lead_code":"28022019003","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"154","customers_type_id":"1","source_person_id":"154","assigned_to":"0","subject":"THERMOPINE CLADDING","industry_id":"15","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-28 11:47:40","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Sunitha Reddy","source":"Architect","source_person_name":"Sunitha Reddy","assigned_employee_name":null,"industry":"VILLAS","rating":"FOLLOW UP"},{"id":"76","lead_code":"28022019002","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"153","customers_type_id":"1","source_person_id":"152","assigned_to":"0","subject":"WOODEN FLOORING","industry_id":"15","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-28 11:41:50","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Ramana Reddy","source":"Architect","source_person_name":"Mahesh Goud","assigned_employee_name":null,"industry":"VILLAS","rating":"FOLLOW UP"},{"id":"75","lead_code":"28022019001","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"150","customers_type_id":"3","source_person_id":"149","assigned_to":"0","subject":"WOODEN FLOORING AND BLINDS","industry_id":"7","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-28 10:43:00","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Satish Rao","source":"Dealer","source_person_name":"Srinivasa Exclusive","assigned_employee_name":null,"industry":"Hotel","rating":"HOT ENQUIRY"},{"id":"74","lead_code":"26022019004","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"117","customers_type_id":"3","source_person_id":null,"assigned_to":"0","subject":"Square interiors and exteriors","industry_id":"9","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-26 18:44:17","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Square Interiors And Exteriors","source":"Dealer","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"COLD ENQUIRY"},{"id":"73","lead_code":"26022019003","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"116","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-26 16:06:57","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Namishree Projects","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"72","lead_code":"26022019002","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"115","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Green Park Hotel in Vijayawada","industry_id":"7","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-26 09:02:39","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Green Park Hotel In Vijayawada","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"Hotel","rating":"COLD ENQUIRY"},{"id":"71","lead_code":"26022019001","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"114","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Laila Hotel & Resorts pvt ltd","industry_id":"7","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-26 08:45:11","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Laila Hotel & Resorts Pvt Ltd","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"Hotel","rating":"COLD ENQUIRY"},{"id":"70","lead_code":"25022019001","created_by":"16","status":"OPEN","mode_id":"2","customer_id":"112","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"FLOORING","industry_id":"10","rating_id":"3","email_opt":"No","sms_opt":"Yes","created_on":"2019-02-25 16:48:00","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Arvind Tiwari","mode":"NEW CUSTOMER","customer_name":"Sms Pharma","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"PHARMA","rating":"HOT ENQUIRY"},{"id":"69","lead_code":"23022019005","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"111","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-23 12:05:50","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Niharika Constructions","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"68","lead_code":"23022019004","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"110","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-23 11:51:05","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Kreative House","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"67","lead_code":"23022019003","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"109","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-23 11:45:12","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Jain Housing & Constructions Ltd.","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"66","lead_code":"23022019002","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"108","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"6","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-23 11:29:01","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Uniworks Designs","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"FOLLOW UP"},{"id":"65","lead_code":"23022019001","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"107","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"product interdiction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-23 11:22:31","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Divya Sree & Nsl Group","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"64","lead_code":"22022019016","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"106","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"JYOTHIRMAYEE PROPERTIES- PALM SPRINGS","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:39:04","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Jyothirmayee Properties- Palm Springs","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"63","lead_code":"22022019015","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"105","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Garden Homes - Adarsh Builders","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:36:15","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Garden Homes - Adarsh Builders","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"62","lead_code":"22022019014","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"104","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Manipal hospital","industry_id":"6","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:31:02","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Manipal Hospital","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"COLD ENQUIRY"},{"id":"61","lead_code":"22022019013","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"103","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Jayabheri- The Capital","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:18:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Jayabheri- The Capital","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"60","lead_code":"22022019012","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"102","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"AIMS Hospital","industry_id":"6","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:13:18","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Aims Hospital","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"FOLLOW UP"},{"id":"59","lead_code":"22022019011","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"101","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"NRI Hospital","industry_id":"6","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 17:03:28","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Nri Hospital","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"COLD ENQUIRY"},{"id":"58","lead_code":"22022019010","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"100","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"NGOs HOUSING PROJECT","industry_id":"1","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:59:31","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Ngos Housing Project","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"FOLLOW UP"},{"id":"57","lead_code":"22022019009","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"99","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"AP Secretatiat & Heads of Department office Buildings, Amaravathi Government Complex","industry_id":"9","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:54:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Ap Secretatiat & Heads Of Department Office Buildings, Amaravath","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"FOLLOW UP"},{"id":"56","lead_code":"22022019008","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"98","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"KMV Bungalaws Project","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:51:17","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Kmv Bungalaws Project","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"55","lead_code":"22022019007","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"97","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"MLA ,MLC,AIS Apartements","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:45:25","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Mla ,mlc,ais Apartements","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"54","lead_code":"22022019006","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"96","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"APCRDA PROJECT OFFICE","industry_id":"9","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:38:24","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Apcrda Office","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"COLD ENQUIRY"},{"id":"53","lead_code":"22022019005","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"95","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"manjeera monarch","industry_id":"1","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:32:23","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Manjeera Monarch","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"COLD ENQUIRY"},{"id":"52","lead_code":"22022019004","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"94","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Grand capital","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:25:47","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Grand Capital","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"51","lead_code":"22022019003","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"93","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"TDP Office","industry_id":"9","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 16:18:46","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Tdp Office","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"FOLLOW UP"},{"id":"50","lead_code":"22022019002","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"92","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Mid Valley City,LEPL","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 09:02:51","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Mid Valley City","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"49","lead_code":"22022019001","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"91","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"SLV Builders and Developers","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-22 08:54:33","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Slv Builders And Developers","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"48","lead_code":"20022019004","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"90","customers_type_id":"3","source_person_id":null,"assigned_to":"0","subject":"The Looks Interior Concepts","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-20 09:19:51","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"The Looks Interior Concepts","source":"Dealer","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"47","lead_code":"20022019003","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"89","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"Design Wings","industry_id":"12","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-20 09:14:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Design Wings","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"COLD ENQUIRY"},{"id":"46","lead_code":"20022019002","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"88","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"L&T Construction","industry_id":"9","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-20 09:09:45","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"L&t Construction","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"COLD ENQUIRY"},{"id":"45","lead_code":"20022019001","created_by":"21","status":"OPEN","mode_id":"2","customer_id":"87","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"Inscape design studio","industry_id":"12","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-20 09:04:40","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Suman","mode":"NEW CUSTOMER","customer_name":"Abdul Azeem","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"COLD ENQUIRY"},{"id":"44","lead_code":"19022019004","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"86","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-19 19:28:50","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Boorugu Infra & Pranava Group","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"43","lead_code":"19022019003","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"85","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-19 19:08:13","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Archiscape Landscape & Architecture","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"42","lead_code":"19022019002","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"84","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-19 19:00:41","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Giridhari Homes","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"41","lead_code":"19022019001","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"83","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"Blinds","industry_id":"9","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-19 16:26:50","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Ennar's-excellency","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"FOLLOW UP"},{"id":"40","lead_code":"18022019001","created_by":"13","status":"CLOSED","mode_id":"1","customer_id":"81","customers_type_id":"1","source_person_id":"80","assigned_to":"0","subject":"HPL","industry_id":"12","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-18 15:11:13","updated_on":"0000-00-00 00:00:00","closed_on":"2019-03-05 14:42:04","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Padmalaya Residency","source":"Architect","source_person_name":"Bhosho Architects Pvt Ltd","assigned_employee_name":null,"industry":"RESIDENCE","rating":"HOT ENQUIRY"},{"id":"39","lead_code":"16022019022","created_by":"18","status":"OPEN","mode_id":"2","customer_id":"79","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 19:09:12","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"PT srinivas","mode":"NEW CUSTOMER","customer_name":"Garcarp","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"38","lead_code":"16022019021","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"77","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:55:24","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"I Kraft Interiors","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"37","lead_code":"16022019020","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"73","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"wooden flooring","industry_id":"9","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:47:27","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Mee Architects","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"FOLLOW UP"},{"id":"36","lead_code":"16022019019","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"71","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Wooden flooring & Carpet","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:42:40","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"White Water Constructions","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"35","lead_code":"16022019018","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"68","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:34:17","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Dasari Developers Llp","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"34","lead_code":"16022019017","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"67","customers_type_id":"3","source_person_id":null,"assigned_to":"0","subject":"Wooden flooring & Carpet","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:27:15","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Fortune Home Theatre","source":"Dealer","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"33","lead_code":"16022019016","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"66","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:19:32","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Pooja Creafted Homes","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"32","lead_code":"16022019015","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"65","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"product interdaction","industry_id":"12","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 18:08:40","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Ameya Designs","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"FOLLOW UP"},{"id":"31","lead_code":"16022019014","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"64","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"wooden flooring","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 17:57:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Prestige Group","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"30","lead_code":"16022019013","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"63","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"outdoor sports flooring & Grass","industry_id":"2","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 17:51:05","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Westren Constructions","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"HOT ENQUIRY"},{"id":"29","lead_code":"16022019012","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"59","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"Vinyl flooring & Blinds","industry_id":"6","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 17:40:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Star Hospitals","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"FOLLOW UP"},{"id":"28","lead_code":"16022019011","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"52","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"Vinyl Flooring","industry_id":"6","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 17:03:42","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Care Hospitals","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"HOSPITAL","rating":"FOLLOW UP"},{"id":"27","lead_code":"16022019010","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"50","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"IPE Deckwood","industry_id":"2","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:56:21","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Honer Homes","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"FOLLOW UP"},{"id":"26","lead_code":"16022019009","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"49","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"Product Introduction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:41:51","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Vertex Homes Pvt Ltd.","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"25","lead_code":"16022019008","created_by":"22","status":"OPEN","mode_id":"2","customer_id":"48","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:39:48","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Naveen","mode":"NEW CUSTOMER","customer_name":"Cushman&wakefield","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"24","lead_code":"16022019007","created_by":"22","status":"OPEN","mode_id":"2","customer_id":"47","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:32:41","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Naveen","mode":"NEW CUSTOMER","customer_name":"Salarpuria Sattva","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"23","lead_code":"16022019006","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"46","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"Product Demo","industry_id":"7","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:27:50","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Westin Hotels & Resorts","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"Hotel","rating":"COLD ENQUIRY"},{"id":"22","lead_code":"16022019005","created_by":"22","status":"OPEN","mode_id":"2","customer_id":"45","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:27:17","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Naveen","mode":"NEW CUSTOMER","customer_name":"My Home Construction","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"21","lead_code":"16022019004","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"44","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"9","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:13:11","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Raj Amudala","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"COLD ENQUIRY"},{"id":"20","lead_code":"16022019003","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"43","customers_type_id":"4","source_person_id":null,"assigned_to":"0","subject":"Product Intaduction","industry_id":"9","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 16:05:57","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"University Of Hyderabad","source":"Other Source","source_person_name":null,"assigned_employee_name":null,"industry":"OFFICE","rating":"COLD ENQUIRY"},{"id":"19","lead_code":"16022019002","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"42","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Wooden flooring","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 15:59:13","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Vishnu Group","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"18","lead_code":"16022019001","created_by":"23","status":"OPEN","mode_id":"2","customer_id":"41","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"carpet","industry_id":"2","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-16 15:53:21","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Brahma","mode":"NEW CUSTOMER","customer_name":"Aurbindo Realty","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"BUILDER","rating":"COLD ENQUIRY"},{"id":"17","lead_code":"15022019001","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"40","customers_type_id":"5","source_person_id":"40","assigned_to":"0","subject":"WOODEN FLOORING","industry_id":"15","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-15 10:58:28","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Bharat Yadav","source":"Client","source_person_name":"Bharat Yadav","assigned_employee_name":null,"industry":"VILLAS","rating":"HOT ENQUIRY"},{"id":"16","lead_code":"14022019002","created_by":"4","status":"OPEN","mode_id":"2","customer_id":"33","customers_type_id":"1","source_person_id":"14","assigned_to":"0","subject":"LUNA WOOD CLADDING LOUVERS HALF PATTERN","industry_id":"12","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-14 12:58:24","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Mohd yousuf ali","mode":"NEW CUSTOMER","customer_name":"Dinesh Lalwani","source":"Architect","source_person_name":"AAMIR & HAMEEDA","assigned_employee_name":null,"industry":"RESIDENCE","rating":"HOT ENQUIRY"},{"id":"15","lead_code":"14022019001","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"32","customers_type_id":"5","source_person_id":"32","assigned_to":"0","subject":"THERMO PINE TIE BEAM","industry_id":"2","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-14 11:25:02","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Vajram Constructions","source":"Client","source_person_name":"Vajram Constructions","assigned_employee_name":null,"industry":"BUILDER","rating":"HOT ENQUIRY"},{"id":"14","lead_code":"13022019001","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"31","customers_type_id":"5","source_person_id":"31","assigned_to":"0","subject":"WPC AND HPL REQUIREMENT","industry_id":"15","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-13 11:10:31","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Radhey Constructions India Pvt Ltd","source":"Client","source_person_name":"Radhey Constructions India Pvt Ltd","assigned_employee_name":null,"industry":"VILLAS","rating":"HOT ENQUIRY"},{"id":"13","lead_code":"12022019003","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"30","customers_type_id":"5","source_person_id":"30","assigned_to":"0","subject":"THERMO PINE","industry_id":"1","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-12 11:37:58","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Siddarth","source":"Client","source_person_name":"Siddarth","assigned_employee_name":null,"industry":"APARTMENT","rating":"HOT ENQUIRY"},{"id":"12","lead_code":"12022019002","created_by":"13","status":"OPEN","mode_id":"1","customer_id":"29","customers_type_id":"5","source_person_id":null,"assigned_to":"0","subject":"WOODEN FLOORING","industry_id":"12","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-12 11:28:46","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Krishna Kumar","mode":"EXISTING CUSTOMER","customer_name":"Greenlife Blueprints Llp","source":"Client","source_person_name":null,"assigned_employee_name":null,"industry":"RESIDENCE","rating":"HOT ENQUIRY"},{"id":"11","lead_code":"12022019001","created_by":"17","status":"OPEN","mode_id":"2","customer_id":"28","customers_type_id":"1","source_person_id":"27","assigned_to":"0","subject":"Luna Wood","industry_id":"1","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-12 11:07:52","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"kanishk Gupta","mode":"NEW CUSTOMER","customer_name":"Indocean Engineers Associates","source":"Architect","source_person_name":"Cinnamon Design Studio","assigned_employee_name":null,"industry":"APARTMENT","rating":"HOT ENQUIRY"},{"id":"10","lead_code":"09022019009","created_by":"4","status":"OPEN","mode_id":"2","customer_id":"26","customers_type_id":"2","source_person_id":null,"assigned_to":"0","subject":"Rubber Flooring","industry_id":"1","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 18:32:02","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Mohd yousuf ali","mode":"NEW CUSTOMER","customer_name":"NCC  PVT LTD","source":"Contractor","source_person_name":null,"assigned_employee_name":null,"industry":"APARTMENT","rating":"FOLLOW UP"},{"id":"9","lead_code":"09022019008","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"23","customers_type_id":"1","source_person_id":"23","assigned_to":"0","subject":"Call him to followup, he said he will visit","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:37:06","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"Walls Asia","source":"Architect","source_person_name":"Walls Asia","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"8","lead_code":"09022019007","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"22","customers_type_id":"1","source_person_id":"22","assigned_to":"0","subject":"Call him for leads","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:34:34","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"SEP Architects Pvt Ltd","source":"Architect","source_person_name":"SEP Architects Pvt Ltd","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"7","lead_code":"09022019006","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"21","customers_type_id":"1","source_person_id":"21","assigned_to":"0","subject":"Call him for lead","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:33:19","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"Sree Associates Projects Pvt Ltd","source":"Architect","source_person_name":"Sree Associates Projects Pvt Ltd","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"6","lead_code":"09022019005","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"20","customers_type_id":"1","source_person_id":"20","assigned_to":"0","subject":"Call to followup","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:31:43","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"Design Shades","source":"Architect","source_person_name":"Design Shades","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"5","lead_code":"09022019004","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"19","customers_type_id":"1","source_person_id":"19","assigned_to":"0","subject":"Visit Him","industry_id":"4","rating_id":"1","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:29:33","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"NaArchitects","source":"Architect","source_person_name":"NaArchitects","assigned_employee_name":null,"industry":"Commercial Space","rating":"COLD ENQUIRY"},{"id":"4","lead_code":"09022019003","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"15","customers_type_id":"1","source_person_id":"15","assigned_to":"0","subject":"Sample submitted","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:27:09","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"Smd Architectural Designs Pvt Ltd","source":"Architect","source_person_name":"Smd Architectural Designs Pvt Ltd","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"3","lead_code":"09022019002","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"17","customers_type_id":"1","source_person_id":"17","assigned_to":"0","subject":"follow up","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:24:43","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"360 Degrees Interior","source":"Architect","source_person_name":"360 Degrees Interior","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"2","lead_code":"09022019001","created_by":"5","status":"OPEN","mode_id":"1","customer_id":"18","customers_type_id":"1","source_person_id":"18","assigned_to":"0","subject":"Carpet samples given","industry_id":"4","rating_id":"2","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-09 15:22:31","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"EXISTING CUSTOMER","customer_name":"Milind Architectural","source":"Architect","source_person_name":"Milind Architectural","assigned_employee_name":null,"industry":"Commercial Space","rating":"FOLLOW UP"},{"id":"1","lead_code":"08022019001","created_by":"5","status":"OPEN","mode_id":"2","customer_id":"16","customers_type_id":"1","source_person_id":null,"assigned_to":"0","subject":"Quotation submitted for there own villa","industry_id":"13","rating_id":"3","email_opt":"Yes","sms_opt":"Yes","created_on":"2019-02-08 12:22:55","updated_on":"0000-00-00 00:00:00","closed_on":"0000-00-00 00:00:00","created_by_employee_name":"Khan","mode":"NEW CUSTOMER","customer_name":"P. PADMA LATHA","source":"Architect","source_person_name":null,"assigned_employee_name":null,"industry":"RESTURANT","rating":"HOT ENQUIRY"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 84
         * lead_code : 09032019002
         * created_by : 21
         * status : OPEN
         * mode_id : 2
         * customer_id : 226
         * customers_type_id : 4
         * source_person_id : 225
         * assigned_to : 0
         * subject : KL University
         * industry_id : 14
         * rating_id : 2
         * email_opt : Yes
         * sms_opt : Yes
         * created_on : 2019-03-09 10:05:23
         * updated_on : 0000-00-00 00:00:00
         * closed_on : 0000-00-00 00:00:00
         * created_by_employee_name : Suman
         * mode : NEW CUSTOMER
         * customer_name : Kl University
         * source : Other Source
         * source_person_name : Kl University
         * assigned_employee_name : null
         * industry : SCHOOL
         * rating : FOLLOW UP
         */

        private String id;
        private String lead_code;
        private String created_by;
        private String status;
        private String mode_id;
        private String customer_id;
        private String customers_type_id;
        private String source_person_id;
        private String assigned_to;
        private String subject;
        private String industry_id;
        private String rating_id;
        private String email_opt;
        private String sms_opt;
        private String created_on;
        private String updated_on;
        private String closed_on;
        private String created_by_employee_name;
        private String mode;
        private String customer_name;
        private String source;
        private String source_person_name;
        private Object assigned_employee_name;
        private String industry;
        private String rating;
        private String quotation_id;
        private String quotation_no;
        private String sales_order_id;
        private String sales_order_no;

        public String getQuotation_id() {
            return quotation_id;
        }

        public void setQuotation_id(String quotation_id) {
            this.quotation_id = quotation_id;
        }

        public String getQuotation_no() {
            return quotation_no;
        }

        public void setQuotation_no(String quotation_no) {
            this.quotation_no = quotation_no;
        }

        public String getSales_order_id() {
            return sales_order_id;
        }

        public void setSales_order_id(String sales_order_id) {
            this.sales_order_id = sales_order_id;
        }

        public String getSales_order_no() {
            return sales_order_no;
        }

        public void setSales_order_no(String sales_order_no) {
            this.sales_order_no = sales_order_no;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLead_code() {
            return lead_code;
        }

        public void setLead_code(String lead_code) {
            this.lead_code = lead_code;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMode_id() {
            return mode_id;
        }

        public void setMode_id(String mode_id) {
            this.mode_id = mode_id;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getCustomers_type_id() {
            return customers_type_id;
        }

        public void setCustomers_type_id(String customers_type_id) {
            this.customers_type_id = customers_type_id;
        }

        public String getSource_person_id() {
            return source_person_id;
        }

        public void setSource_person_id(String source_person_id) {
            this.source_person_id = source_person_id;
        }

        public String getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(String assigned_to) {
            this.assigned_to = assigned_to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getIndustry_id() {
            return industry_id;
        }

        public void setIndustry_id(String industry_id) {
            this.industry_id = industry_id;
        }

        public String getRating_id() {
            return rating_id;
        }

        public void setRating_id(String rating_id) {
            this.rating_id = rating_id;
        }

        public String getEmail_opt() {
            return email_opt;
        }

        public void setEmail_opt(String email_opt) {
            this.email_opt = email_opt;
        }

        public String getSms_opt() {
            return sms_opt;
        }

        public void setSms_opt(String sms_opt) {
            this.sms_opt = sms_opt;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }

        public String getClosed_on() {
            return closed_on;
        }

        public void setClosed_on(String closed_on) {
            this.closed_on = closed_on;
        }

        public String getCreated_by_employee_name() {
            return created_by_employee_name;
        }

        public void setCreated_by_employee_name(String created_by_employee_name) {
            this.created_by_employee_name = created_by_employee_name;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getSource_person_name() {
            return source_person_name;
        }

        public void setSource_person_name(String source_person_name) {
            this.source_person_name = source_person_name;
        }

        public Object getAssigned_employee_name() {
            return assigned_employee_name;
        }

        public void setAssigned_employee_name(Object assigned_employee_name) {
            this.assigned_employee_name = assigned_employee_name;
        }

        public String getIndustry() {
            return industry;
        }

        public void setIndustry(String industry) {
            this.industry = industry;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }
    }
}
