package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class BankDetailResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"id":"3","customers_id":"18","bank_name":"ICICI bank","bank_acc_no":"061801503504","bank_branch":"hyderabad","ifsc_code":"ICIC0000618","created_on":"2019-03-26 11:19:25","updated_on":"0000-00-00 00:00:00","is_active":"1"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * customers_id : 18
         * bank_name : ICICI bank
         * bank_acc_no : 061801503504
         * bank_branch : hyderabad
         * ifsc_code : ICIC0000618
         * created_on : 2019-03-26 11:19:25
         * updated_on : 0000-00-00 00:00:00
         * is_active : 1
         */

        @SerializedName("id")
        private String id;
        @SerializedName("customers_id")
        private String customersId;
        @SerializedName("bank_name")
        private String bankName;
        @SerializedName("bank_acc_no")
        private String bankAccNo;
        @SerializedName("bank_branch")
        private String bankBranch;
        @SerializedName("ifsc_code")
        private String ifscCode;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("is_active")
        private String isActive;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomersId() {
            return customersId;
        }

        public void setCustomersId(String customersId) {
            this.customersId = customersId;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getBankAccNo() {
            return bankAccNo;
        }

        public void setBankAccNo(String bankAccNo) {
            this.bankAccNo = bankAccNo;
        }

        public String getBankBranch() {
            return bankBranch;
        }

        public void setBankBranch(String bankBranch) {
            this.bankBranch = bankBranch;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public void setIfscCode(String ifscCode) {
            this.ifscCode = ifscCode;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }
    }
}
