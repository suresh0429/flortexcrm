package com.flortexcrm.Response;

import java.util.List;

public class CreateTaskResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"employees":[{"id":"1","name":"Admin"},{"id":"3","name":"Bushra"},{"id":"4","name":"hemant"},{"id":"2","name":"nabi"},{"id":"5","name":"TEST USER"}],"priorities":[{"id":"1","name":"NORMAL"},{"id":"2","name":"HIGH PRIOPRITY"}],"tasks_types":[{"id":"1","name":"Lead Task (Order)"},{"id":"2","name":"PAYMENTS"},{"id":"3","name":"WORK TO DO"},{"id":"4","name":"C FORMS "},{"id":"5","name":"INSTALLATION"},{"id":"6","name":"Pending Installtion"},{"id":"7","name":"Delivery"}],"tasks_status":[{"id":"1","name":"NEW"},{"id":"2","name":"OPEN"},{"id":"3","name":"CLOSED"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<EmployeesBean> employees;
        private List<PrioritiesBean> priorities;
        private List<TasksTypesBean> tasks_types;
        private List<TasksStatusBean> tasks_status;

        public List<EmployeesBean> getEmployees() {
            return employees;
        }

        public void setEmployees(List<EmployeesBean> employees) {
            this.employees = employees;
        }

        public List<PrioritiesBean> getPriorities() {
            return priorities;
        }

        public void setPriorities(List<PrioritiesBean> priorities) {
            this.priorities = priorities;
        }

        public List<TasksTypesBean> getTasks_types() {
            return tasks_types;
        }

        public void setTasks_types(List<TasksTypesBean> tasks_types) {
            this.tasks_types = tasks_types;
        }

        public List<TasksStatusBean> getTasks_status() {
            return tasks_status;
        }

        public void setTasks_status(List<TasksStatusBean> tasks_status) {
            this.tasks_status = tasks_status;
        }

        public static class EmployeesBean {
            /**
             * id : 1
             * name : Admin
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class PrioritiesBean {
            /**
             * id : 1
             * name : NORMAL
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class TasksTypesBean {
            /**
             * id : 1
             * name : Lead Task (Order)
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class TasksStatusBean {
            /**
             * id : 1
             * name : NEW
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
