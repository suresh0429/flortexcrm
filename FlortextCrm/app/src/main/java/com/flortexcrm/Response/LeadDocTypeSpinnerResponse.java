package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadDocTypeSpinnerResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"1","name":"DELIVERY CHALLAN"},{"id":"8","name":"EXTRA DOC"},{"id":"2","name":"PURCHASE ORDER"},{"id":"7","name":"QUOTATION"},{"id":"6","name":"RECD ACKNOWLEDGEMENT DC / INVOICE"},{"id":"4","name":"SALE ORDER"},{"id":"3","name":"TAX INVOICE"},{"id":"5","name":"WAY BILL"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : DELIVERY CHALLAN
         */

        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
