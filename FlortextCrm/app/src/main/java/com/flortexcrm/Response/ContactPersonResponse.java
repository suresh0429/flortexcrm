package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactPersonResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"25","customers_id":"2","role_id":"1","salutation":"2","name":"Ganesh","mobile":"1234567889","email":"ganesh@gmail.com","email_opt":"Yes","sms_opt":"No","is_primary":"0","created_on":"2019-04-15 10:48:06","updated_on":"2019-04-15 10:48:06","is_active":"1","role_name":"Director"},{"id":"27","customers_id":"2","role_id":"1","salutation":"2","name":"Ganesh","mobile":"1234567889","email":"ganesh@gmail.com","email_opt":"Yes","sms_opt":"No","is_primary":"0","created_on":"2019-04-15 10:57:10","updated_on":"2019-04-15 10:57:10","is_active":"1","role_name":"Director"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 25
         * customers_id : 2
         * role_id : 1
         * salutation : 2
         * name : Ganesh
         * mobile : 1234567889
         * email : ganesh@gmail.com
         * email_opt : Yes
         * sms_opt : No
         * is_primary : 0
         * created_on : 2019-04-15 10:48:06
         * updated_on : 2019-04-15 10:48:06
         * is_active : 1
         * role_name : Director
         */

        @SerializedName("id")
        private String id;
        @SerializedName("customers_id")
        private String customersId;
        @SerializedName("role_id")
        private String roleId;
        @SerializedName("salutation")
        private String salutation;
        @SerializedName("name")
        private String name;
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("email")
        private String email;
        @SerializedName("email_opt")
        private String emailOpt;
        @SerializedName("sms_opt")
        private String smsOpt;
        @SerializedName("is_primary")
        private String isPrimary;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("is_active")
        private String isActive;
        @SerializedName("role_name")
        private String roleName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomersId() {
            return customersId;
        }

        public void setCustomersId(String customersId) {
            this.customersId = customersId;
        }

        public String getRoleId() {
            return roleId;
        }

        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

        public String getSalutation() {
            return salutation;
        }

        public void setSalutation(String salutation) {
            this.salutation = salutation;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmailOpt() {
            return emailOpt;
        }

        public void setEmailOpt(String emailOpt) {
            this.emailOpt = emailOpt;
        }

        public String getSmsOpt() {
            return smsOpt;
        }

        public void setSmsOpt(String smsOpt) {
            this.smsOpt = smsOpt;
        }

        public String getIsPrimary() {
            return isPrimary;
        }

        public void setIsPrimary(String isPrimary) {
            this.isPrimary = isPrimary;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }
    }
}
