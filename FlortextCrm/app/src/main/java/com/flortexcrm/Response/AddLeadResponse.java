package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class AddLeadResponse {


    /**
     * status : 10100
     * message : Lead created successfully
     * data : {"lead_id":25}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lead_id : 25
         */

        @SerializedName("lead_id")
        private int leadId;

        public int getLeadId() {
            return leadId;
        }

        public void setLeadId(int leadId) {
            this.leadId = leadId;
        }
    }
}
