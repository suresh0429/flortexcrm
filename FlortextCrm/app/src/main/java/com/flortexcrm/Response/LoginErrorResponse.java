package com.flortexcrm.Response;

public class LoginErrorResponse {


    /**
     * status : 10500
     * message : Login failed. Incorrect password
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
