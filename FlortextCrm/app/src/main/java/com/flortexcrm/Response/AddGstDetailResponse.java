package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class AddGstDetailResponse {


    /**
     * status : 10100
     * message : Data save successfully
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
