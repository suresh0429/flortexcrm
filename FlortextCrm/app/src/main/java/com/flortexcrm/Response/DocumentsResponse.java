package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocumentsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"3","customers_id":"18","document_name":"FLOR IMAGE","document_path":"uploads/customer_documents/a03f8-screen-shot-2019-03-26-at-11.27.33-am.png","created_on":"2019-03-26 12:43:43"},{"id":"4","customers_id":"18","document_name":"Flor Raw","document_path":"uploads/customer_documents/3ed6a-screen-shot-2018-12-28-at-11.04.19-am.png","created_on":"2019-03-26 12:44:14"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * customers_id : 18
         * document_name : FLOR IMAGE
         * document_path : uploads/customer_documents/a03f8-screen-shot-2019-03-26-at-11.27.33-am.png
         * created_on : 2019-03-26 12:43:43
         */

        @SerializedName("id")
        private String id;
        @SerializedName("customers_id")
        private String customersId;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("document_path")
        private String documentPath;
        @SerializedName("created_on")
        private String createdOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomersId() {
            return customersId;
        }

        public void setCustomersId(String customersId) {
            this.customersId = customersId;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getDocumentPath() {
            return documentPath;
        }

        public void setDocumentPath(String documentPath) {
            this.documentPath = documentPath;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
