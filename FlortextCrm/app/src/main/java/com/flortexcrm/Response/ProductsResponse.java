package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"3","name":"Aberdeen 01 Brown - PVC","image":"","packing":"53.82","product_code":"237","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"20","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 19:51:05","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 01 Brown - PVC"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * name : Aberdeen 01 Brown - PVC
         * image :
         * packing : 53.82
         * product_code : 237
         * description : VITOFLOOR  Loop Plie Modular C arpet Tile
         * product_cat_id : 0
         * product_cat_sub_id : 0
         * product_cat_child_id : 0
         * uom_id : 2
         * product_department_id : 0
         * product_type_id : 1
         * product_main_group_id : 1
         * product_group_id : 5
         * hsn_id : 3
         * manufacturer_barcode :
         * weight_per_box : 20
         * barcode :
         * no_of_pcs_in_box : 20
         * size : 580 Gms, Pp, 1/10 Guage, 50cm X 50cm
         * laying_rate : 0
         * selling_rate : 0
         * dealer_price : 60
         * gst_itc_eligibility : Inputs
         * gst_tax : Taxable
         * gst_product_type : Goods
         * product_laying : Yes
         * is_active : 1
         * minimum_stock : 10
         * created_on : 2018-12-25 19:51:05
         * updated_on : 2018-12-25 20:34:50
         * category_name : null
         * category_sub_name : null
         * category_child_name : null
         * product_name : Aberdeen 01 Brown - PVC
         */

        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("image")
        private String image;
        @SerializedName("packing")
        private String packing;
        @SerializedName("product_code")
        private String productCode;
        @SerializedName("description")
        private String description;
        @SerializedName("product_cat_id")
        private String productCatId;
        @SerializedName("product_cat_sub_id")
        private String productCatSubId;
        @SerializedName("product_cat_child_id")
        private String productCatChildId;
        @SerializedName("uom_id")
        private String uomId;
        @SerializedName("product_department_id")
        private String productDepartmentId;
        @SerializedName("product_type_id")
        private String productTypeId;
        @SerializedName("product_main_group_id")
        private String productMainGroupId;
        @SerializedName("product_group_id")
        private String productGroupId;
        @SerializedName("hsn_id")
        private String hsnId;
        @SerializedName("manufacturer_barcode")
        private String manufacturerBarcode;
        @SerializedName("weight_per_box")
        private String weightPerBox;
        @SerializedName("barcode")
        private String barcode;
        @SerializedName("no_of_pcs_in_box")
        private String noOfPcsInBox;
        @SerializedName("size")
        private String size;
        @SerializedName("laying_rate")
        private String layingRate;
        @SerializedName("selling_rate")
        private String sellingRate;
        @SerializedName("dealer_price")
        private String dealerPrice;
        @SerializedName("gst_itc_eligibility")
        private String gstItcEligibility;
        @SerializedName("gst_tax")
        private String gstTax;
        @SerializedName("gst_product_type")
        private String gstProductType;
        @SerializedName("product_laying")
        private String productLaying;
        @SerializedName("is_active")
        private String isActive;
        @SerializedName("minimum_stock")
        private String minimumStock;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("category_name")
        private Object categoryName;
        @SerializedName("category_sub_name")
        private Object categorySubName;
        @SerializedName("category_child_name")
        private Object categoryChildName;
        @SerializedName("product_name")
        private String productName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPacking() {
            return packing;
        }

        public void setPacking(String packing) {
            this.packing = packing;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProductCatId() {
            return productCatId;
        }

        public void setProductCatId(String productCatId) {
            this.productCatId = productCatId;
        }

        public String getProductCatSubId() {
            return productCatSubId;
        }

        public void setProductCatSubId(String productCatSubId) {
            this.productCatSubId = productCatSubId;
        }

        public String getProductCatChildId() {
            return productCatChildId;
        }

        public void setProductCatChildId(String productCatChildId) {
            this.productCatChildId = productCatChildId;
        }

        public String getUomId() {
            return uomId;
        }

        public void setUomId(String uomId) {
            this.uomId = uomId;
        }

        public String getProductDepartmentId() {
            return productDepartmentId;
        }

        public void setProductDepartmentId(String productDepartmentId) {
            this.productDepartmentId = productDepartmentId;
        }

        public String getProductTypeId() {
            return productTypeId;
        }

        public void setProductTypeId(String productTypeId) {
            this.productTypeId = productTypeId;
        }

        public String getProductMainGroupId() {
            return productMainGroupId;
        }

        public void setProductMainGroupId(String productMainGroupId) {
            this.productMainGroupId = productMainGroupId;
        }

        public String getProductGroupId() {
            return productGroupId;
        }

        public void setProductGroupId(String productGroupId) {
            this.productGroupId = productGroupId;
        }

        public String getHsnId() {
            return hsnId;
        }

        public void setHsnId(String hsnId) {
            this.hsnId = hsnId;
        }

        public String getManufacturerBarcode() {
            return manufacturerBarcode;
        }

        public void setManufacturerBarcode(String manufacturerBarcode) {
            this.manufacturerBarcode = manufacturerBarcode;
        }

        public String getWeightPerBox() {
            return weightPerBox;
        }

        public void setWeightPerBox(String weightPerBox) {
            this.weightPerBox = weightPerBox;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getNoOfPcsInBox() {
            return noOfPcsInBox;
        }

        public void setNoOfPcsInBox(String noOfPcsInBox) {
            this.noOfPcsInBox = noOfPcsInBox;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getLayingRate() {
            return layingRate;
        }

        public void setLayingRate(String layingRate) {
            this.layingRate = layingRate;
        }

        public String getSellingRate() {
            return sellingRate;
        }

        public void setSellingRate(String sellingRate) {
            this.sellingRate = sellingRate;
        }

        public String getDealerPrice() {
            return dealerPrice;
        }

        public void setDealerPrice(String dealerPrice) {
            this.dealerPrice = dealerPrice;
        }

        public String getGstItcEligibility() {
            return gstItcEligibility;
        }

        public void setGstItcEligibility(String gstItcEligibility) {
            this.gstItcEligibility = gstItcEligibility;
        }

        public String getGstTax() {
            return gstTax;
        }

        public void setGstTax(String gstTax) {
            this.gstTax = gstTax;
        }

        public String getGstProductType() {
            return gstProductType;
        }

        public void setGstProductType(String gstProductType) {
            this.gstProductType = gstProductType;
        }

        public String getProductLaying() {
            return productLaying;
        }

        public void setProductLaying(String productLaying) {
            this.productLaying = productLaying;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getMinimumStock() {
            return minimumStock;
        }

        public void setMinimumStock(String minimumStock) {
            this.minimumStock = minimumStock;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public Object getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(Object categoryName) {
            this.categoryName = categoryName;
        }

        public Object getCategorySubName() {
            return categorySubName;
        }

        public void setCategorySubName(Object categorySubName) {
            this.categorySubName = categorySubName;
        }

        public Object getCategoryChildName() {
            return categoryChildName;
        }

        public void setCategoryChildName(Object categoryChildName) {
            this.categoryChildName = categoryChildName;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }
    }
}
