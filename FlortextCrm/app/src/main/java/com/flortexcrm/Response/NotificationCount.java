package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class NotificationCount {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"notification_count":"21"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * notification_count : 21
         */

        @SerializedName("notification_count")
        private String notificationCount;

        public String getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(String notificationCount) {
            this.notificationCount = notificationCount;
        }
    }
}
