package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

public class GSTResponse {

    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"id":"23","customers_id":"23","gst_no":"777","gst_state_id":"35","gst_sales_type_id":"3","pan_no":"78787","created_on":"2019-04-12 18:05:43","updated_on":"2019-04-15 13:20:33","is_active":"1","gst_code":"36"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 23
         * customers_id : 23
         * gst_no : 777
         * gst_state_id : 35
         * gst_sales_type_id : 3
         * pan_no : 78787
         * created_on : 2019-04-12 18:05:43
         * updated_on : 2019-04-15 13:20:33
         * is_active : 1
         * gst_code : 36
         */

        @SerializedName("id")
        private String id;
        @SerializedName("customers_id")
        private String customersId;
        @SerializedName("gst_no")
        private String gstNo;
        @SerializedName("gst_state_id")
        private String gstStateId;
        @SerializedName("gst_sales_type_id")
        private String gstSalesTypeId;
        @SerializedName("pan_no")
        private String panNo;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("is_active")
        private String isActive;
        @SerializedName("gst_code")
        private String gstCode;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomersId() {
            return customersId;
        }

        public void setCustomersId(String customersId) {
            this.customersId = customersId;
        }

        public String getGstNo() {
            return gstNo;
        }

        public void setGstNo(String gstNo) {
            this.gstNo = gstNo;
        }

        public String getGstStateId() {
            return gstStateId;
        }

        public void setGstStateId(String gstStateId) {
            this.gstStateId = gstStateId;
        }

        public String getGstSalesTypeId() {
            return gstSalesTypeId;
        }

        public void setGstSalesTypeId(String gstSalesTypeId) {
            this.gstSalesTypeId = gstSalesTypeId;
        }

        public String getPanNo() {
            return panNo;
        }

        public void setPanNo(String panNo) {
            this.panNo = panNo;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getGstCode() {
            return gstCode;
        }

        public void setGstCode(String gstCode) {
            this.gstCode = gstCode;
        }
    }
}
