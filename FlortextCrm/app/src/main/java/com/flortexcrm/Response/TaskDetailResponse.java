package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TaskDetailResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"task":{"id":"139","leads_id":null,"customers_type_id":null,"customer_id":null,"employee_id":"2","activity_id":"0","task_subject":"hdjdj","task_details":"nfnfn","priority_id":"1","task_type_id":"1","task_status_id":"1","layer_name":null,"material_details":null,"installation_status":null,"installation_remark":null,"auto_person":null,"tansport_name":null,"truck_no":null,"lr_no":null,"mode":null,"delivery_status":null,"delivery_remark":null,"remark":null,"attachments":null,"started_on":"16-04-2019 12:21","completed_on":"0000-00-00 00:00:00","created_on":"2019-04-16 12:21:54","updated_on":"0000-00-00 00:00:00","status":"1","task_type_name":"Lead Task (Order)","assigned_to_employees":[{"tasks_id":"139","employee_id":"1","status_id":"1","due_date":"2019-04-16 12:21:00","created_at":"1555397514","user_name":"Admin"}],"assigned_to_employees_id":["1"]},"chats":[{"id":"411","action_by":"2","lead_id":"0","task_id":"139","created_on":"2019-04-16 12:21:54","comment":"Vijay created Lead Task (Order) task for date 16-04-2019 12:21 pm and assinged to Admin","attachments":null,"user_name":"Vijay"}],"employees":[{"id":"2","roles_id":["2###3","4###6"],"name":"Vijay","email":"vijay@innasoft.in","mobile":"8019448098","password":"6f26adf1f23bb5ab03abd7bfcd504cbf","salt":"AnCqr7npIP","token":"6249442cc474f8e9e10f35aa8b7150e1","fcm_token":"cP_wpdHNCoQ:APA91bGsGMgyOiNoFhQFmqIHgSxORbhcrrWU3pp9f4fK_wtvJOWYcRu8fHPJriAwojvpPyx1cMZmNNukXX4Tr8-DX7OMqcpYN2_P3lu6vSb3FFl2QaMQ7J-OzxP-sO52flvLBzCFKF9h","profile_image":"1544976439_profile_image.jpg","created_at":"1543223968","updated_at":"1553578142","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"2","departments_id":"2","created_at":"1553578142","department_name":"Installation"},{"users_id":"2","departments_id":"4","created_at":"1553578142","department_name":"Sales"}],"departments_id":["2","4"],"roles":[{"users_id":"2","departments_id":"2","roles_id":"3","created_at":"1553578142","role_name":"Installation Boy"},{"users_id":"2","departments_id":"4","roles_id":"6","created_at":"1553578142","role_name":"Salesman"}]},{"id":"3","roles_id":["3###4"],"name":"Bushra","email":"vijaydel@innasoft.in","mobile":"9999966666","password":"f7ec86c483d398fa7c68438f626ea4a6","salt":"OltTcXJhIy","token":"b4591e2f7288e8ca44a5ab9499cc3f2c","fcm_token":"","profile_image":null,"created_at":"1544189012","updated_at":"1552319371","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"3","departments_id":"3","created_at":"1552319371","department_name":"Delivery"}],"departments_id":["3"],"roles":[{"users_id":"3","departments_id":"3","roles_id":"4","created_at":"1552319371","role_name":"Delivery Boy"}]},{"id":"4","roles_id":["3###4"],"name":"hemant","email":"hemant@gmail.com","mobile":"9898989898","password":"5631236e0907a30309f3c1aebcb0c41a","salt":"oVQjAqOkrw","token":"65cc40f71c9de2a49d9128a2143a6961","fcm_token":"","profile_image":null,"created_at":"1548177873","updated_at":"1548178172","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"4","departments_id":"3","created_at":"1548178172","department_name":"Delivery"}],"departments_id":["3"],"roles":[{"users_id":"4","departments_id":"3","roles_id":"4","created_at":"1548178172","role_name":"Delivery Boy"}]},{"id":"5","roles_id":["4###6"],"name":"TEST USER","email":"test@flortex.in","mobile":"9908650650","password":"f62e2160f2a0d761c8df75380261f118","salt":"Rfa2U4HEmW","token":"b5872e1f93484f9dc553a33ad93dcdd0","fcm_token":"","profile_image":null,"created_at":"1552133963","updated_at":"","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"5","departments_id":"4","created_at":"1552133963","department_name":"Sales"}],"departments_id":["4"],"roles":[{"users_id":"5","departments_id":"4","roles_id":"6","created_at":"1552133963","role_name":"Salesman"}]}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * task : {"id":"139","leads_id":null,"customers_type_id":null,"customer_id":null,"employee_id":"2","activity_id":"0","task_subject":"hdjdj","task_details":"nfnfn","priority_id":"1","task_type_id":"1","task_status_id":"1","layer_name":null,"material_details":null,"installation_status":null,"installation_remark":null,"auto_person":null,"tansport_name":null,"truck_no":null,"lr_no":null,"mode":null,"delivery_status":null,"delivery_remark":null,"remark":null,"attachments":null,"started_on":"16-04-2019 12:21","completed_on":"0000-00-00 00:00:00","created_on":"2019-04-16 12:21:54","updated_on":"0000-00-00 00:00:00","status":"1","task_type_name":"Lead Task (Order)","assigned_to_employees":[{"tasks_id":"139","employee_id":"1","status_id":"1","due_date":"2019-04-16 12:21:00","created_at":"1555397514","user_name":"Admin"}],"assigned_to_employees_id":["1"]}
         * chats : [{"id":"411","action_by":"2","lead_id":"0","task_id":"139","created_on":"2019-04-16 12:21:54","comment":"Vijay created Lead Task (Order) task for date 16-04-2019 12:21 pm and assinged to Admin","attachments":null,"user_name":"Vijay"}]
         * employees : [{"id":"2","roles_id":["2###3","4###6"],"name":"Vijay","email":"vijay@innasoft.in","mobile":"8019448098","password":"6f26adf1f23bb5ab03abd7bfcd504cbf","salt":"AnCqr7npIP","token":"6249442cc474f8e9e10f35aa8b7150e1","fcm_token":"cP_wpdHNCoQ:APA91bGsGMgyOiNoFhQFmqIHgSxORbhcrrWU3pp9f4fK_wtvJOWYcRu8fHPJriAwojvpPyx1cMZmNNukXX4Tr8-DX7OMqcpYN2_P3lu6vSb3FFl2QaMQ7J-OzxP-sO52flvLBzCFKF9h","profile_image":"1544976439_profile_image.jpg","created_at":"1543223968","updated_at":"1553578142","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"2","departments_id":"2","created_at":"1553578142","department_name":"Installation"},{"users_id":"2","departments_id":"4","created_at":"1553578142","department_name":"Sales"}],"departments_id":["2","4"],"roles":[{"users_id":"2","departments_id":"2","roles_id":"3","created_at":"1553578142","role_name":"Installation Boy"},{"users_id":"2","departments_id":"4","roles_id":"6","created_at":"1553578142","role_name":"Salesman"}]},{"id":"3","roles_id":["3###4"],"name":"Bushra","email":"vijaydel@innasoft.in","mobile":"9999966666","password":"f7ec86c483d398fa7c68438f626ea4a6","salt":"OltTcXJhIy","token":"b4591e2f7288e8ca44a5ab9499cc3f2c","fcm_token":"","profile_image":null,"created_at":"1544189012","updated_at":"1552319371","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"3","departments_id":"3","created_at":"1552319371","department_name":"Delivery"}],"departments_id":["3"],"roles":[{"users_id":"3","departments_id":"3","roles_id":"4","created_at":"1552319371","role_name":"Delivery Boy"}]},{"id":"4","roles_id":["3###4"],"name":"hemant","email":"hemant@gmail.com","mobile":"9898989898","password":"5631236e0907a30309f3c1aebcb0c41a","salt":"oVQjAqOkrw","token":"65cc40f71c9de2a49d9128a2143a6961","fcm_token":"","profile_image":null,"created_at":"1548177873","updated_at":"1548178172","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"4","departments_id":"3","created_at":"1548178172","department_name":"Delivery"}],"departments_id":["3"],"roles":[{"users_id":"4","departments_id":"3","roles_id":"4","created_at":"1548178172","role_name":"Delivery Boy"}]},{"id":"5","roles_id":["4###6"],"name":"TEST USER","email":"test@flortex.in","mobile":"9908650650","password":"f62e2160f2a0d761c8df75380261f118","salt":"Rfa2U4HEmW","token":"b5872e1f93484f9dc553a33ad93dcdd0","fcm_token":"","profile_image":null,"created_at":"1552133963","updated_at":"","status":"1","current_status":"Active","current_status_bootstrap_class":"primary","departments":[{"users_id":"5","departments_id":"4","created_at":"1552133963","department_name":"Sales"}],"departments_id":["4"],"roles":[{"users_id":"5","departments_id":"4","roles_id":"6","created_at":"1552133963","role_name":"Salesman"}]}]
         */

        @SerializedName("task")
        private TaskBean task;
        @SerializedName("chats")
        private List<ChatsBean> chats;
        @SerializedName("employees")
        private List<EmployeesBean> employees;

        public TaskBean getTask() {
            return task;
        }

        public void setTask(TaskBean task) {
            this.task = task;
        }

        public List<ChatsBean> getChats() {
            return chats;
        }

        public void setChats(List<ChatsBean> chats) {
            this.chats = chats;
        }

        public List<EmployeesBean> getEmployees() {
            return employees;
        }

        public void setEmployees(List<EmployeesBean> employees) {
            this.employees = employees;
        }

        public static class TaskBean {
            /**
             * id : 139
             * leads_id : null
             * customers_type_id : null
             * customer_id : null
             * employee_id : 2
             * activity_id : 0
             * task_subject : hdjdj
             * task_details : nfnfn
             * priority_id : 1
             * task_type_id : 1
             * task_status_id : 1
             * layer_name : null
             * material_details : null
             * installation_status : null
             * installation_remark : null
             * auto_person : null
             * tansport_name : null
             * truck_no : null
             * lr_no : null
             * mode : null
             * delivery_status : null
             * delivery_remark : null
             * remark : null
             * attachments : null
             * started_on : 16-04-2019 12:21
             * completed_on : 0000-00-00 00:00:00
             * created_on : 2019-04-16 12:21:54
             * updated_on : 0000-00-00 00:00:00
             * status : 1
             * task_type_name : Lead Task (Order)
             * assigned_to_employees : [{"tasks_id":"139","employee_id":"1","status_id":"1","due_date":"2019-04-16 12:21:00","created_at":"1555397514","user_name":"Admin"}]
             * assigned_to_employees_id : ["1"]
             */

            @SerializedName("id")
            private String id;
            @SerializedName("leads_id")
            private Object leadsId;
            @SerializedName("customers_type_id")
            private Object customersTypeId;
            @SerializedName("customer_id")
            private Object customerId;
            @SerializedName("employee_id")
            private String employeeId;
            @SerializedName("activity_id")
            private String activityId;
            @SerializedName("task_subject")
            private String taskSubject;
            @SerializedName("task_details")
            private String taskDetails;
            @SerializedName("priority_id")
            private String priorityId;
            @SerializedName("task_type_id")
            private String taskTypeId;
            @SerializedName("task_status_id")
            private String taskStatusId;
            @SerializedName("layer_name")
            private Object layerName;
            @SerializedName("material_details")
            private Object materialDetails;
            @SerializedName("installation_status")
            private Object installationStatus;
            @SerializedName("installation_remark")
            private Object installationRemark;
            @SerializedName("auto_person")
            private Object autoPerson;
            @SerializedName("tansport_name")
            private Object tansportName;
            @SerializedName("truck_no")
            private Object truckNo;
            @SerializedName("lr_no")
            private Object lrNo;
            @SerializedName("mode")
            private Object mode;
            @SerializedName("delivery_status")
            private Object deliveryStatus;
            @SerializedName("delivery_remark")
            private Object deliveryRemark;
            @SerializedName("remark")
            private Object remark;
            @SerializedName("attachments")
            private Object attachments;
            @SerializedName("started_on")
            private String startedOn;
            @SerializedName("completed_on")
            private String completedOn;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("status")
            private String status;
            @SerializedName("task_type_name")
            private String taskTypeName;
            @SerializedName("assigned_to_employees")
            private List<AssignedToEmployeesBean> assignedToEmployees;
            @SerializedName("assigned_to_employees_id")
            private List<String> assignedToEmployeesId;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public Object getLeadsId() {
                return leadsId;
            }

            public void setLeadsId(Object leadsId) {
                this.leadsId = leadsId;
            }

            public Object getCustomersTypeId() {
                return customersTypeId;
            }

            public void setCustomersTypeId(Object customersTypeId) {
                this.customersTypeId = customersTypeId;
            }

            public Object getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Object customerId) {
                this.customerId = customerId;
            }

            public String getEmployeeId() {
                return employeeId;
            }

            public void setEmployeeId(String employeeId) {
                this.employeeId = employeeId;
            }

            public String getActivityId() {
                return activityId;
            }

            public void setActivityId(String activityId) {
                this.activityId = activityId;
            }

            public String getTaskSubject() {
                return taskSubject;
            }

            public void setTaskSubject(String taskSubject) {
                this.taskSubject = taskSubject;
            }

            public String getTaskDetails() {
                return taskDetails;
            }

            public void setTaskDetails(String taskDetails) {
                this.taskDetails = taskDetails;
            }

            public String getPriorityId() {
                return priorityId;
            }

            public void setPriorityId(String priorityId) {
                this.priorityId = priorityId;
            }

            public String getTaskTypeId() {
                return taskTypeId;
            }

            public void setTaskTypeId(String taskTypeId) {
                this.taskTypeId = taskTypeId;
            }

            public String getTaskStatusId() {
                return taskStatusId;
            }

            public void setTaskStatusId(String taskStatusId) {
                this.taskStatusId = taskStatusId;
            }

            public Object getLayerName() {
                return layerName;
            }

            public void setLayerName(Object layerName) {
                this.layerName = layerName;
            }

            public Object getMaterialDetails() {
                return materialDetails;
            }

            public void setMaterialDetails(Object materialDetails) {
                this.materialDetails = materialDetails;
            }

            public Object getInstallationStatus() {
                return installationStatus;
            }

            public void setInstallationStatus(Object installationStatus) {
                this.installationStatus = installationStatus;
            }

            public Object getInstallationRemark() {
                return installationRemark;
            }

            public void setInstallationRemark(Object installationRemark) {
                this.installationRemark = installationRemark;
            }

            public Object getAutoPerson() {
                return autoPerson;
            }

            public void setAutoPerson(Object autoPerson) {
                this.autoPerson = autoPerson;
            }

            public Object getTansportName() {
                return tansportName;
            }

            public void setTansportName(Object tansportName) {
                this.tansportName = tansportName;
            }

            public Object getTruckNo() {
                return truckNo;
            }

            public void setTruckNo(Object truckNo) {
                this.truckNo = truckNo;
            }

            public Object getLrNo() {
                return lrNo;
            }

            public void setLrNo(Object lrNo) {
                this.lrNo = lrNo;
            }

            public Object getMode() {
                return mode;
            }

            public void setMode(Object mode) {
                this.mode = mode;
            }

            public Object getDeliveryStatus() {
                return deliveryStatus;
            }

            public void setDeliveryStatus(Object deliveryStatus) {
                this.deliveryStatus = deliveryStatus;
            }

            public Object getDeliveryRemark() {
                return deliveryRemark;
            }

            public void setDeliveryRemark(Object deliveryRemark) {
                this.deliveryRemark = deliveryRemark;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public Object getAttachments() {
                return attachments;
            }

            public void setAttachments(Object attachments) {
                this.attachments = attachments;
            }

            public String getStartedOn() {
                return startedOn;
            }

            public void setStartedOn(String startedOn) {
                this.startedOn = startedOn;
            }

            public String getCompletedOn() {
                return completedOn;
            }

            public void setCompletedOn(String completedOn) {
                this.completedOn = completedOn;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getTaskTypeName() {
                return taskTypeName;
            }

            public void setTaskTypeName(String taskTypeName) {
                this.taskTypeName = taskTypeName;
            }

            public List<AssignedToEmployeesBean> getAssignedToEmployees() {
                return assignedToEmployees;
            }

            public void setAssignedToEmployees(List<AssignedToEmployeesBean> assignedToEmployees) {
                this.assignedToEmployees = assignedToEmployees;
            }

            public List<String> getAssignedToEmployeesId() {
                return assignedToEmployeesId;
            }

            public void setAssignedToEmployeesId(List<String> assignedToEmployeesId) {
                this.assignedToEmployeesId = assignedToEmployeesId;
            }

            public static class AssignedToEmployeesBean {
                /**
                 * tasks_id : 139
                 * employee_id : 1
                 * status_id : 1
                 * due_date : 2019-04-16 12:21:00
                 * created_at : 1555397514
                 * user_name : Admin
                 */

                @SerializedName("tasks_id")
                private String tasksId;
                @SerializedName("employee_id")
                private String employeeId;
                @SerializedName("status_id")
                private String statusId;
                @SerializedName("due_date")
                private String dueDate;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("user_name")
                private String userName;

                public String getTasksId() {
                    return tasksId;
                }

                public void setTasksId(String tasksId) {
                    this.tasksId = tasksId;
                }

                public String getEmployeeId() {
                    return employeeId;
                }

                public void setEmployeeId(String employeeId) {
                    this.employeeId = employeeId;
                }

                public String getStatusId() {
                    return statusId;
                }

                public void setStatusId(String statusId) {
                    this.statusId = statusId;
                }

                public String getDueDate() {
                    return dueDate;
                }

                public void setDueDate(String dueDate) {
                    this.dueDate = dueDate;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUserName() {
                    return userName;
                }

                public void setUserName(String userName) {
                    this.userName = userName;
                }
            }
        }

        public static class ChatsBean {
            /**
             * id : 411
             * action_by : 2
             * lead_id : 0
             * task_id : 139
             * created_on : 2019-04-16 12:21:54
             * comment : Vijay created Lead Task (Order) task for date 16-04-2019 12:21 pm and assinged to Admin
             * attachments : null
             * user_name : Vijay
             */

            @SerializedName("id")
            private String id;
            @SerializedName("action_by")
            private String actionBy;
            @SerializedName("lead_id")
            private String leadId;
            @SerializedName("task_id")
            private String taskId;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("comment")
            private String comment;
            @SerializedName("attachments")
            private Object attachments;
            @SerializedName("user_name")
            private String userName;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getActionBy() {
                return actionBy;
            }

            public void setActionBy(String actionBy) {
                this.actionBy = actionBy;
            }

            public String getLeadId() {
                return leadId;
            }

            public void setLeadId(String leadId) {
                this.leadId = leadId;
            }

            public String getTaskId() {
                return taskId;
            }

            public void setTaskId(String taskId) {
                this.taskId = taskId;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Object getAttachments() {
                return attachments;
            }

            public void setAttachments(Object attachments) {
                this.attachments = attachments;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }
        }

        public static class EmployeesBean {
            /**
             * id : 2
             * roles_id : ["2###3","4###6"]
             * name : Vijay
             * email : vijay@innasoft.in
             * mobile : 8019448098
             * password : 6f26adf1f23bb5ab03abd7bfcd504cbf
             * salt : AnCqr7npIP
             * token : 6249442cc474f8e9e10f35aa8b7150e1
             * fcm_token : cP_wpdHNCoQ:APA91bGsGMgyOiNoFhQFmqIHgSxORbhcrrWU3pp9f4fK_wtvJOWYcRu8fHPJriAwojvpPyx1cMZmNNukXX4Tr8-DX7OMqcpYN2_P3lu6vSb3FFl2QaMQ7J-OzxP-sO52flvLBzCFKF9h
             * profile_image : 1544976439_profile_image.jpg
             * created_at : 1543223968
             * updated_at : 1553578142
             * status : 1
             * current_status : Active
             * current_status_bootstrap_class : primary
             * departments : [{"users_id":"2","departments_id":"2","created_at":"1553578142","department_name":"Installation"},{"users_id":"2","departments_id":"4","created_at":"1553578142","department_name":"Sales"}]
             * departments_id : ["2","4"]
             * roles : [{"users_id":"2","departments_id":"2","roles_id":"3","created_at":"1553578142","role_name":"Installation Boy"},{"users_id":"2","departments_id":"4","roles_id":"6","created_at":"1553578142","role_name":"Salesman"}]
             */

            @SerializedName("id")
            private String id;
            @SerializedName("name")
            private String name;
            @SerializedName("email")
            private String email;
            @SerializedName("mobile")
            private String mobile;
            @SerializedName("password")
            private String password;
            @SerializedName("salt")
            private String salt;
            @SerializedName("token")
            private String token;
            @SerializedName("fcm_token")
            private String fcmToken;
            @SerializedName("profile_image")
            private String profileImage;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("status")
            private String status;
            @SerializedName("current_status")
            private String currentStatus;
            @SerializedName("current_status_bootstrap_class")
            private String currentStatusBootstrapClass;
            @SerializedName("roles_id")
            private List<String> rolesId;
            @SerializedName("departments")
            private List<DepartmentsBean> departments;
            @SerializedName("departments_id")
            private List<String> departmentsId;
            @SerializedName("roles")
            private List<RolesBean> roles;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getSalt() {
                return salt;
            }

            public void setSalt(String salt) {
                this.salt = salt;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public String getFcmToken() {
                return fcmToken;
            }

            public void setFcmToken(String fcmToken) {
                this.fcmToken = fcmToken;
            }

            public String getProfileImage() {
                return profileImage;
            }

            public void setProfileImage(String profileImage) {
                this.profileImage = profileImage;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCurrentStatus() {
                return currentStatus;
            }

            public void setCurrentStatus(String currentStatus) {
                this.currentStatus = currentStatus;
            }

            public String getCurrentStatusBootstrapClass() {
                return currentStatusBootstrapClass;
            }

            public void setCurrentStatusBootstrapClass(String currentStatusBootstrapClass) {
                this.currentStatusBootstrapClass = currentStatusBootstrapClass;
            }

            public List<String> getRolesId() {
                return rolesId;
            }

            public void setRolesId(List<String> rolesId) {
                this.rolesId = rolesId;
            }

            public List<DepartmentsBean> getDepartments() {
                return departments;
            }

            public void setDepartments(List<DepartmentsBean> departments) {
                this.departments = departments;
            }

            public List<String> getDepartmentsId() {
                return departmentsId;
            }

            public void setDepartmentsId(List<String> departmentsId) {
                this.departmentsId = departmentsId;
            }

            public List<RolesBean> getRoles() {
                return roles;
            }

            public void setRoles(List<RolesBean> roles) {
                this.roles = roles;
            }

            public static class DepartmentsBean {
                /**
                 * users_id : 2
                 * departments_id : 2
                 * created_at : 1553578142
                 * department_name : Installation
                 */

                @SerializedName("users_id")
                private String usersId;
                @SerializedName("departments_id")
                private String departmentsId;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("department_name")
                private String departmentName;

                public String getUsersId() {
                    return usersId;
                }

                public void setUsersId(String usersId) {
                    this.usersId = usersId;
                }

                public String getDepartmentsId() {
                    return departmentsId;
                }

                public void setDepartmentsId(String departmentsId) {
                    this.departmentsId = departmentsId;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getDepartmentName() {
                    return departmentName;
                }

                public void setDepartmentName(String departmentName) {
                    this.departmentName = departmentName;
                }
            }

            public static class RolesBean {
                /**
                 * users_id : 2
                 * departments_id : 2
                 * roles_id : 3
                 * created_at : 1553578142
                 * role_name : Installation Boy
                 */

                @SerializedName("users_id")
                private String usersId;
                @SerializedName("departments_id")
                private String departmentsId;
                @SerializedName("roles_id")
                private String rolesId;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("role_name")
                private String roleName;

                public String getUsersId() {
                    return usersId;
                }

                public void setUsersId(String usersId) {
                    this.usersId = usersId;
                }

                public String getDepartmentsId() {
                    return departmentsId;
                }

                public void setDepartmentsId(String departmentsId) {
                    this.departmentsId = departmentsId;
                }

                public String getRolesId() {
                    return rolesId;
                }

                public void setRolesId(String rolesId) {
                    this.rolesId = rolesId;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getRoleName() {
                    return roleName;
                }

                public void setRoleName(String roleName) {
                    this.roleName = roleName;
                }
            }
        }
    }
}
