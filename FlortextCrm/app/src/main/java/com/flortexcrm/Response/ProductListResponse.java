package com.flortexcrm.Response;

import java.util.List;

public class ProductListResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"3","name":"Aberdeen 01 Brown - PVC","image":"","packing":"53.82","product_code":"237","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"20","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 19:51:05","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 01 Brown - PVC"},{"id":"4","name":"Aberdeen 02 Beige - ebac","image":"","packing":"53.82","product_code":"231","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"15","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:02","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 02 Beige - ebac"},{"id":"5","name":"Aberdeen 02 Beige - PVC","image":"","packing":"53.82","product_code":"232","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"20","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:03","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 02 Beige - PVC"},{"id":"6","name":"Aberdeen 03 Grey - ebac","image":"","packing":"53.82","product_code":"233","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"15","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:03","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 03 Grey - ebac"},{"id":"7","name":"Aberdeen 04 Blue - ebac","image":"","packing":"53.82","product_code":"234","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"15","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:03","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 04 Blue - ebac"},{"id":"8","name":"Aberdeen 04 Blue - PVC","image":"","packing":"53.82","product_code":"235","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"20","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:03","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 04 Blue - PVC"},{"id":"9","name":"Aberdeen 01 Brown - ebac","image":"","packing":"53.82","product_code":"236","description":"VITOFLOOR  Loop Plie Modular C arpet Tile","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"1","product_group_id":"5","hsn_id":"3","manufacturer_barcode":"","weight_per_box":"15","barcode":"","no_of_pcs_in_box":"20","size":"580 Gms, Pp, 1/10 Guage, 50cm X 50cm","laying_rate":"0","selling_rate":"0","dealer_price":"60","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:16:03","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Aberdeen 01 Brown - ebac"},{"id":"10","name":"Polish Linseed Oil","image":"","packing":"1","product_code":"354","description":"POLISHING LINSEED OIL","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"2","product_group_id":"1","hsn_id":"4","manufacturer_barcode":"","weight_per_box":" ","barcode":"","no_of_pcs_in_box":"0","size":"None","laying_rate":"0","selling_rate":"0","dealer_price":"0","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"0","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:32:32","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Polish Linseed Oil"},{"id":"11","name":"Polithin Sheets","image":"","packing":"1","product_code":"365","description":"Polithin Sheets cover","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"2","product_department_id":"0","product_type_id":"1","product_main_group_id":"2","product_group_id":"1","hsn_id":"5","manufacturer_barcode":"","weight_per_box":" ","barcode":"","no_of_pcs_in_box":"0","size":"None","laying_rate":"0","selling_rate":"0","dealer_price":"0","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"No","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:32:32","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Polithin Sheets"},{"id":"12","name":"Wpc L Profile","image":"","packing":"7","product_code":"246483","description":"MDF LAMINATED ACCESORIES","product_cat_id":"0","product_cat_sub_id":"0","product_cat_child_id":"0","uom_id":"3","product_department_id":"0","product_type_id":"1","product_main_group_id":"2","product_group_id":"1","hsn_id":"6","manufacturer_barcode":"","weight_per_box":" ","barcode":"","no_of_pcs_in_box":"0","size":"None","laying_rate":"0","selling_rate":"0","dealer_price":"80","gst_itc_eligibility":"Inputs","gst_tax":"Taxable","gst_product_type":"Goods","product_laying":"Yes","is_active":"1","minimum_stock":"10","created_on":"2018-12-25 20:32:33","updated_on":"2018-12-25 20:34:50","category_name":null,"category_sub_name":null,"category_child_name":null,"product_name":"Wpc L Profile"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * name : Aberdeen 01 Brown - PVC
         * image :
         * packing : 53.82
         * product_code : 237
         * description : VITOFLOOR  Loop Plie Modular C arpet Tile
         * product_cat_id : 0
         * product_cat_sub_id : 0
         * product_cat_child_id : 0
         * uom_id : 2
         * product_department_id : 0
         * product_type_id : 1
         * product_main_group_id : 1
         * product_group_id : 5
         * hsn_id : 3
         * manufacturer_barcode :
         * weight_per_box : 20
         * barcode :
         * no_of_pcs_in_box : 20
         * size : 580 Gms, Pp, 1/10 Guage, 50cm X 50cm
         * laying_rate : 0
         * selling_rate : 0
         * dealer_price : 60
         * gst_itc_eligibility : Inputs
         * gst_tax : Taxable
         * gst_product_type : Goods
         * product_laying : Yes
         * is_active : 1
         * minimum_stock : 10
         * created_on : 2018-12-25 19:51:05
         * updated_on : 2018-12-25 20:34:50
         * category_name : null
         * category_sub_name : null
         * category_child_name : null
         * product_name : Aberdeen 01 Brown - PVC
         */

        private String id;
        private String name;
        private String image;
        private String packing;
        private String product_code;
        private String description;
        private String product_cat_id;
        private String product_cat_sub_id;
        private String product_cat_child_id;
        private String uom_id;
        private String product_department_id;
        private String product_type_id;
        private String product_main_group_id;
        private String product_group_id;
        private String hsn_id;
        private String manufacturer_barcode;
        private String weight_per_box;
        private String barcode;
        private String no_of_pcs_in_box;
        private String size;
        private String laying_rate;
        private String selling_rate;
        private String dealer_price;
        private String gst_itc_eligibility;
        private String gst_tax;
        private String gst_product_type;
        private String product_laying;
        private String is_active;
        private String minimum_stock;
        private String created_on;
        private String updated_on;
        private Object category_name;
        private Object category_sub_name;
        private Object category_child_name;
        private String product_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPacking() {
            return packing;
        }

        public void setPacking(String packing) {
            this.packing = packing;
        }

        public String getProduct_code() {
            return product_code;
        }

        public void setProduct_code(String product_code) {
            this.product_code = product_code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_cat_id() {
            return product_cat_id;
        }

        public void setProduct_cat_id(String product_cat_id) {
            this.product_cat_id = product_cat_id;
        }

        public String getProduct_cat_sub_id() {
            return product_cat_sub_id;
        }

        public void setProduct_cat_sub_id(String product_cat_sub_id) {
            this.product_cat_sub_id = product_cat_sub_id;
        }

        public String getProduct_cat_child_id() {
            return product_cat_child_id;
        }

        public void setProduct_cat_child_id(String product_cat_child_id) {
            this.product_cat_child_id = product_cat_child_id;
        }

        public String getUom_id() {
            return uom_id;
        }

        public void setUom_id(String uom_id) {
            this.uom_id = uom_id;
        }

        public String getProduct_department_id() {
            return product_department_id;
        }

        public void setProduct_department_id(String product_department_id) {
            this.product_department_id = product_department_id;
        }

        public String getProduct_type_id() {
            return product_type_id;
        }

        public void setProduct_type_id(String product_type_id) {
            this.product_type_id = product_type_id;
        }

        public String getProduct_main_group_id() {
            return product_main_group_id;
        }

        public void setProduct_main_group_id(String product_main_group_id) {
            this.product_main_group_id = product_main_group_id;
        }

        public String getProduct_group_id() {
            return product_group_id;
        }

        public void setProduct_group_id(String product_group_id) {
            this.product_group_id = product_group_id;
        }

        public String getHsn_id() {
            return hsn_id;
        }

        public void setHsn_id(String hsn_id) {
            this.hsn_id = hsn_id;
        }

        public String getManufacturer_barcode() {
            return manufacturer_barcode;
        }

        public void setManufacturer_barcode(String manufacturer_barcode) {
            this.manufacturer_barcode = manufacturer_barcode;
        }

        public String getWeight_per_box() {
            return weight_per_box;
        }

        public void setWeight_per_box(String weight_per_box) {
            this.weight_per_box = weight_per_box;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getNo_of_pcs_in_box() {
            return no_of_pcs_in_box;
        }

        public void setNo_of_pcs_in_box(String no_of_pcs_in_box) {
            this.no_of_pcs_in_box = no_of_pcs_in_box;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getLaying_rate() {
            return laying_rate;
        }

        public void setLaying_rate(String laying_rate) {
            this.laying_rate = laying_rate;
        }

        public String getSelling_rate() {
            return selling_rate;
        }

        public void setSelling_rate(String selling_rate) {
            this.selling_rate = selling_rate;
        }

        public String getDealer_price() {
            return dealer_price;
        }

        public void setDealer_price(String dealer_price) {
            this.dealer_price = dealer_price;
        }

        public String getGst_itc_eligibility() {
            return gst_itc_eligibility;
        }

        public void setGst_itc_eligibility(String gst_itc_eligibility) {
            this.gst_itc_eligibility = gst_itc_eligibility;
        }

        public String getGst_tax() {
            return gst_tax;
        }

        public void setGst_tax(String gst_tax) {
            this.gst_tax = gst_tax;
        }

        public String getGst_product_type() {
            return gst_product_type;
        }

        public void setGst_product_type(String gst_product_type) {
            this.gst_product_type = gst_product_type;
        }

        public String getProduct_laying() {
            return product_laying;
        }

        public void setProduct_laying(String product_laying) {
            this.product_laying = product_laying;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getMinimum_stock() {
            return minimum_stock;
        }

        public void setMinimum_stock(String minimum_stock) {
            this.minimum_stock = minimum_stock;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }

        public Object getCategory_name() {
            return category_name;
        }

        public void setCategory_name(Object category_name) {
            this.category_name = category_name;
        }

        public Object getCategory_sub_name() {
            return category_sub_name;
        }

        public void setCategory_sub_name(Object category_sub_name) {
            this.category_sub_name = category_sub_name;
        }

        public Object getCategory_child_name() {
            return category_child_name;
        }

        public void setCategory_child_name(Object category_child_name) {
            this.category_child_name = category_child_name;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }
    }
}
