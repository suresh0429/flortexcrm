package com.flortexcrm.Response;

import java.util.List;

public class LeadSpinnerResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"lead_code":"20032019001","industries":[{"id":"1","name":"APARTMENT"},{"id":"2","name":"BUILDER"},{"id":"3","name":"BUILDING ELEVATION"},{"id":"4","name":"Commercial Space"},{"id":"5","name":"GYM"},{"id":"6","name":"HOSPITAL"},{"id":"7","name":"Hotel"},{"id":"8","name":"IT INDUSTRIES"},{"id":"9","name":"OFFICE"},{"id":"10","name":"PHARMA"},{"id":"11","name":"PUBS"},{"id":"12","name":"RESIDENCE"},{"id":"13","name":"RESTURANT"},{"id":"14","name":"SCHOOL"},{"id":"15","name":"VILLAS"}],"ratings":[{"id":"1","name":"COLD ENQUIRY"},{"id":"2","name":"FOLLOW UP"},{"id":"3","name":"HOT ENQUIRY"}],"source_type":[{"id":"1","name":"Architecture"},{"id":"5","name":"Client"},{"id":"2","name":"Contractor"},{"id":"3","name":"Dealer"},{"id":"4","name":"Other Source"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lead_code : 20032019001
         * industries : [{"id":"1","name":"APARTMENT"},{"id":"2","name":"BUILDER"},{"id":"3","name":"BUILDING ELEVATION"},{"id":"4","name":"Commercial Space"},{"id":"5","name":"GYM"},{"id":"6","name":"HOSPITAL"},{"id":"7","name":"Hotel"},{"id":"8","name":"IT INDUSTRIES"},{"id":"9","name":"OFFICE"},{"id":"10","name":"PHARMA"},{"id":"11","name":"PUBS"},{"id":"12","name":"RESIDENCE"},{"id":"13","name":"RESTURANT"},{"id":"14","name":"SCHOOL"},{"id":"15","name":"VILLAS"}]
         * ratings : [{"id":"1","name":"COLD ENQUIRY"},{"id":"2","name":"FOLLOW UP"},{"id":"3","name":"HOT ENQUIRY"}]
         * source_type : [{"id":"1","name":"Architecture"},{"id":"5","name":"Client"},{"id":"2","name":"Contractor"},{"id":"3","name":"Dealer"},{"id":"4","name":"Other Source"}]
         */

        private String lead_code;
        private List<IndustriesBean> industries;
        private List<RatingsBean> ratings;
        private List<SourceTypeBean> source_type;

        public String getLead_code() {
            return lead_code;
        }

        public void setLead_code(String lead_code) {
            this.lead_code = lead_code;
        }

        public List<IndustriesBean> getIndustries() {
            return industries;
        }

        public void setIndustries(List<IndustriesBean> industries) {
            this.industries = industries;
        }

        public List<RatingsBean> getRatings() {
            return ratings;
        }

        public void setRatings(List<RatingsBean> ratings) {
            this.ratings = ratings;
        }

        public List<SourceTypeBean> getSource_type() {
            return source_type;
        }

        public void setSource_type(List<SourceTypeBean> source_type) {
            this.source_type = source_type;
        }

        public static class IndustriesBean {
            /**
             * id : 1
             * name : APARTMENT
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class RatingsBean {
            /**
             * id : 1
             * name : COLD ENQUIRY
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class SourceTypeBean {
            /**
             * id : 1
             * name : Architecture
             */

            private String id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
