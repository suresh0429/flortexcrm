package com.flortexcrm.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadProductsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"149","leads_id":"12","main_group_id":"16","products_id":"154","remark":"tyutv","pack":"12","quantity":"12","box":"1.00","packing":"0.00","transport":"0.00","uom_id":"2","rate":"40.00","tax_rate":"12.00","created_on":"2019-02-25 21:25:20","updated_on":"2019-02-25 21:25:20","product_group":"Allegra","product_name":"10087 Checkmate (rustic Brown)","dealer_price":"40","hsn_code":"57033090","uom":"SQF"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 149
         * leads_id : 12
         * main_group_id : 16
         * products_id : 154
         * remark : tyutv
         * pack : 12
         * quantity : 12
         * box : 1.00
         * packing : 0.00
         * transport : 0.00
         * uom_id : 2
         * rate : 40.00
         * tax_rate : 12.00
         * created_on : 2019-02-25 21:25:20
         * updated_on : 2019-02-25 21:25:20
         * product_group : Allegra
         * product_name : 10087 Checkmate (rustic Brown)
         * dealer_price : 40
         * hsn_code : 57033090
         * uom : SQF
         */

        @SerializedName("id")
        private String id;
        @SerializedName("leads_id")
        private String leadsId;
        @SerializedName("main_group_id")
        private String mainGroupId;
        @SerializedName("products_id")
        private String productsId;
        @SerializedName("remark")
        private String remark;
        @SerializedName("pack")
        private String pack;
        @SerializedName("quantity")
        private String quantity;
        @SerializedName("box")
        private String box;
        @SerializedName("packing")
        private String packing;
        @SerializedName("transport")
        private String transport;
        @SerializedName("uom_id")
        private String uomId;
        @SerializedName("rate")
        private String rate;
        @SerializedName("tax_rate")
        private String taxRate;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("updated_on")
        private String updatedOn;
        @SerializedName("product_group")
        private String productGroup;
        @SerializedName("product_name")
        private String productName;
        @SerializedName("dealer_price")
        private String dealerPrice;
        @SerializedName("hsn_code")
        private String hsnCode;
        @SerializedName("uom")
        private String uom;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLeadsId() {
            return leadsId;
        }

        public void setLeadsId(String leadsId) {
            this.leadsId = leadsId;
        }

        public String getMainGroupId() {
            return mainGroupId;
        }

        public void setMainGroupId(String mainGroupId) {
            this.mainGroupId = mainGroupId;
        }

        public String getProductsId() {
            return productsId;
        }

        public void setProductsId(String productsId) {
            this.productsId = productsId;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getPack() {
            return pack;
        }

        public void setPack(String pack) {
            this.pack = pack;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getBox() {
            return box;
        }

        public void setBox(String box) {
            this.box = box;
        }

        public String getPacking() {
            return packing;
        }

        public void setPacking(String packing) {
            this.packing = packing;
        }

        public String getTransport() {
            return transport;
        }

        public void setTransport(String transport) {
            this.transport = transport;
        }

        public String getUomId() {
            return uomId;
        }

        public void setUomId(String uomId) {
            this.uomId = uomId;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getTaxRate() {
            return taxRate;
        }

        public void setTaxRate(String taxRate) {
            this.taxRate = taxRate;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getProductGroup() {
            return productGroup;
        }

        public void setProductGroup(String productGroup) {
            this.productGroup = productGroup;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getDealerPrice() {
            return dealerPrice;
        }

        public void setDealerPrice(String dealerPrice) {
            this.dealerPrice = dealerPrice;
        }

        public String getHsnCode() {
            return hsnCode;
        }

        public void setHsnCode(String hsnCode) {
            this.hsnCode = hsnCode;
        }

        public String getUom() {
            return uom;
        }

        public void setUom(String uom) {
            this.uom = uom;
        }
    }
}
