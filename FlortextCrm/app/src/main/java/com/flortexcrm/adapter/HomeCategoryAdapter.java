package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.activity.CalenderViewActivity;
import com.flortexcrm.activity.CustomersActivity;
import com.flortexcrm.activity.LeadsActivity;
import com.flortexcrm.activity.StocksActivity;
import com.flortexcrm.activity.TaskActivity;
import com.flortexcrm.fragments.HomeFragment;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;
import com.flortexcrm.model.HomeCategoryModel;


public class HomeCategoryAdapter extends  RecyclerView.Adapter<HomeCategoryAdapter.HomeCategoryHolder>
{
    private HomeCategoryModel[] categoryData;
    HomeFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont,boldFont;

    public HomeCategoryAdapter( HomeCategoryModel[] categoryData, HomeFragment context, int resource)
    {
        this.categoryData = categoryData;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater)   context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.bold));

    }

    @NonNull
    @Override
    public HomeCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View layout = li.inflate(resource, viewGroup, false);
        HomeCategoryHolder categ = new HomeCategoryHolder(layout);
        return categ;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoryHolder holder, int position)
    {

      //  holder.frame_week.setCardBackgroundColor(Color.parseColor("#34444C"));

        holder.category_title.setText(categoryData[position].getCateg_name());
        holder.category_title.setTypeface(boldFont);
        holder.img_categroy.setImageResource(categoryData[position].getCateg_image());

        holder.setItemClickListener(new CrmListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                if(pos==0)
                {
                    context.startActivity(new Intent(context.getActivity(), CalenderViewActivity.class));
                }
                else if(pos==1)
                {
                    context.startActivity(new Intent(context.getActivity(), LeadsActivity.class));
                }
                else if(pos==2)
                {
                    context.startActivity(new Intent(context.getActivity(), TaskActivity.class));
                }
                else if(pos==3)
                {
                    context.startActivity(new Intent(context.getActivity(), CustomersActivity.class));
                }
                else if (pos==4)
                {
                    context.startActivity(new Intent(context.getActivity(), StocksActivity.class));
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryData.length;
    }


   //Holder Category
    public class HomeCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public ImageView img_categroy;
        public CardView frame_week;
        public TextView category_title,member_count;
        CrmListItemClickListener crmListItemClickListener;

        public HomeCategoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            img_categroy = itemView.findViewById(R.id.img_categroy);
            category_title = itemView.findViewById(R.id.category_title);
            frame_week = itemView.findViewById(R.id.frame_week);
        }

        @Override
        public void onClick(View view) {
            this.crmListItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.crmListItemClickListener = ic;
        }
    }
}
