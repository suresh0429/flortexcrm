package com.flortexcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.BankDetailResponse;
import com.flortexcrm.Response.DocumentsResponse;
import com.flortexcrm.utility.Dialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.MyViewHolder> {
    private Context context;
    private List<DocumentsResponse.DataBean> homeList;
    Typeface regularFont, boldFont;
    private Activity parentActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtName,txtDate;
        ImageView imgPath;

        public MyViewHolder(View view) {
            super(view);

            txtName = (TextView) view.findViewById(R.id.txtName);
            txtDate = (TextView) view.findViewById(R.id.txtDate);
            imgPath = (ImageView) view.findViewById(R.id.imgPath);




        }
    }

   /* public DocumentsAdapter(Context mContext, List<DocumentsResponse.DataBean> homekitchenList) {
        this.context = mContext;
        this.homeList = homekitchenList;
    }*/

    public DocumentsAdapter(Context context, List<DocumentsResponse.DataBean> homeList, Activity parentActivity) {
        this.context = context;
        this.homeList = homeList;
        this.parentActivity = parentActivity;
    }

    @Override
    public DocumentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_documents, parent, false);

        return new DocumentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DocumentsAdapter.MyViewHolder holder, final int position) {
        final DocumentsResponse.DataBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));


        holder.txtName.setText(home.getDocumentName());
        holder.txtDate.setText(home.getCreatedOn());
        holder.txtDate.setTypeface(regularFont);
        holder.txtName.setTypeface(regularFont);

        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setBackgroundColor(Color.BLACK);
        imagePopup.setFullScreen(true);
        imagePopup.setHideCloseIcon(true);
        imagePopup.setImageOnClickClose(true);


        Picasso.with(context).load(RetrofitClient.BASE_IMAGE_URL+home.getDocumentPath()).into(holder.imgPath);

        // to download the image from url if you want different resolution or different image
        imagePopup.initiatePopupWithPicasso(RetrofitClient.BASE_IMAGE_URL+home.getDocumentPath());


        holder.imgPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.viewPopup();
            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }



}