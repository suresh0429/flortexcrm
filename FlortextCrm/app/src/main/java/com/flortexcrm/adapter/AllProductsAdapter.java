package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.ProductListResponse;
import com.flortexcrm.activity.ProductsActivity;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AllProductsAdapter extends RecyclerView.Adapter<AllProductsAdapter.TaskHolder>  {

    public List<ProductListResponse.DataBean> allProdulistModels;
    ProductsActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont;


    public AllProductsAdapter(List<ProductListResponse.DataBean> allProdulistModels, ProductsActivity context, int resource) {
        this.allProdulistModels = allProdulistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }


    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        TaskHolder slh = new TaskHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final TaskHolder holder, final int position)
    {
         holder.prodCode_text.setText(allProdulistModels.get(position).getProduct_code());
         holder.name_text.setText(allProdulistModels.get(position).getName());
         holder.minStock_text.setText(allProdulistModels.get(position).getMinimum_stock());
          String timedate = parseDateToddMMyyyy(allProdulistModels.get(position).getCreated_on());
         holder.createdDate_text.setText(timedate);

         holder.prodCode_title_text.setTypeface(boldFont);
         holder.prodCode_text.setTypeface(regularFont);
        holder.name_title.setTypeface(boldFont);
        holder.name_text.setTypeface(regularFont);
        holder.minStock_title.setTypeface(boldFont);
        holder.minStock_text.setTypeface(regularFont);
        holder.createdDate_title.setTypeface(boldFont);
        holder.createdDate_text.setTypeface(regularFont);
        holder.viewMore_text.setTypeface(regularFont);

        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
            }
        });

         holder.viewMore_text.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v)
             {

             }
         });
    }

    @Override
    public int getItemCount() {
        return this.allProdulistModels.size();
    }

    ////Holder
    public class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView prodCode_title_text,prodCode_text,name_title,name_text,minStock_title,minStock_text,createdDate_title,createdDate_text,viewMore_text;
        ImageView img_product;
        CrmListItemClickListener allCRMItemClickListener;

        public TaskHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            img_product = itemView.findViewById(R.id.img_product);
            prodCode_title_text = itemView.findViewById(R.id.prodCode_title_text);
            prodCode_text = itemView.findViewById(R.id.prodCode_text);
            name_title = itemView.findViewById(R.id.name_title);
            name_text = itemView.findViewById(R.id.name_text);
            minStock_title = itemView.findViewById(R.id.minStock_title);
            minStock_text = itemView.findViewById(R.id.minStock_text);
            createdDate_title = itemView.findViewById(R.id.createdDate_title);
            createdDate_text = itemView.findViewById(R.id.createdDate_text);
            viewMore_text = itemView.findViewById(R.id.viewMore_text);
           }

        @Override
        public void onClick(View view) {
            this.allCRMItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.allCRMItemClickListener = ic;
        }
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
