package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.model.CalendarModel;

import java.util.ArrayList;
import java.util.List;

public class EventsAdapter extends ArrayAdapter<CalendarModel> {

    List<CalendarModel> calendarModels = new ArrayList<CalendarModel>();

    public EventsAdapter(Context context, int textViewResourceId, List<CalendarModel> objects) {
        super(context, textViewResourceId, objects);
        calendarModels = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_view_items, null);
        TextView textEvent = (TextView) v.findViewById(R.id.txtEvent);
        TextView textDate = (TextView) v.findViewById(R.id.updatedate);
        View viewBg = (View) v.findViewById(R.id.view);
        textEvent.setText(calendarModels.get(position).getEvent_data());
        textDate.setText("Updated on : "+calendarModels.get(position).getUpdatedDate());

        if (calendarModels.get(position).getStatus().equalsIgnoreCase("NEW")){
            viewBg.setBackgroundColor(Color.parseColor("#4bbc42"));
            textDate.setTextColor(Color.parseColor("#4bbc42"));
        }else if (calendarModels.get(position).getStatus().equalsIgnoreCase("OPEN")){

            viewBg.setBackgroundColor(Color.parseColor("#95319b"));
            textDate.setTextColor(Color.parseColor("#95319b"));
        }else {
            viewBg.setBackgroundColor(Color.parseColor("#ABABAB"));
            textDate.setTextColor(Color.parseColor("#ABABAB"));
        }

        return v;

    }

}
