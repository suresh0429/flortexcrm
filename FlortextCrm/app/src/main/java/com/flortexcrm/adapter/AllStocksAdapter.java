package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.StockResponse;
import com.flortexcrm.activity.StocksActivity;
import com.flortexcrm.filter.CustomFilterforStockList;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AllStocksAdapter extends RecyclerView.Adapter<AllStocksAdapter.StocksHolder> implements Filterable {

    public ArrayList<StockResponse.DataBean> allStockslistModels, filterList;
    CustomFilterforStockList filter;
    StocksActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont;


    public AllStocksAdapter(ArrayList<StockResponse.DataBean> allStockslistModels, StocksActivity context, int resource) {
        this.allStockslistModels = allStockslistModels;
        this.context = context;
        this.resource = resource;
        this.filterList = allStockslistModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }


    @Override
    public StocksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        StocksHolder slh = new StocksHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final StocksHolder holder, final int position) {

        holder.productName_text.setText(allStockslistModels.get(position).getProduct_name());
        holder.groupName_text.setText(allStockslistModels.get(position).getGroupName());
        holder.balancQty_text.setText(allStockslistModels.get(position).getBalance_qty());
        holder.pendSO_QTY_text.setText(allStockslistModels.get(position).getPendingSOQty());
        holder.pendPO_QTY_text.setText(allStockslistModels.get(position).getPendingPOQty());
        holder.blockedQty_text.setText(allStockslistModels.get(position).getBlockQty());
        holder.totalQty_text.setText(allStockslistModels.get(position).getTotalQty());
        holder.total_text.setText(allStockslistModels.get(position).getTotal());

        holder.productName_title.setTypeface(boldFont);
        holder.productName_text.setTypeface(regularFont);
        holder.groupName_title.setTypeface(boldFont);
        holder.groupName_text.setTypeface(regularFont);
        holder.balancQty_title.setTypeface(boldFont);
        holder.balancQty_text.setTypeface(regularFont);
        holder.pendSO_QTY_title.setTypeface(boldFont);
        holder.pendSO_QTY_text.setTypeface(regularFont);
        //holder.pendPO_QTY_title.setTypeface(boldFont);
        holder.pendPO_QTY_text.setTypeface(regularFont);
        holder.blockedQty_title.setTypeface(boldFont);
        holder.blockedQty_text.setTypeface(regularFont);
        holder.totalQty_title.setTypeface(boldFont);
        holder.totalQty_text.setTypeface(regularFont);
        holder.pend_QTY.setTypeface(boldFont);
        holder.total_title.setTypeface(boldFont);
        holder.total_text.setTypeface(regularFont);


        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.allStockslistModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforStockList(filterList, this);
        }

        return filter;
    }


    ////Holder
    public class StocksHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView productName_title, productName_text, groupName_title, groupName_text, balancQty_title, balancQty_text, pendSO_QTY_title, pendSO_QTY_text, pendPO_QTY_title,
                pendPO_QTY_text, blockedQty_title, blockedQty_text, totalQty_title, totalQty_text, pend_QTY, total_title, total_text;
        CrmListItemClickListener allCRMItemClickListener;

        public StocksHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            productName_title = itemView.findViewById(R.id.productName_title);
            productName_text = itemView.findViewById(R.id.productName_text);
            groupName_title = itemView.findViewById(R.id.groupName_title);
            groupName_text = itemView.findViewById(R.id.groupName_text);
            balancQty_title = itemView.findViewById(R.id.balancQty_title);
            balancQty_text = itemView.findViewById(R.id.balancQty_text);
            pendSO_QTY_title = itemView.findViewById(R.id.pendSO_QTY_title);
            pendSO_QTY_text = itemView.findViewById(R.id.pendSO_QTY_text);
            //pendPO_QTY_title = itemView.findViewById(R.id.pendPO_QTY_title);
            pendPO_QTY_text = itemView.findViewById(R.id.pendPO_QTY_text);
            blockedQty_title = itemView.findViewById(R.id.blockedQty_title);
            blockedQty_text = itemView.findViewById(R.id.blockedQty_text);
            totalQty_title = itemView.findViewById(R.id.totalQty_title);
            totalQty_text = itemView.findViewById(R.id.totalQty_text);
            pend_QTY = itemView.findViewById(R.id.pend_QTY);
            total_title = itemView.findViewById(R.id.total_title);
            total_text = itemView.findViewById(R.id.total_text);

        }

        @Override
        public void onClick(View view) {
            this.allCRMItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.allCRMItemClickListener = ic;
        }
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd-MM-yyyy HH:mm";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
