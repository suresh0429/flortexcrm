package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.CustomerResponse;
import com.flortexcrm.activity.BankDetailsActivity;
import com.flortexcrm.activity.ContactPersonActivity;
import com.flortexcrm.activity.CustomersActivity;
import com.flortexcrm.activity.DocumentsActivity;
import com.flortexcrm.activity.AddGstDetailsActivity;
import com.flortexcrm.activity.GSTDetailsActivity;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class AllCustomerAdapter extends RecyclerView.Adapter<AllCustomerAdapter.CustomerHolder> {

    public List<CustomerResponse.DataBean> allCustlistModels;
    CustomersActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont;


    public AllCustomerAdapter(ArrayList<CustomerResponse.DataBean> allCustlistModels, CustomersActivity context, int resource) {
        this.allCustlistModels = allCustlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }


    @Override
    public CustomerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CustomerHolder slh = new CustomerHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CustomerHolder holder, final int position) {


        String salution = allCustlistModels.get(position).getSalutation();
        holder.cust_code_text.setText(allCustlistModels.get(position).getCustomer_code());
        holder.custName_text.setText(allCustlistModels.get(position).getName());
//         holder.email_text.setText(allCustlistModels.get(position).email);
        holder.mobile_text.setText(allCustlistModels.get(position).getMobile());

        holder.cust_title_text.setTypeface(boldFont);
        holder.cust_code_text.setTypeface(regularFont);
        holder.custName_title.setTypeface(boldFont);
        holder.custName_text.setTypeface(regularFont);
        // holder.email_title.setTypeface(boldFont);
        // holder.email_text.setTypeface(regularFont);
        holder.mobile_title.setTypeface(boldFont);
        holder.mobile_text.setTypeface(regularFont);
        //  holder.rating_title.setTypeface(boldFont);
        //  holder.rating_text.setTypeface(regularFont);
        holder.mobile_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+allCustlistModels.get(position).getMobile()));
                context.startActivity(intent);
            }
        });

        holder.txtContactPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ContactPersonActivity.class);
                intent.putExtra("customerId", allCustlistModels.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.txtGstDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddGstDetailsActivity.class);
                intent.putExtra("customerId", allCustlistModels.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.txtBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BankDetailsActivity.class);
                intent.putExtra("customerId", allCustlistModels.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.txtDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DocumentsActivity.class);
                intent.putExtra("customerId", allCustlistModels.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.allCustlistModels.size();
    }


    ////Holder
    public class CustomerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtContactPerson, txtGstDetails, txtBankDetails, txtDocuments, cust_title_text, cust_code_text, custName_title, custName_text, email_title, email_text, mobile_title, mobile_text;
        CrmListItemClickListener allCRMItemClickListener;

               public CustomerHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cust_title_text = itemView.findViewById(R.id.cust_title_text);
            cust_code_text = itemView.findViewById(R.id.cust_code_text);
            custName_title = itemView.findViewById(R.id.custName_title);
            custName_text = itemView.findViewById(R.id.custName_text);
            mobile_title = itemView.findViewById(R.id.mobile_title);
            mobile_text = itemView.findViewById(R.id.mobile_text);

            txtContactPerson = itemView.findViewById(R.id.txtContactPerson);
            txtGstDetails = itemView.findViewById(R.id.txtGstDetails);
            txtBankDetails = itemView.findViewById(R.id.txtBankDetails);
            txtDocuments = itemView.findViewById(R.id.txtDocuments);


        }


        @Override
        public void onClick(View view) {
            this.allCRMItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.allCRMItemClickListener = ic;
        }


    }


    public void updateList(List<CustomerResponse.DataBean> list) {
        allCustlistModels = list;
        notifyDataSetChanged();
    }


}
