package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.activity.DocumentsActivity;
import com.flortexcrm.activity.TaskActivity;
import com.flortexcrm.activity.TaskDetailsActivity;
import com.flortexcrm.filter.CustomFilterforLeadsList;
import com.flortexcrm.filter.CustomFilterforTasksList;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class AllTaskAdapter extends RecyclerView.Adapter<AllTaskAdapter.TaskHolder> implements Filterable {

    public ArrayList<TaskResponse.DataBean> allTasklistModels,filterList;
    TaskActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont;
    CustomFilterforTasksList filter;


    public AllTaskAdapter(ArrayList<TaskResponse.DataBean> allTasklistModels, TaskActivity context, int resource) {
        this.allTasklistModels = allTasklistModels;
        this.context = context;
        this.resource = resource;
        this.filterList = allTasklistModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }


    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        TaskHolder slh = new TaskHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final TaskHolder holder, final int position) {


        String timedate = allTasklistModels.get(position).getCreatedOn();

        // StringTokenizer stringTokenizer = timedate

        String strDateTime[] = StringUtils.split(timedate);
        String Date = strDateTime[0];
        String Time = strDateTime[1];

        //holder.creaedate_text.setText(timedate);
        holder.creaedate_text.setText(Date);
        holder.creaetime_text.setText(Time);
        holder.status_text.setText(allTasklistModels.get(position).getStatus());
        holder.lead_code_text.setText(allTasklistModels.get(position).getLeadCode());
        holder.assignBy_text.setText(allTasklistModels.get(position).getEmployeeName());
        holder.suubejctTask_text.setText(allTasklistModels.get(position).getTaskSubject());
        holder.type_text.setText(allTasklistModels.get(position).getType());
        holder.priority_text.setText(allTasklistModels.get(position).getPriority());

        holder.creaetime_text.setTypeface(regularFont);
        holder.creaedate_text.setTypeface(regularFont);
        holder.status_text.setTypeface(regularFont);
        holder.lead_title_text.setTypeface(boldFont);
        holder.lead_code_text.setTypeface(regularFont);
        holder.assignBy_title.setTypeface(boldFont);
        holder.assignBy_text.setTypeface(regularFont);
        holder.suubejctTask_title.setTypeface(boldFont);
        holder.suubejctTask_text.setTypeface(regularFont);
        holder.type_title.setTypeface(boldFont);
        holder.type_text.setTypeface(regularFont);
        holder.priority_title.setTypeface(boldFont);
        holder.priority_text.setTypeface(regularFont);


        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                TaskResponse.DataBean dataBean = allTasklistModels.get(pos);

                Intent intent = new Intent(context, TaskDetailsActivity.class);
                intent.putExtra("taskId", dataBean.getId());
                intent.putExtra("activity","TaskListAdapter");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.allTasklistModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforTasksList(filterList, this);
        }

        return filter;
    }


    ////Holder
    public class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView creaedate_text, status_text, lead_title_text, lead_code_text, assignBy_title, assignBy_text,suubejctTask_title, suubejctTask_text, type_title, type_text, priority_title, priority_text,creaetime_text;
        CrmListItemClickListener allCRMItemClickListener;

        public TaskHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            creaedate_text = itemView.findViewById(R.id.creaedate_text);
            status_text = itemView.findViewById(R.id.status_text);
            lead_title_text = itemView.findViewById(R.id.lead_title_text);
            lead_code_text = itemView.findViewById(R.id.lead_code_text);
            assignBy_title = itemView.findViewById(R.id.assignBy_title);
            assignBy_text = itemView.findViewById(R.id.assignBy_text);
            suubejctTask_title = itemView.findViewById(R.id.suubejctTask_title);
            suubejctTask_text = itemView.findViewById(R.id.suubejctTask_text);
            type_title = itemView.findViewById(R.id.type_title);
            type_text = itemView.findViewById(R.id.type_text);
            priority_title = itemView.findViewById(R.id.priority_title);
            priority_text = itemView.findViewById(R.id.priority_text);
            creaetime_text = itemView.findViewById(R.id.creaetime_text);


        }

        @Override
        public void onClick(View view) {
            this.allCRMItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.allCRMItemClickListener = ic;
        }
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd-MM-yyyy HH:mm";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
