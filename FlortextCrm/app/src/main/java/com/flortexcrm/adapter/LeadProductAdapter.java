package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.AddLeadProductResponse;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.Response.LeadProductsResponse;
import com.flortexcrm.Response.ProductsResponse;
import com.flortexcrm.activity.AddProductActivity;
import com.flortexcrm.activity.EditProductActivity;
import com.flortexcrm.activity.LeadProductsActivity;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadProductAdapter extends RecyclerView.Adapter<LeadProductAdapter.MyViewHolder> {
    private Context context;
    private List<LeadProductsResponse.DataBean> homeList;
    Typeface regularFont, boldFont;
    ArrayList<SpinnerModel> productsResponseArrayList ;
    public static String lead_id;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtBox,txtQty,txtRate;
        TextView txtProductName;
        ImageView img_edit,img_delete;

        public MyViewHolder(View view) {
            super(view);

            txtProductName = (TextView) view.findViewById(R.id.txtProductName);
            txtBox = (TextView) view.findViewById(R.id.txtBox);
            txtQty = (TextView) view.findViewById(R.id.txtQty);
            txtRate = (TextView) view.findViewById(R.id.txtRate);
            img_edit=view.findViewById(R.id.img_edit);
            img_delete=view.findViewById(R.id.img_delete);



        }
    }

    public LeadProductAdapter(Context mContext, List<LeadProductsResponse.DataBean> homekitchenList) {
        this.context = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public LeadProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_leadproduct, parent, false);

        return new LeadProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LeadProductAdapter.MyViewHolder holder, final int position) {
        final LeadProductsResponse.DataBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        holder.txtBox.setText(": "+home.getBox());
        holder.txtQty.setText(": "+home.getQuantity());
        holder.txtRate.setText(": "+home.getRate());


        // holder.txtName.setTypeface(regularFont);
        holder.txtProductName.setTypeface(regularFont);
        holder.txtBox.setTypeface(regularFont);
        holder.txtQty.setTypeface(regularFont);
        holder.txtRate.setTypeface(regularFont);

       /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, fruits);
        //Getting the instance of AutoCompleteTextView
        holder.txtProductName.setThreshold(1);//will start working from first character
        holder.txtProductName.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        holder.txtProductName.setTextColor(Color.RED);*/
        holder.txtProductName.setText(home.getProductName());
       // productName(holder.txtProductName);
        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, EditProductActivity.class);
                intent.putExtra("leadId",home.getLeadsId());
                intent.putExtra("Box",home.getBox());
                intent.putExtra("Rate",home.getRate());
                intent.putExtra("Quantity",home.getQuantity());
                intent.putExtra("p_id",home.getProductName());
                intent.putExtra("productId",home.getProductsId());
                intent.putExtra("Id",home.getId());

                // calculation for packing value
                Double resultPacking = Double.parseDouble(home.getQuantity()) / Double.parseDouble(home.getBox());

                intent.putExtra("packing",resultPacking);
                context.startActivity(intent);
            }
        });

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId= PrefUtils.getUserId(context);
                delete(userId,home.getLeadsId(),home.getId());

            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


private void delete(String userId, final String leadsId, String id){

    if (Connectivity.isConnected(context)) {
        Dialog.showProgressBar(context, "Loading...");
        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().deleteLeadProduct(RetrofitClient.BASE_URL + "users/" + userId + "/leads/" + leadsId + "/products/"+id+"/delete");
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {
                if (response.isSuccessful()) ;
                FcmResponse addLeadProductResponse = response.body();
                if (addLeadProductResponse.getStatus().equals("10100")) {

                    Dialog.hideProgressBar();

                    Toast.makeText(context, addLeadProductResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, LeadProductsActivity.class);
                    intent.putExtra("leadId", leadsId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Dialog.hideProgressBar();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
}

