package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.ContactPersonResponse;
import com.flortexcrm.Response.TaskDetailResponse;

import java.util.List;

public class ContactPersonAdapter extends RecyclerView.Adapter<ContactPersonAdapter.MyViewHolder> {
    private Context context;
    private List<ContactPersonResponse.DataBean> homeList;
    Typeface regularFont, boldFont;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtActive,txtNameTitle,txtName,txtRoleTitle,txtRole,txtMobileTitle,txtMobile,txtEmailTitle,txtEmail,txtEmailNotify,txtSmsNotify;
        TextView emailSwitch,smsSwitch;

        public MyViewHolder(View view) {
            super(view);

            txtActive = (TextView) view.findViewById(R.id.txtActive);
            txtNameTitle = (TextView) view.findViewById(R.id.txtNameTitle);
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtRoleTitle = (TextView) view.findViewById(R.id.txtRoleTitle);
            txtRole = (TextView) view.findViewById(R.id.txtRole);
            txtMobileTitle = (TextView) view.findViewById(R.id.txtMobileTitle);
            txtMobile = (TextView) view.findViewById(R.id.txtMobile);
            txtEmailTitle = (TextView) view.findViewById(R.id.txtEmailTitle);
            txtEmail = (TextView) view.findViewById(R.id.txtEmail);
            txtEmailNotify = (TextView) view.findViewById(R.id.txtEmailNotify);
            txtSmsNotify = (TextView) view.findViewById(R.id.txtSmsNotify);

            emailSwitch = (TextView) view.findViewById(R.id.emailSwitch);
            smsSwitch = (TextView) view.findViewById(R.id.smsSwitch);



        }
    }

    public ContactPersonAdapter(Context mContext, List<ContactPersonResponse.DataBean> homekitchenList) {
        this.context = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public ContactPersonAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contat_person, parent, false);

        return new ContactPersonAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ContactPersonAdapter.MyViewHolder holder, final int position) {
        final ContactPersonResponse.DataBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));


        if (home.getIsPrimary().equals("1")){
            holder.txtActive.setText("Active");
        }else {
            holder.txtActive.setText("In Active");
        }


        holder.txtName.setText(home.getName());
        holder.txtRole.setText(home.getRoleName());
        holder.txtMobile.setText(home.getMobile());
        holder.txtEmail.setText(home.getEmail());
        holder.smsSwitch.setText(home.getSmsOpt());
        holder.emailSwitch.setText(home.getEmailOpt());


        // holder.txtName.setTypeface(regularFont);
        holder.txtActive.setTypeface(regularFont);
        holder.txtRole.setTypeface(regularFont);
        holder.txtName.setTypeface(regularFont);
        holder.txtMobile.setTypeface(regularFont);
        holder.txtEmail.setTypeface(regularFont);
        holder.emailSwitch.setTypeface(regularFont);
        holder.smsSwitch.setTypeface(regularFont);



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }
}