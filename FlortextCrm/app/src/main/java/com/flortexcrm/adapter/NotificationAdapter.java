package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.Response.NotificationsResponse;
import com.flortexcrm.Response.TaskDateResponse;
import com.flortexcrm.activity.LeadProductsActivity;
import com.flortexcrm.activity.TaskDetailsActivity;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;
import com.flortexcrm.utility.PrefUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context context;
    private List<NotificationsResponse.DataBean> homeList;
    Typeface regularFont, boldFont;

    public NotificationAdapter(Context mContext, List<NotificationsResponse.DataBean> homekitchenList) {
        this.context = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_review_chats1, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.MyViewHolder holder, final int position) {

        final NotificationsResponse.DataBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        if (home.getIsRead().equalsIgnoreCase("0")) {
           // holder.not_rl.setBackgroundColor(Color.DKGRAY);
            holder.not_rl.setBackgroundColor(Color.parseColor("#A3E3E1E1"));
        }else {
            holder.not_rl.setBackgroundColor(Color.WHITE);
        }

        holder.txtDateTime.setText(home.getCreatedOn());
        holder.txtName.setText(home.getDisplayText());
        holder.txtChatOrFile.setText(home.getMessage());

        // holder.txtName.setTypeface(regularFont);
        holder.txtDateTime.setTypeface(regularFont);
        holder.txtChatOrFile.setTypeface(regularFont);

        holder.not_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task_id = home.getTaskId();
                if ( task_id != null || !task_id.equalsIgnoreCase("0")) {
                    Intent intent = new Intent(context, TaskDetailsActivity.class);
                    intent.putExtra("taskId", task_id);
                    intent.putExtra("activity","NotificationFragment");
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "This is not a valid Task", Toast.LENGTH_SHORT).show();
                }

                markUnreadNotification(home.getId());
            }
        });
    }



    @Override
    public int getItemCount() {
        return homeList.size();
    }


    /*Holder*/

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RelativeLayout not_rl;
        TextView txtDateTime, txtName, txtChatOrFile;
        ImageView imgDefault;
        RelativeLayout relativeLayout;
        CrmListItemClickListener notificationCRMItemClickListener;

        public MyViewHolder(View view) {
            super(view);

            not_rl = (RelativeLayout) view.findViewById(R.id.not_rl);
            txtDateTime = (TextView) view.findViewById(R.id.txtDateTime);
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtChatOrFile = (TextView) view.findViewById(R.id.txtChatOrFile);
            imgDefault = (ImageView) view.findViewById(R.id.imgDefault);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.not_rl);
        }

        @Override
        public void onClick(View view) {
            this.notificationCRMItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(CrmListItemClickListener ic) {
            this.notificationCRMItemClickListener = ic;
        }
    }

    private void markUnreadNotification(String id) {
       String userId= PrefUtils.getUserId(context);
        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().markUnread(RetrofitClient.BASE_URL+"users/"+userId+"/notifications/"+id+"/read");
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse fcmResponse = response.body();

                if ( (fcmResponse.getStatus().equals("10100"))) {

                  //  Toast.makeText(context, fcmResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }else if ( (fcmResponse.getStatus().equals("10200"))) {

                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {

            }
        });
    }
}
