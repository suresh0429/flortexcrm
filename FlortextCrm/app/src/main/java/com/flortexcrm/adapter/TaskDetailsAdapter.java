package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.TaskDetailResponse;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import java.util.List;

public class TaskDetailsAdapter extends RecyclerView.Adapter<TaskDetailsAdapter.MyViewHolder> {
    private Context context;
    private List<TaskDetailResponse.DataBean.TaskBean.AssignedToEmployeesBean> homeList;
    Typeface regularFont, boldFont;
    CrmListItemClickListener crmListItemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtAssignedTo,txtDueDate;

        public MyViewHolder(View view) {
            super(view);

            txtAssignedTo = (TextView) view.findViewById(R.id.txtAssignedTo);
            txtDueDate = (TextView) view.findViewById(R.id.txtDueDate);


        }
    }

    public TaskDetailsAdapter(Context context, List<TaskDetailResponse.DataBean.TaskBean.AssignedToEmployeesBean> homekitchenList) {
        this.context = context;
        this.homeList = homekitchenList;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_task_details, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final TaskDetailResponse.DataBean.TaskBean.AssignedToEmployeesBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        holder.txtAssignedTo.setText(home.getUserName());
        holder.txtDueDate.setText(home.getDueDate());

        holder.txtAssignedTo.setTypeface(regularFont);
        holder.txtDueDate.setTypeface(regularFont);

        holder.txtDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (crmListItemClickListener != null) {
                    crmListItemClickListener.onItemClick( view,position);
                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }

    public void setOnRecyclerViewItemClickListener(CrmListItemClickListener onRecyclerViewItemClickListener) {
        this.crmListItemClickListener = onRecyclerViewItemClickListener;
    }


}