package com.flortexcrm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import com.flortexcrm.activity.AddLeadActivity;
import com.flortexcrm.filter.FilterForCustomerList;
import com.flortexcrm.holder.FilterCustomerHolder;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;
import com.flortexcrm.model.CustomerModel;
import java.util.ArrayList;

public class FilterCustomerAdapter extends RecyclerView.Adapter<FilterCustomerHolder> implements Filterable {

    public ArrayList<CustomerModel> spinnerModels,filterList;
    public AddLeadActivity context;
    FilterForCustomerList filter;
    LayoutInflater li;
    int resource;

    public FilterCustomerAdapter(ArrayList<CustomerModel> spinnerModels, AddLeadActivity context, int resource) {
        this.spinnerModels = spinnerModels;
        this.filterList = spinnerModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public FilterCustomerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,null);
        FilterCustomerHolder slh = new FilterCustomerHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final FilterCustomerHolder holder, final int position) {

        holder.row_filter_customer_txt.setText(spinnerModels.get(position).getName());

        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
               context.setCustomerName(spinnerModels.get(pos).getName(),spinnerModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.spinnerModels.size();

    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
          filter=new FilterForCustomerList(filterList,this);
        }

        return filter;
    }
}
