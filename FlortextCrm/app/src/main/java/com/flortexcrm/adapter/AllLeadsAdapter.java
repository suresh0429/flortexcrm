package com.flortexcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.flortexcrm.Apis.Api;
import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.LeadsResponse;
import com.flortexcrm.activity.LeadDocumentActivity;
import com.flortexcrm.activity.LeadProductsActivity;
import com.flortexcrm.activity.LeadsActivity;
import com.flortexcrm.filter.CustomFilterforLeadsList;
import com.flortexcrm.holder.LeadsDataHolder;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AllLeadsAdapter extends RecyclerView.Adapter<LeadsDataHolder> implements Filterable {

    public ArrayList<LeadsResponse.DataBean> allLeadslistModels, filterList;
    CustomFilterforLeadsList filter;
    LeadsActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont;


    public AllLeadsAdapter(ArrayList<LeadsResponse.DataBean> allLeadslistModels, LeadsActivity context, int resource) {
        this.allLeadslistModels = allLeadslistModels;
        this.context = context;
        this.resource = resource;
        this.filterList = allLeadslistModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }


    @Override
    public LeadsDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        LeadsDataHolder slh = new LeadsDataHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final LeadsDataHolder holder, final int position) {

        String timedate = parseDateToddMMyyyy(allLeadslistModels.get(position).getCreated_on());
        holder.creaedate_text.setText(timedate);
        holder.status_text.setText(allLeadslistModels.get(position).getStatus());

        holder.lead_code_text.setText(allLeadslistModels.get(position).getLead_code());
        holder.accName_text.setText(allLeadslistModels.get(position).getCreated_by_employee_name());
        holder.subject_text.setText(allLeadslistModels.get(position).getSubject());
        holder.customer_text.setText(allLeadslistModels.get(position).getCustomer_name());
        holder.rating_text.setText(allLeadslistModels.get(position).getRating());
        holder.industry_text.setText(allLeadslistModels.get(position).getIndustry());
        holder.source_text.setText(allLeadslistModels.get(position).getSource());
        if (allLeadslistModels.get(position).getSource_person_name() != null) {
            holder.source_ll.setVisibility(View.VISIBLE);
            holder.source_name_text.setText(allLeadslistModels.get(position).getSource_person_name());
        }else {
            holder.source_ll.setVisibility(View.GONE);
        }
        holder.creaedate_text.setTypeface(regularFont);
        holder.status_text.setTypeface(regularFont);
        holder.lead_title_text.setTypeface(boldFont);
        holder.lead_code_text.setTypeface(regularFont);
        holder.accName_title.setTypeface(boldFont);
        holder.accName_text.setTypeface(regularFont);
        holder.suubejct_title.setTypeface(boldFont);
        holder.subject_text.setTypeface(regularFont);
        holder.customer_title.setTypeface(boldFont);
        holder.rating_title.setTypeface(boldFont);
        holder.rating_text.setTypeface(regularFont);
        holder.industry_title.setTypeface(boldFont);
        holder.industry_text.setTypeface(regularFont);
        holder.source_title.setTypeface(boldFont);
        holder.source_text.setTypeface(regularFont);
        holder.source_name_title.setTypeface(boldFont);
        holder.source_name_text.setTypeface(regularFont);

        holder.setItemClickListener(new CrmListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
            }
        });



        holder.product_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LeadProductsActivity.class);
                intent.putExtra("leadId",allLeadslistModels.get(position).getId());

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.document_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LeadDocumentActivity.class);
                intent.putExtra("leadId",allLeadslistModels.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.sales_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (allLeadslistModels.get(position).getSales_order_id() == null){

                    Toast.makeText(context, "Sale Order Not Generated", Toast.LENGTH_SHORT).show();

                }else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(RetrofitClient.BASE_IMAGE_URL+"admin/sales_order/preview/" + allLeadslistModels.get(position).getSales_order_id()));
                    context.startActivity(browserIntent);
                }
            }
        });
        holder.quation_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allLeadslistModels.get(position).getQuotation_id() == null){

                    Toast.makeText(context, "Quatation Not Generated", Toast.LENGTH_SHORT).show();

                }else {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(RetrofitClient.BASE_IMAGE_URL+"admin/quotation/preview/" + allLeadslistModels.get(position).getQuotation_id()));
                    context.startActivity(browserIntent);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return this.allLeadslistModels.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforLeadsList(filterList, this);
        }

        return filter;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
