package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.TaskDetailResponse;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {
    private Context context;
    private List<TaskDetailResponse.DataBean.ChatsBean> homeList;
    Typeface regularFont, boldFont;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDateTime,txtName,txtChatOrFile;
        ImageView imgDefault;

        public MyViewHolder(View view) {
            super(view);

            txtDateTime = (TextView) view.findViewById(R.id.txtDateTime);
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtChatOrFile = (TextView) view.findViewById(R.id.txtChatOrFile);
            imgDefault =(ImageView)view.findViewById(R.id.imgDefault);


        }
    }

    public ReviewsAdapter(Context mContext, List<TaskDetailResponse.DataBean.ChatsBean> homekitchenList) {
        this.context = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public ReviewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_review_chats, parent, false);

        return new ReviewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ReviewsAdapter.MyViewHolder holder, final int position) {
        final TaskDetailResponse.DataBean.ChatsBean home = homeList.get(position);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        holder.txtDateTime.setText(home.getCreatedOn());
        holder.txtName.setText(home.getUserName());

        if (home.getComment() != null && !home.getComment().isEmpty()) {
            // doSomething
            holder.txtChatOrFile.setText(home.getComment());
        }
        else if (home.getAttachments() != null){

            holder.txtChatOrFile.setText(""+ home.getAttachments());
            holder.txtChatOrFile.setTextColor(Color.BLUE);
        }



       // holder.txtName.setTypeface(regularFont);
        holder.txtDateTime.setTypeface(regularFont);
        holder.txtChatOrFile.setTypeface(regularFont);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }
}
