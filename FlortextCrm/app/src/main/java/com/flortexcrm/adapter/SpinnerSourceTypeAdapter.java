package com.flortexcrm.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.Response.LeadSpinnerResponse;

import java.util.List;

public class SpinnerSourceTypeAdapter extends BaseAdapter {
    List<LeadSpinnerResponse.DataBean.SourceTypeBean> models;
    Context context;

    public SpinnerSourceTypeAdapter(List<LeadSpinnerResponse.DataBean.SourceTypeBean> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = View.inflate(context, R.layout.spinner_row_sample, null);
        }

        Typeface regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));

        TextView txtName = (TextView) convertView.findViewById(R.id.serList);

        txtName.setText(models.get(position).getName());
        txtName.setTypeface(regular);


        return convertView;
    }
}
