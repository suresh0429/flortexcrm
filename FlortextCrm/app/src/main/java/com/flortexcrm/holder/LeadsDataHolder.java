package com.flortexcrm.holder;

import android.app.MediaRouteButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;


public class LeadsDataHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView creaedate_text, status_text, lead_title_text, lead_code_text, accName_title,
            accName_text, suubejct_title, subject_text, customer_title, customer_text,
            rating_title, rating_text, industry_title, industry_text, source_title,
            source_text,product_text, document_text, sales_order, quation_text,
            source_name_title,source_name_text;
    public LinearLayout source_ll;
    CrmListItemClickListener allCRMItemClickListener;

    public LeadsDataHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        source_ll = itemView.findViewById(R.id.source_ll);
        creaedate_text = itemView.findViewById(R.id.creaedate_text);
        status_text = itemView.findViewById(R.id.status_text);
        lead_title_text = itemView.findViewById(R.id.lead_title_text);
        lead_code_text = itemView.findViewById(R.id.lead_code_text);
        accName_title = itemView.findViewById(R.id.accName_title);
        accName_text = itemView.findViewById(R.id.accName_text);
        suubejct_title = itemView.findViewById(R.id.suubejct_title);
        subject_text = itemView.findViewById(R.id.subject_text);

        customer_title = itemView.findViewById(R.id.customer_title);
        customer_text = itemView.findViewById(R.id.customer_text);


        rating_title = itemView.findViewById(R.id.rating_title);
        rating_text = itemView.findViewById(R.id.rating_text);
        industry_title = itemView.findViewById(R.id.industry_title);
        industry_text = itemView.findViewById(R.id.industry_text);
        source_title = itemView.findViewById(R.id.source_title);
        source_text = itemView.findViewById(R.id.source_text);

        product_text = itemView.findViewById(R.id.product_text);
        document_text = itemView.findViewById(R.id.document_text);
        sales_order = itemView.findViewById(R.id.sales_order);
        quation_text = itemView.findViewById(R.id.quation_text);
        source_name_title = itemView.findViewById(R.id.source_name_title);
        source_name_text = itemView.findViewById(R.id.source_name_text);

    }

    @Override
    public void onClick(View view) {
        this.allCRMItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CrmListItemClickListener ic) {
        this.allCRMItemClickListener = ic;
    }
}
