package com.flortexcrm.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.flortexcrm.R;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;

public class FilterCustomerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView row_filter_customer_txt;

    CrmListItemClickListener crmListItemClickListener;

    public FilterCustomerHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        row_filter_customer_txt = itemView.findViewById(R.id.row_filter_customer_txt);

    }

    @Override
    public void onClick(View view)
    {

        this.crmListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CrmListItemClickListener ic)
    {
        this.crmListItemClickListener = ic;
    }
}
