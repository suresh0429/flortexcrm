package com.flortexcrm.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.CreateTaskResponse;
import com.flortexcrm.Response.TaskDateResponse;
import com.flortexcrm.Response.TaskDetailResponse;
import com.flortexcrm.Response.TaskInstallationResponse;
import com.flortexcrm.Response.TaskOtherResponse;
import com.flortexcrm.Response.TaskReviewResponse;
import com.flortexcrm.adapter.ReviewsAdapter;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.adapter.TaskDetailsAdapter;
import com.flortexcrm.itemclicklistener.CrmListItemClickListener;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.FileUtils;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskDetailsActivity extends AppCompatActivity implements View.OnClickListener, CrmListItemClickListener {
    private int GALLERY = 1;
    private static final String IMAGE_DIRECTORY = "/demonuts";

    private List<TaskDetailResponse.DataBean.TaskBean.AssignedToEmployeesBean> docsBeanList;
    TaskDetailsAdapter taskDetailsAdapter;
    ReviewsAdapter reviewsAdapter;

    private static final String TAG = "Sample";
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private static final String STATE_TEXTVIEW = "STATE_TEXTVIEW";
    private SwitchDateTimeDialogFragment dateTimeFragment;
    int cyear, cmonth, cday, millisecond, second, minute, hour, hourofday;
    ArrayList<SpinnerModel> emplyoyeeArrayList;
    Typeface bold, regular;
    String userId, taskId, emplyoeeId, tasktype_id, status, activity;

    EditText etDueDate, etreason;
    AppCompatSpinner etAssignto;

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtsubject)
    TextView txtsubject;
    @BindView(R.id.txtDetails)
    TextView txtDetails;
    @BindView(R.id.txtType)
    TextView txtType;
    @BindView(R.id.addText)
    TextView addText;
    @BindView(R.id.asssigntaskRecycler)
    RecyclerView asssigntaskRecycler;
    @BindView(R.id.reviewRecycler)
    RecyclerView reviewRecycler;
    @BindView(R.id.etReview)
    EditText etReview;
    @BindView(R.id.imageFile)
    ImageView imageFile;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.botam_Layout)
    RelativeLayout botamLayout;
    File file;
    Uri uri;
    Bundle savedInstanceState;
    Button button_compelted;
    String text;

    String[] i_status = {"Select Installation status", "COMPLETED", "PENDING", "PARTIAL"};
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        ButterKnife.bind(this);
        checkInternet = NetworkChecking.isConnected(this);
        Calendar c = Calendar.getInstance();
        cyear = c.get(Calendar.YEAR);//calender year starts from 1900 so you must add 1900 to the value recevie.i.e., 1990+112 = 2012
        cmonth = c.get(Calendar.MONTH);//this is april so you will receive  3 instead of 4.
        cday = c.get(Calendar.DAY_OF_MONTH);
        millisecond = c.get(Calendar.MILLISECOND);
        second = c.get(Calendar.SECOND);
        minute = c.get(Calendar.MINUTE);
        //12 hour format
        hour = c.get(Calendar.HOUR);
        //24 hour format
        hourofday = c.get(Calendar.HOUR_OF_DAY);


        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(TaskDetailsActivity.this);
        toolbarTitle.setTypeface(bold);

        button_compelted = findViewById(R.id.button_compelted);


        if (getIntent() != null) {

            activity = getIntent().getStringExtra("activity");
            taskId = getIntent().getStringExtra("taskId");

        }

        getTaskDetailsData();


        button_compelted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tasktype_id.equals("5")) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(TaskDetailsActivity.this);
                    builder.setCancelable(false);
                    View view = getLayoutInflater().inflate(R.layout.alert_installation, null);
                    builder.setView(view);
                    final AlertDialog b = builder.create();
                    b.show();
                    final EditText edt_l_name = view.findViewById(R.id.edt_l_name);
                    final EditText edt_mdetails = view.findViewById(R.id.edt_mdetails);
                    final EditText edt_remark = view.findViewById(R.id.edt_remark);

                    final AppCompatSpinner instal_spinner = view.findViewById(R.id.instal_spinner);
                    Button btn_save = view.findViewById(R.id.btn_save);
                    Button cancel = view.findViewById(R.id.cancel);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            b.dismiss();
                        }
                    });
                    ArrayAdapter aa = new ArrayAdapter(TaskDetailsActivity.this, android.R.layout.simple_spinner_item, i_status);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    instal_spinner.setAdapter(aa);
                    instal_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            text = instal_spinner.getSelectedItem().toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                    btn_save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String layername = edt_l_name.getText().toString();
                            String details = edt_mdetails.getText().toString();
                            String remark = edt_remark.getText().toString();

                            if (layername.isEmpty()) {
                                edt_l_name.setError("Enter Layer name");
                                edt_l_name.requestFocus();
                                return;
                            }
                            if (details.isEmpty()) {
                                edt_mdetails.setError("Enter Material details");
                                edt_mdetails.requestFocus();
                                return;
                            }
                            if (remark.isEmpty()) {
                                edt_remark.setError("Enter Remark");
                                edt_remark.requestFocus();
                                return;
                            }


                            if (Connectivity.isConnected(TaskDetailsActivity.this)) {
                                Dialog.showProgressBar(TaskDetailsActivity.this, "Loading......");
                                Call<TaskInstallationResponse> call = RetrofitClient.getInstance().getApi().AddInstallationTask(RetrofitClient.BASE_URL + "/users/" + userId + "/tasks/" + taskId + "/mark-completed", layername, details, text, remark);
                                call.enqueue(new Callback<TaskInstallationResponse>() {
                                    @Override
                                    public void onResponse(Call<TaskInstallationResponse> call, Response<TaskInstallationResponse> response) {
                                        if (response.isSuccessful()) ;
                                        TaskInstallationResponse taskInstallationResponse = response.body();
                                        if (taskInstallationResponse.getStatus().equals("10100")) {
                                            Dialog.hideProgressBar();
                                            Toast.makeText(TaskDetailsActivity.this, taskInstallationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
                                            intent.putExtra("taskId", taskId);
                                            intent.putExtra("taskTypeId", tasktype_id);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        } else {

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<TaskInstallationResponse> call, Throwable t) {
                                        Dialog.hideProgressBar();
                                        Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                });
                            }
                        }
                    });


                } else if (tasktype_id.equals("7")) {
                    Intent intent = new Intent(TaskDetailsActivity.this, TaskDeliveryActivity.class);
                    intent.putExtra("taskId", taskId);
                    intent.putExtra("taskTypeId", tasktype_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(TaskDetailsActivity.this);
                    builder.setCancelable(false);
                    View view = getLayoutInflater().inflate(R.layout.alert_other, null);
                    final EditText edt_remark = view.findViewById(R.id.edt_remark);
                    Button cancel = view.findViewById(R.id.cancel);
                    Button ok = view.findViewById(R.id.ok);

                    builder.setView(view);

                    final AlertDialog b = builder.create();
                    b.show();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String remark = edt_remark.getText().toString();
                            if (remark.isEmpty()) {
                                edt_remark.setError("Enter Remark");
                                edt_remark.requestFocus();
                                return;
                            }

                            Dialog.showProgressBar(TaskDetailsActivity.this, "Loading......");
                            Call<TaskOtherResponse> call = RetrofitClient.getInstance().getApi().AddOtherTask(RetrofitClient.BASE_URL + "/users/" + userId + "/tasks/" + taskId + "/mark-completed", remark);
                            call.enqueue(new Callback<TaskOtherResponse>() {
                                @Override
                                public void onResponse(Call<TaskOtherResponse> call, Response<TaskOtherResponse> response) {
                                    if (response.isSuccessful()) ;
                                    TaskOtherResponse taskOtherResponse = response.body();
                                    if (taskOtherResponse.getStatus().equals("10100")) {
                                        Dialog.hideProgressBar();
                                        Toast.makeText(TaskDetailsActivity.this, taskOtherResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
                                        intent.putExtra("taskId", taskId);
                                        intent.putExtra("taskTypeId", tasktype_id);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);

                                    } else {

                                    }
                                }

                                @Override
                                public void onFailure(Call<TaskOtherResponse> call, Throwable t) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            b.dismiss();

                        }
                    });
                }
            }
        });


    }

    @OnClick({R.id.close_img, R.id.btnSend, R.id.imageFile, R.id.addText})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_img:

                if (activity.equalsIgnoreCase("NotificationFragment")) {
                    Intent intent = new Intent(TaskDetailsActivity.this, HomeActivity.class);
                    startActivity(intent);
                }else if (activity.equalsIgnoreCase("TaskListAdapter")){
                    Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
                    startActivity(intent);
                }else if (activity.equalsIgnoreCase("Firebase")){
                    Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
                    startActivity(intent);
                }

                //finish();
                break;
            case R.id.btnSend:

                if (!etReview.getText().toString().isEmpty()) {

                    taskReview(etReview.getText().toString());

                }

                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }


                break;
            case R.id.addText:
                popUpEditText();
                break;
            case R.id.imageFile:
                getFile();
                break;
        }
    }

    private void popUpEditText() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(TaskDetailsActivity.this);
        mBuilder.setCancelable(false);
        View mView = getLayoutInflater().inflate(R.layout.addtask_diloge, null);
        etDueDate = (EditText) mView.findViewById(R.id.etDueDate);
        etAssignto = (AppCompatSpinner) mView.findViewById(R.id.etAssignto);

        Bundle bundle = new Bundle();
        timeDatePicker(bundle);
        selectTaskpinners();

        mBuilder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        mBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etDueDate.getText().toString().isEmpty()) {
                    taskAssign(etDueDate.getText().toString());
                    dialog.dismiss();
                } else {
                    //etDueDate.setError("Enter Due Date");
                    Toast.makeText(TaskDetailsActivity.this, "Enter Due Date", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void getFile() {

        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            finish();
            startActivity(intent);
            return;
        }
       /* Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, 100);*/

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

            //the image URI
            Uri uri = data.getData();
            File file = new File(FileUtils.getPath(TaskDetailsActivity.this, uri));
            etReview.setText(file.getName());


        }*/

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(TaskDetailsActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    // imgeFile.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(TaskDetailsActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + file.getAbsolutePath());
            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void selectTaskpinners() {
        emplyoyeeArrayList = new ArrayList<>();

        Call<CreateTaskResponse> call = RetrofitClient.getInstance().getApi().getSpinnersTask(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/create");

        call.enqueue(new Callback<CreateTaskResponse>() {
            @Override
            public void onResponse(Call<CreateTaskResponse> call, Response<CreateTaskResponse> response) {

                if (response.isSuccessful()) {

                    final List<CreateTaskResponse.DataBean.EmployeesBean> employeesBeanList = response.body() != null ? response.body().getData().getEmployees() : null;


                    for (CreateTaskResponse.DataBean.EmployeesBean employeesBean : employeesBeanList) {

                        emplyoyeeArrayList.add(new SpinnerModel(employeesBean.getId(), employeesBean.getName()));
                    }


                    // Priority
                    SpinnerAdapter preriotyAdapter = new SpinnerAdapter(emplyoyeeArrayList, TaskDetailsActivity.this);
                    etAssignto.setAdapter(preriotyAdapter);
                    etAssignto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            emplyoeeId = emplyoyeeArrayList.get(i).getId();
                            //Toast.makeText(TaskDetailsActivity.this, "Company Name: " + emplyoeeId, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<CreateTaskResponse> call, Throwable t) {

            }
        });
    }

    private void getTaskDetailsData() {
        if (Connectivity.isConnected(TaskDetailsActivity.this)) {
            Dialog.showProgressBar(TaskDetailsActivity.this, "Loading Details...");
            Call<TaskDetailResponse> call = RetrofitClient.getInstance().getApi().getAllTaskDetailsList(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/" + taskId);
            Log.d("TaskDetailUrl", RetrofitClient.BASE_URL + "users/" + userId + "/tasks/" + taskId);
            call.enqueue(new Callback<TaskDetailResponse>() {
                @Override
                public void onResponse(Call<TaskDetailResponse> call, Response<TaskDetailResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    TaskDetailResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        if ((productListResponse.getStatus().equals("10100"))) {

                            docsBeanList = response.body() != null ? response.body().getData().getTask().getAssignedToEmployees() : null;
                            List<TaskDetailResponse.DataBean.ChatsBean> chatsBeanList = response.body() != null ? response.body().getData().getChats() : null;

                            tasktype_id = productListResponse.getData().getTask().getTaskTypeId();
                            txtsubject.setText(productListResponse.getData().getTask().getTaskSubject());
                            txtDetails.setText(productListResponse.getData().getTask().getTaskDetails());
                            txtType.setText(productListResponse.getData().getTask().getTaskTypeName());

                            status = productListResponse.getData().getTask().getStatus();

                            if (status.equalsIgnoreCase("3")) {
                                botamLayout.setVisibility(View.GONE);
                            } else {
                                botamLayout.setVisibility(View.VISIBLE);
                            }

                            if (docsBeanList != null) {
                                taskDetailsAdapter = new TaskDetailsAdapter(TaskDetailsActivity.this, docsBeanList);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(TaskDetailsActivity.this);
                                asssigntaskRecycler.setNestedScrollingEnabled(false);
                                asssigntaskRecycler.setLayoutManager(layoutManager);
                                asssigntaskRecycler.addItemDecoration(new DividerItemDecoration(TaskDetailsActivity.this, DividerItemDecoration.VERTICAL));
                                taskDetailsAdapter.setOnRecyclerViewItemClickListener(TaskDetailsActivity.this);
                                asssigntaskRecycler.setAdapter(taskDetailsAdapter);

                                reviewsAdapter = new ReviewsAdapter(TaskDetailsActivity.this, chatsBeanList);
                                LinearLayoutManager layoutManager1 = new LinearLayoutManager(TaskDetailsActivity.this);
                                reviewRecycler.setNestedScrollingEnabled(false);
                                reviewRecycler.setLayoutManager(layoutManager1);
                                reviewRecycler.addItemDecoration(new DividerItemDecoration(TaskDetailsActivity.this, DividerItemDecoration.VERTICAL));
                                reviewRecycler.setAdapter(reviewsAdapter);

                            }


                        } else {
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<TaskDetailResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });


        } else {
            Toast.makeText(TaskDetailsActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    // time and Date picker
    private void timeDatePicker(Bundle savedInstanceState) {

       /* if (savedInstanceState != null) {
            // Restore value from saved state
            etDueDate.setText(savedInstanceState.getCharSequence(STATE_TEXTVIEW));
        }
*/

        // Construct SwitchDateTimePicker
        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel),
                    getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d-MM-yyyy HH:mm", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setHighlightAMPMSelection(false);


        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // Set listener for date
        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {


                etDueDate.setText(myDateFormat.format(date));


            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                etDueDate.setText("");
            }
        });

        etDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Re-init each time
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
            }
        });
    }

   /* @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the current textView
        savedInstanceState.putCharSequence(STATE_TEXTVIEW, etDueDate.getText());
        super.onSaveInstanceState(savedInstanceState);
    }*/


    @Override
    public void onItemClick(View v, int pos) {
        final TaskDetailResponse.DataBean.TaskBean.AssignedToEmployeesBean home = docsBeanList.get(pos);
        // Toast.makeText(this,"Position clicked: " + pos+"    ,"+home.getDueDate(),Toast.LENGTH_LONG).show();

        popUpEditTextUpdate(home.getEmployeeId());
    }

    private void popUpEditTextUpdate(final String employeeId) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(TaskDetailsActivity.this);
        mBuilder.setCancelable(false);
        View mView = getLayoutInflater().inflate(R.layout.updatedate_diloge, null);
        etDueDate = (EditText) mView.findViewById(R.id.etDueDate);
        etreason = (EditText) mView.findViewById(R.id.etReson);

        Bundle bundle = new Bundle();
        timeDatePickerUpdate(bundle);


        mBuilder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        mBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etDueDate.getText().toString().isEmpty() && !etreason.getText().toString().isEmpty()) {
                    taskDateUpdate(etDueDate.getText().toString(), etreason.getText().toString(), employeeId);
                    dialog.dismiss();
                } else {
                    //etDueDate.setError("Enter Due Date");
                    Toast.makeText(TaskDetailsActivity.this, "Enter Valid Details", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    // time and Date picker
    private void timeDatePickerUpdate(Bundle savedInstanceState) {

       /* if (savedInstanceState != null) {
            // Restore value from saved state
            etDueDate.setText(savedInstanceState.getCharSequence(STATE_TEXTVIEW));
        }
*/

        // Construct SwitchDateTimePicker
        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel),
                    getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d-MM-yyyy HH:mm", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setHighlightAMPMSelection(false);


        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // Set listener for date
        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {


                etDueDate.setText(myDateFormat.format(date));


            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                etDueDate.setText("");
            }
        });

        etDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Re-init each time
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
            }
        });
    }

    // retrofit Add Assign
    private void taskAssign(String duedate) {

        Call<TaskDateResponse> call = RetrofitClient.getInstance().getApi().assignTak(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/" + taskId + "/assign", duedate, emplyoeeId);
        call.enqueue(new Callback<TaskDateResponse>() {
            @Override
            public void onResponse(Call<TaskDateResponse> call, Response<TaskDateResponse> response) {

                TaskDateResponse taskDateResponse = response.body();

                if ((taskDateResponse.getStatus().equals("10100"))) {

                    getTaskDetailsData();


                } else if ((taskDateResponse.getStatus().equals("10200"))) {

                    Toast.makeText(TaskDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(TaskDetailsActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TaskDateResponse> call, Throwable t) {

            }
        });
    }


    // retrofit dateUpdate
    private void taskDateUpdate(String duedate, String reason, String employeeId) {

        Call<TaskDateResponse> call = RetrofitClient.getInstance().getApi().updateTaskDate(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/" + taskId + "/assign/date/change", duedate, employeeId, reason);
        call.enqueue(new Callback<TaskDateResponse>() {
            @Override
            public void onResponse(Call<TaskDateResponse> call, Response<TaskDateResponse> response) {

                TaskDateResponse taskDateResponse = response.body();

                if ((taskDateResponse.getStatus().equals("10100"))) {

                    getTaskDetailsData();


                } else {
                   /* int color = Color.RED;
                    snackBar(loginResponse.getMessage(), color);*/

                }
            }

            @Override
            public void onFailure(Call<TaskDateResponse> call, Throwable t) {

            }
        });
    }


    // retrofit review
    private void taskReview(String comment) {

        Call<TaskReviewResponse> call = RetrofitClient.getInstance().getApi().updateTaskReview(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/" + taskId + "/comment", comment);
        call.enqueue(new Callback<TaskReviewResponse>() {
            @Override
            public void onResponse(Call<TaskReviewResponse> call, Response<TaskReviewResponse> response) {

                TaskReviewResponse taskDateResponse = response.body();

                if ((taskDateResponse.getStatus().equals("10100"))) {


                    getTaskDetailsData();
                    etReview.setText(null);
                    //  Toast.makeText(getApplicationContext(),taskDateResponse.getMessage(),Toast.LENGTH_LONG).show();


                } else {
                   /* int color = Color.RED;
                    snackBar(loginResponse.getMessage(), color);*/

                }
            }

            @Override
            public void onFailure(Call<TaskReviewResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        if (activity.equalsIgnoreCase("NotificationFragment")) {
            Intent intent = new Intent(TaskDetailsActivity.this, HomeActivity.class);
            startActivity(intent);
        }else if (activity.equalsIgnoreCase("TaskListAdapter")){
            Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
            startActivity(intent);
        }else if (activity.equalsIgnoreCase("Firebase")){
            Intent intent = new Intent(TaskDetailsActivity.this, TaskActivity.class);
            startActivity(intent);
        }
    }
}
