package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.ContactPersonResponse;
import com.flortexcrm.adapter.ContactPersonAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactPersonActivity extends AppCompatActivity implements View.OnClickListener {
    Typeface bold, regular;
    String userId, customerId;

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.recyclerViewTask)
    RecyclerView recyclerViewTask;
    @BindView(R.id.add_ContactPerson)
    TextView addContactPerson;
    TextView text_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_person2);
        text_nodata=findViewById(R.id.text_nodata);
        ButterKnife.bind(this);
        toolbarTitle.setTypeface(bold);
        closeImg.setOnClickListener(this);
        addContactPerson.setOnClickListener(this);
        userId = PrefUtils.getUserId(ContactPersonActivity.this);
        customerId = getIntent().getStringExtra("customerId");
        getContactPersonData();

    }

    private void getContactPersonData() {

        if (Connectivity.isConnected(ContactPersonActivity.this)) {
            Dialog.showProgressBar(ContactPersonActivity.this, "Loading data...");
            Call<ContactPersonResponse> call = RetrofitClient.getInstance().getApi().getContactPersonList(RetrofitClient.BASE_URL + "users/" + userId + "/customers/" + customerId + "/contact-persons");
            call.enqueue(new Callback<ContactPersonResponse>() {
                @Override
                public void onResponse(Call<ContactPersonResponse> call, Response<ContactPersonResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    ContactPersonResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<ContactPersonResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;


                        if ((productListResponse.getStatus().equals("10100"))) {

                            ContactPersonAdapter contactPersonAdapter = new ContactPersonAdapter(ContactPersonActivity.this, docsBeanList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(ContactPersonActivity.this);
                            recyclerViewTask.setNestedScrollingEnabled(false);
                            recyclerViewTask.setLayoutManager(layoutManager);
                            recyclerViewTask.setAdapter(contactPersonAdapter);


                        }
                        else if((productListResponse.getStatus().equals("10300"))) {
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();
                            text_nodata.setVisibility(View.VISIBLE);

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<ContactPersonResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });
        } else {
            Toast.makeText(ContactPersonActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == closeImg) {
            finish();
        }
        if (v == addContactPerson) {
            Intent intent = new Intent(ContactPersonActivity.this, AddContactPersonActivity.class);
            intent.putExtra("customerId",customerId);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


}
