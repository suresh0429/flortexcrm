package com.flortexcrm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.AddLeadProductResponse;
import com.flortexcrm.Response.ProductsResponse;
import com.flortexcrm.model.AutoCompleteModel;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductActivity extends AppCompatActivity {
    AutoCompleteTextView auto_cname;
    EditText edt_box, edt_qty, edt_rate;
    TextView txtAddProduct;
    ArrayList<AutoCompleteModel> productsResponseArrayList;
    ImageView close_img;
    String product_id, lead_id, userId;
    Double boxvalue;
    private boolean checkInternet;
    boolean et1Focus, et2Focus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product);

        checkInternet = NetworkChecking.isConnected(this);

        edt_rate = findViewById(R.id.edt_rate);
        edt_qty = findViewById(R.id.edt_qty);
        edt_box = findViewById(R.id.edt_box);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        auto_cname = findViewById(R.id.auto_cname);

        userId = PrefUtils.getUserId(AddProductActivity.this);
        lead_id = getIntent().getStringExtra("leadId");


        txtAddProduct = findViewById(R.id.txtAddProduct);
        txtAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInternet) {
                    addLeadProduct();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        productName(auto_cname);
    }


    private void productName(final AutoCompleteTextView productTextname) {
        productsResponseArrayList = new ArrayList<>();

        Call<ProductsResponse> call = RetrofitClient.getInstance().getApi().getProductsName(RetrofitClient.BASE_URL + "products");

        call.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                if (response.isSuccessful()) {

                    final List<ProductsResponse.DataBean> products = response.body() != null ? response.body().getData() : null;


                    if (products != null) {
                        for (ProductsResponse.DataBean prioritiesBean : products) {

                            productsResponseArrayList.add(new AutoCompleteModel(prioritiesBean.getId(), prioritiesBean.getName(),prioritiesBean.getPacking()));
                        }


                        AutoCompleteAdapter adapter = new AutoCompleteAdapter(AddProductActivity.this,
                                R.layout.autocompleteitem, productsResponseArrayList);
                        productTextname.setThreshold(1);
                        productTextname.setAdapter(adapter);
                        productTextname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ProductsResponse.DataBean productsBean = products.get(position);

                                product_id = productsResponseArrayList.get(position).getId();

                                edt_box.setText("1");
                                edt_qty.setText(productsBean.getPacking());

                                boxvalue =Double.parseDouble(productsBean.getPacking());

                                productCalculation(boxvalue);

                                Log.e("POSITION",""+products.get(position).getPacking()+"________"+position);
                            }
                        });
                    }

                } else {

                    //View parentLayout = findViewById(android.R.id.content);
                    // Snackbar.make(btnSerSubmit, getResources().getString(R.string.slowInternetconnection), Snackbar.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {

            }
        });
    }

    private void productCalculation(final Double boxvalue) {
        edt_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //quando o texto é alterado chamamos o filtro.
                if (et1Focus) {
                    double valor = (s.length() > 0) ? Double.parseDouble(s.toString()) : 0;
                    valor = (valor * boxvalue);
                    edt_qty.setText(String.valueOf(valor));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et2Focus) {
                    double valor = (s.length() > 0) ? Double.parseDouble(s.toString()) : 0;
                    valor = (valor / boxvalue);
                    edt_box.setText(String.valueOf(valor));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edt_box.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                et1Focus = b;
            }
        });

        edt_qty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                et2Focus = b;
            }
        });

    }


    private void addLeadProduct() {
        String box = edt_box.getText().toString();
        String qty = edt_qty.getText().toString();
        String rate = edt_rate.getText().toString();

        if (product_id == null) {

            Toast.makeText(getApplicationContext(), "Search Valid Product Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (box.isEmpty()) {

            edt_box.setError("Enter Box");
            edt_box.requestFocus();

            return;
        }
        if (qty.isEmpty()) {

            edt_qty.setError("Enter Qty");
            edt_qty.requestFocus();
            return;
        }
        if (rate.isEmpty()) {

            edt_rate.setError("Enter Rate");
            edt_rate.requestFocus();
            return;
        }


        if (Connectivity.isConnected(AddProductActivity.this)) {
            Dialog.showProgressBar(AddProductActivity.this, "Loading...");
            Call<AddLeadProductResponse> call = RetrofitClient.getInstance().getApi().addLeadProduct(RetrofitClient.BASE_URL + "users/" + userId + "/leads/" + lead_id + "/products", "", product_id, rate, qty, box);
            call.enqueue(new Callback<AddLeadProductResponse>() {
                @Override
                public void onResponse(Call<AddLeadProductResponse> call, Response<AddLeadProductResponse> response) {
                    if (response.isSuccessful()) ;
                    AddLeadProductResponse addLeadProductResponse = response.body();
                    if (addLeadProductResponse.getStatus().equals("10100")) {

                        Dialog.hideProgressBar();

                        Toast.makeText(AddProductActivity.this, addLeadProductResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(AddProductActivity.this, LeadProductsActivity.class);
                        intent.putExtra("leadId", lead_id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    } else {

                    }

                }

                @Override
                public void onFailure(Call<AddLeadProductResponse> call, Throwable t) {
                    Dialog.hideProgressBar();
                    Toast.makeText(AddProductActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }


    }
}
