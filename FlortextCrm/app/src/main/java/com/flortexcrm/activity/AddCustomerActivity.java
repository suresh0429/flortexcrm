package com.flortexcrm.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.CitySpinnerresponse;
import com.flortexcrm.Response.CustomerSpinnerResponse;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCustomerActivity extends AppCompatActivity {
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.profileImage)
    ImageView profileImage;
    @BindView(R.id.txtCustomerCode)
    EditText txtCustomerCode;
    @BindView(R.id.etSalutation)
    AppCompatSpinner etSalutation;
    @BindView(R.id.etBillerName)
    EditText etBillerName;
    @BindView(R.id.etCustomerType)
    AppCompatSpinner etCustomerType;
    @BindView(R.id.activeSwitch)
    Switch activeSwitch;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etLandline)
    EditText etLandline;
    @BindView(R.id.emailSwitch)
    Switch emailSwitch;
    @BindView(R.id.smsSwitch)
    Switch smsSwitch;
    @BindView(R.id.etState)
    AppCompatSpinner etState;
    @BindView(R.id.etChooseCity)
    AppCompatSpinner etChooseCity;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.etStreet)
    EditText etStreet;
    @BindView(R.id.etZipCode)
    EditText etZipCode;
    @BindView(R.id.etLandMark)
    EditText etLandMark;
    @BindView(R.id.txtAddCustomer)
    TextView txtAddCustomer;

    File file;
    Typeface bold, regular;
    String userId, userName;
    private boolean checkInternet;

    ArrayList<SpinnerModel> customerTypeArrayList, salutationArrayList, statesArrayList, cityArrayList;
    String customerTypeId, salutationId, stateId, cityId,activeStatus,emailoptStatus,smsOptStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        ButterKnife.bind(this);
        requestMultiplePermissions();

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(AddCustomerActivity.this);
        userName = PrefUtils.getUserName(AddCustomerActivity.this);

        checkInternet = NetworkChecking.isConnected(this);

        toolbarTitle.setTypeface(bold);

        selectTaskpinners();

        activeSwitch.setChecked(false);
        activeStatus = "InActive";
        activeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){

                   activeStatus = "Active" ;
                }
                else {
                    activeStatus = "InActive" ;
                }
            }
        });

        emailSwitch.setChecked(false);
        emailoptStatus = "No";
        emailSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){

                    emailoptStatus = "Yes" ;
                }
                else {
                    emailoptStatus = "No" ;
                }
            }
        });

        smsSwitch.setChecked(false);
        smsOptStatus = "No";
        smsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){

                    smsOptStatus = "Yes" ;
                }
                else {
                    smsOptStatus = "No" ;
                }
            }
        });

    }

    private void selectTaskpinners() {

        customerTypeArrayList = new ArrayList<>();
        salutationArrayList = new ArrayList<>();
        statesArrayList = new ArrayList<>();

            Call<CustomerSpinnerResponse> call = RetrofitClient.getInstance().getApi().getSpinnersCustomers(RetrofitClient.BASE_URL + "users/" + userId + "/customers/create");

            call.enqueue(new Callback<CustomerSpinnerResponse>() {
                @Override
                public void onResponse(Call<CustomerSpinnerResponse> call, Response<CustomerSpinnerResponse> response) {

                    if (response.isSuccessful()) {
                        Dialog.hideProgressBar();

                        final List<CustomerSpinnerResponse.DataBean.CustomersTypesBean> customersTypesBeanList = response.body() != null ? response.body().getData().getCustomersTypes() : null;
                        final List<CustomerSpinnerResponse.DataBean.SalutationsBean> salutationsBeanList = response.body() != null ? response.body().getData().getSalutations() : null;
                        final List<CustomerSpinnerResponse.DataBean.StatesBean> statesBeanList = response.body() != null ? response.body().getData().getStates() : null;

                        final CustomerSpinnerResponse.DataBean dataBeans = response.body() != null ? response.body().getData() : null;

                        txtCustomerCode.setText("" + dataBeans.getCustomerCode());

                        for (CustomerSpinnerResponse.DataBean.CustomersTypesBean prioritiesBean : customersTypesBeanList) {

                            customerTypeArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }

                        for (CustomerSpinnerResponse.DataBean.SalutationsBean tasksStatusBean : salutationsBeanList) {

                            salutationArrayList.add(new SpinnerModel(tasksStatusBean.getId(), tasksStatusBean.getName()));
                        }


                        for (CustomerSpinnerResponse.DataBean.StatesBean tasksTypesBean : statesBeanList) {

                            statesArrayList.add(new SpinnerModel(tasksTypesBean.getId(), tasksTypesBean.getName()));
                        }


                        // customer Type
                        SpinnerAdapter preriotyAdapter = new SpinnerAdapter(customerTypeArrayList, AddCustomerActivity.this);
                        etCustomerType.setAdapter(preriotyAdapter);
                        etCustomerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                                customerTypeId = customerTypeArrayList.get(i).getId();
                                // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        // status
                        SpinnerAdapter statusAdapter = new SpinnerAdapter(salutationArrayList, AddCustomerActivity.this);
                        etSalutation.setAdapter(statusAdapter);
                        etSalutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                                salutationId = salutationArrayList.get(i).getId();
                                // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


                        // type
                        SpinnerAdapter tasktypeAdapter = new SpinnerAdapter(statesArrayList, AddCustomerActivity.this);
                        etState.setAdapter(tasktypeAdapter);
                        etState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                                stateId = statesArrayList.get(i).getId();
                                //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();
                                selectCitySpinners(stateId);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


                    }

                }

                @Override
                public void onFailure(Call<CustomerSpinnerResponse> call, Throwable t) {
                    Dialog.hideProgressBar();
                }
            });



    }

    private void selectCitySpinners(String stateId) {

        cityArrayList = new ArrayList<>();

        Call<CitySpinnerresponse> call = RetrofitClient.getInstance().getApi().getSpinnersCity(RetrofitClient.BASE_URL + "master/location/cities/" + stateId);

        call.enqueue(new Callback<CitySpinnerresponse>() {
            @Override
            public void onResponse(Call<CitySpinnerresponse> call, Response<CitySpinnerresponse> response) {

                if (response.isSuccessful()) {

                    final List<CitySpinnerresponse.DataBean> customersTypesBeanList = response.body() != null ? response.body().getData() : null;

                    if (customersTypesBeanList != null) {

                        for (CitySpinnerresponse.DataBean prioritiesBean : customersTypesBeanList) {

                            cityArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }

                    }
                    // city
                    SpinnerAdapter cityAdapter = new SpinnerAdapter(cityArrayList, AddCustomerActivity.this);
                    etChooseCity.setAdapter(cityAdapter);
                    etChooseCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            cityId = cityArrayList.get(i).getId();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<CitySpinnerresponse> call, Throwable t) {

            }
        });
    }

    @OnClick({R.id.close_img, R.id.txtAddCustomer,R.id.profileImage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.profileImage:
                showPictureDialog();
                break;
            case R.id.txtAddCustomer:
                if (checkInternet) {
                    addCustomer();
                }else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(AddCustomerActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    profileImage.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AddCustomerActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            profileImage.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(AddCustomerActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    // retrofit FCM TOCKEN UPDATE
    private void addCustomer() {

        if (etBillerName.getText().toString().isEmpty()){
            etBillerName.setError("Enter Biller Name");
            return;
        }
        String emailCheck = etEmail.getText().toString();
        if(!isEmailValid(emailCheck) || emailCheck.length()==0)
        {
            etEmail.setError("Enter Valid Email");
            return;
            //Toast.makeText(AddContactPersonActivity.this,"Enter valid email address",Toast.LENGTH_SHORT).show();
        }
        /*if (etEmail.getText().toString().isEmpty()){
            etEmail.setError("Enter Email");
            return;
        }*/
        if (etMobile.getText().toString().length()<10){
            etMobile.setError("Enter Mobile");
            return;
        }
//        if (etLandline.getText().toString().isEmpty()){
//            etLandline.setError("Enter landline");
//            return;
//        }

        if (cityId == null){
            Toast.makeText(getApplicationContext(),"Select City First",Toast.LENGTH_LONG).show();
        }

        if (etArea.getText().toString().isEmpty()){
            etArea.setError("Enter Area");
            return;
        }
        if (etStreet.getText().toString().isEmpty()){
            etStreet.setError("Enter Street");
            return;
        }
        if (etZipCode.getText().toString().isEmpty()){
            etZipCode.setError("Enter ZipCode");
            return;
        }

        if (etLandMark.getText().toString().isEmpty()){
            etLandMark.setError("Enter LandMark");
            return;
        }

        Dialog.showProgressBar(AddCustomerActivity.this, "Adding Customers...");
        RequestBody customerCode = RequestBody.create(MediaType.parse("text/plain"), txtCustomerCode.getText().toString());
        RequestBody salId = RequestBody.create(MediaType.parse("text/plain"), salutationId);
        RequestBody billerName = RequestBody.create(MediaType.parse("text/plain"), etBillerName.getText().toString());
        RequestBody customtypeId = RequestBody.create(MediaType.parse("text/plain"), customerTypeId);

        RequestBody active = RequestBody.create(MediaType.parse("text/plain"), activeStatus);

        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), etEmail.getText().toString());
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), etMobile.getText().toString());
        RequestBody landline = RequestBody.create(MediaType.parse("text/plain"), etLandline.getText().toString());

        RequestBody emailOpt = RequestBody.create(MediaType.parse("text/plain"), emailoptStatus);
        RequestBody smsOpt = RequestBody.create(MediaType.parse("text/plain"), smsOptStatus);

        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateId);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), cityId);
        RequestBody area = RequestBody.create(MediaType.parse("text/plain"), etArea.getText().toString());
        RequestBody street = RequestBody.create(MediaType.parse("text/plain"), etStreet.getText().toString());
        RequestBody zipcode = RequestBody.create(MediaType.parse("text/plain"), etZipCode.getText().toString());
        RequestBody landmark = RequestBody.create(MediaType.parse("text/plain"), etLandMark.getText().toString());


      /*  MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("doc", file.getName(), requestFile);

        }*/

        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().addPostCustomer(RetrofitClient.BASE_URL+"users/"+userId+"/customers",
                customerCode,salId,billerName,customtypeId,email,mobile,landline,state,city,area,street,zipcode,landmark,active,emailOpt,smsOpt);
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();
                Dialog.hideProgressBar();

                if ( loginResponse.getStatus().equals("10100")) {

                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddCustomerActivity.this,CustomersActivity.class);
                   // intent.putExtra("leadId",leadId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
                else if (loginResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Log.e("RESPONSE",""+t.getMessage());
                Dialog.hideProgressBar();
            }
        });
    }



    private void  requestMultiplePermissions(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }


                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

}
