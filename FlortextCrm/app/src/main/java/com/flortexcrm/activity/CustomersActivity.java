package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.CustomerResponse;
import com.flortexcrm.adapter.AllCustomerAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomersActivity extends AppCompatActivity implements View.OnClickListener
{

    ImageView close_img;
    RecyclerView recyclerViewCustomer;
    TextView toolbar_title,add_customer;
    Typeface bold,regular;
    String userId;
    AllCustomerAdapter allCustomerAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView text_nodata;
    SearchView mSearch_customers;
    List<CustomerResponse.DataBean> docsBeanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId= PrefUtils.getUserId(CustomersActivity.this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        add_customer = findViewById(R.id.add_customer);
        add_customer.setTypeface(regular);
        add_customer.setOnClickListener(this);
        text_nodata=findViewById(R.id.text_nodata);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recyclerViewCustomer = findViewById(R.id.recyclerViewCustomer);
        mSearch_customers = findViewById(R.id.mSearch_customers);
        mSearch_customers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_customers.setIconified(false);

            }
        });
        mSearch_customers.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                filter(query.toString());
                return false;
            }
        });

        getCustomerData();
    }





    private void getCustomerData() {
        if(Connectivity.isConnected(CustomersActivity.this))
        {
            Dialog.showProgressBar(CustomersActivity.this, "Loading Customers...");
            Call<CustomerResponse> call = RetrofitClient.getInstance().getApi().getAllCustomerList(RetrofitClient.BASE_URL + "users/" + userId + "/customers");
            call.enqueue(new Callback<CustomerResponse>() {
                @Override
                public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    CustomerResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                       /* for (ProductListResponse.DataBean address : docsBeanList) {

                            addressModelItems.add(new AddressModelItem(address.getId(), address.getUser_id(), address.getName(), address.getCity(), address.getState(), address.getZip(),
                                    address.getPhone(), address.getAddress(), address.getSet_as_default(), address.getStatus(), address.getEmail(), itemslength, module, customerId, totalPrice, checkoutStatus));
                        }*/

                            allCustomerAdapter = new AllCustomerAdapter((ArrayList<CustomerResponse.DataBean>) docsBeanList, CustomersActivity.this, R.layout.row_customer_data);
                            layoutManager = new LinearLayoutManager(CustomersActivity.this);
                            recyclerViewCustomer.setNestedScrollingEnabled(false);
                            recyclerViewCustomer.setLayoutManager(layoutManager);
                            recyclerViewCustomer.setAdapter(allCustomerAdapter);

                        } else if (productListResponse.getStatus().equals("10300")){
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<CustomerResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });


        }
        else
        {
            Toast.makeText(CustomersActivity.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if(v==close_img)
        {
            finish();
        }
        if(v== add_customer)
        {
            Intent intent = new Intent(CustomersActivity.this, AddCustomerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    void filter(String text){
        List<CustomerResponse.DataBean> temp = new ArrayList();
        for(CustomerResponse.DataBean d: docsBeanList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getName().toLowerCase().contains(text)
                    || d.getArea().toLowerCase().contains(text)
                    || d.getCustomer_code().toLowerCase().contains(text)
                    || d.getMobile().toLowerCase().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        allCustomerAdapter.updateList(temp);
    }
}
