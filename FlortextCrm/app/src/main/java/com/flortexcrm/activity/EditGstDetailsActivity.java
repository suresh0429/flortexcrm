package com.flortexcrm.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.CustomerSpinnerResponse;
import com.flortexcrm.Response.SaleTypeResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditGstDetailsActivity extends AppCompatActivity {
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtGstno)
    TextView txtGstno;
    @BindView(R.id.etGstno)
    EditText etGstno;
    @BindView(R.id.txtGSTState)
    TextView txtGSTState;
    @BindView(R.id.etGSTState)
    AppCompatSpinner etGSTState;
    @BindView(R.id.txtGSTSalesType)
    TextView txtGSTSalesType;
    @BindView(R.id.etGSTSalesType)
    AppCompatSpinner etGSTSalesType;
    @BindView(R.id.txtPanno)
    TextView txtPanno;
    @BindView(R.id.etPanno)
    EditText etPanno;
    @BindView(R.id.txtIsactive)
    TextView txtIsactive;
    @BindView(R.id.Isactive)
    Switch Isactive;
    @BindView(R.id.txtSave)
    TextView txtSave;
    String userId,stateId,saletypeId,c_id,StateId,Sale_id,gst_no,pan_no;
    ArrayList<SpinnerModel> customerTypeArrayList, salutationArrayList, statesArrayList, saletypeArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gst_details);

        userId = PrefUtils.getUserId(EditGstDetailsActivity.this);

        c_id=getIntent().getStringExtra("customerId");
        StateId=getIntent().getStringExtra("state_id");
        Sale_id=getIntent().getStringExtra("sale_id");
        gst_no=getIntent().getStringExtra("gst_no");
        pan_no=getIntent().getStringExtra("pan_no");
//
//        etGstno.setText(gst_no);
//        etPanno.setText(pan_no);

        SelectState();
        SelectSaleType();

    }

    private void SelectState() {

        customerTypeArrayList = new ArrayList<>();
        salutationArrayList = new ArrayList<>();
        statesArrayList = new ArrayList<>();


        Call<CustomerSpinnerResponse> call = RetrofitClient.getInstance().getApi().getSpinnersCustomers(RetrofitClient.BASE_URL + "users/" + userId + "/customers/create");

        call.enqueue(new Callback<CustomerSpinnerResponse>() {
            @Override
            public void onResponse(Call<CustomerSpinnerResponse> call, Response<CustomerSpinnerResponse> response) {

                if (response.isSuccessful()) {

                    final List<CustomerSpinnerResponse.DataBean.CustomersTypesBean> customersTypesBeanList = response.body() != null ? response.body().getData().getCustomersTypes() : null;
                    final List<CustomerSpinnerResponse.DataBean.SalutationsBean> salutationsBeanList = response.body() != null ? response.body().getData().getSalutations() : null;
                    final List<CustomerSpinnerResponse.DataBean.StatesBean> statesBeanList = response.body() != null ? response.body().getData().getStates() : null;

                    final CustomerSpinnerResponse.DataBean dataBeans = response.body() != null ? response.body().getData() : null;

//                    txtCustomerCode.setText("" + dataBeans.getCustomerCode());

                    for (CustomerSpinnerResponse.DataBean.CustomersTypesBean prioritiesBean : customersTypesBeanList) {

                        customerTypeArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                    }

                    for (CustomerSpinnerResponse.DataBean.SalutationsBean tasksStatusBean : salutationsBeanList) {

                        salutationArrayList.add(new SpinnerModel(tasksStatusBean.getId(), tasksStatusBean.getName()));
                    }


                    for (CustomerSpinnerResponse.DataBean.StatesBean tasksTypesBean : statesBeanList) {

                        statesArrayList.add(new SpinnerModel(tasksTypesBean.getId(), tasksTypesBean.getName()));
                    }

                    // type
                    SpinnerAdapter tasktypeAdapter = new SpinnerAdapter(statesArrayList, EditGstDetailsActivity.this);
                    etGSTState.setAdapter(tasktypeAdapter);
//                    etGSTState.setSelection(statesArrayList.indexOf(StateId));

                    for (int i = 0; i < statesArrayList.size(); i++) {
                        if (statesArrayList.get(i).getId().equals(StateId)) {
                            etGSTState.setSelection(i);
                        }
                    }
                    etGSTState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            stateId = statesArrayList.get(i).getId();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();
//                            selectCitySpinners(stateId);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<CustomerSpinnerResponse> call, Throwable t) {

            }
        });

    }

    private void SelectSaleType() {

        saletypeArrayList=new ArrayList<>();

        Call<SaleTypeResponse> call=RetrofitClient.getInstance().getApi().getSpinnersSaleType(RetrofitClient.BASE_URL+"master/gst_types");
        call.enqueue(new Callback<SaleTypeResponse>() {
            @Override
            public void onResponse(Call<SaleTypeResponse> call, Response<SaleTypeResponse> response) {

                if (response.isSuccessful()) {

                    final List<SaleTypeResponse.DataBean> saleTypesBeanList = response.body() != null ? response.body().getData() : null;

                    if (saleTypesBeanList != null) {

                        for (SaleTypeResponse.DataBean prioritiesBean : saleTypesBeanList) {

                            saletypeArrayList.add(new SpinnerModel(prioritiesBean.getId(),prioritiesBean.getName()));
                        }
                    }

                    SpinnerAdapter cityAdapter = new SpinnerAdapter(saletypeArrayList, EditGstDetailsActivity.this);
                    etGSTSalesType.setAdapter(cityAdapter);

                    for (int i = 0; i < saletypeArrayList.size(); i++) {
                        if (saletypeArrayList.get(i).getId().equals(Sale_id)) {
                            etGSTSalesType.setSelection(i);
                        }
                    }

                    etGSTSalesType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            saletypeId=saletypeArrayList.get(position).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<SaleTypeResponse> call, Throwable t) {

            }
        });
    }



}
