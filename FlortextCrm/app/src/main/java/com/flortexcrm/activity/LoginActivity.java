package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.Response.LoginErrorResponse;
import com.flortexcrm.Response.LoginResponse;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.ErrorUtils;
import com.flortexcrm.utility.PrefUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textForget,textTitle;
    Button login_button;
    EditText editEmail, editPassword;
    String deviceId, deviceName,fcmToken;
    Typeface regular,bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));


      /*  textForget = findViewById(R.id.textForget);
        textForget.setTypeface(regular);
        textTitle = findViewById(R.id.textTitle);
        textTitle.setTypeface(bold);*/

        editEmail = findViewById(R.id.editEmail);
        editEmail.setTypeface(regular);
        editPassword = findViewById(R.id.editPassword);
        editPassword.setTypeface(regular);
        login_button = findViewById(R.id.login_button);
        login_button.setOnClickListener(this);
        login_button.setTypeface(bold);


        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        deviceName=manufacturer+" "+model;

        Log.d("DeviceDetail",deviceName+"----"+deviceId);


        // fcm Token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("TOKEN",""+fcmToken);
            }
        });
    }

    @Override
    public void onClick(View v)
    {

        if(v==login_button)
        {
           // startActivity(new Intent(LoginActivity.this, MainActivity.class));

            if(Connectivity.isConnected(LoginActivity.this))
            {
                String user_name=editEmail.getText().toString();
                String password=editPassword.getText().toString();

                    if(!isEmailValid(user_name) || user_name.length()==0)
                    {
                        Toast.makeText(LoginActivity.this,"Enter valid email address",Toast.LENGTH_SHORT).show();
                    }else if(password.length()==0)
                    {
                        Toast.makeText(LoginActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {

                       userLogin(user_name,password);
                    }
            }
            else
            {
                Toast.makeText(LoginActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    // retrofit login
    private void userLogin(String user_name, String password) {

        Dialog.showProgressBar(LoginActivity.this, "Login...");
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(user_name, password,deviceId,deviceName,"ANDROID");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                Dialog.hideProgressBar();
                LoginResponse loginResponse = response.body();

//                Log.e("RESPONSE",""+loginResponse.getStatus()+" --- "+loginResponse.getMessage()+" ---"+loginResponse.getData());

                if (response.isSuccessful()) {
                    // use response data and do some fancy stuff :)

                    if ( loginResponse.getStatus().equals("10100")) {

                        PrefUtils.saveToLoginPrefs(LoginActivity.this, loginResponse.getData().getJwt(), loginResponse.getData().getUserId(),
                                loginResponse.getData().getUserName(),loginResponse.getData().getEmail(),loginResponse.getData().getMobile());


                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        fcmTokenUpdate(loginResponse.getData().getUserId());


                    }
                    else if (loginResponse.getStatus().equals("10200")){
                        Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                    }
                    else if (loginResponse.getStatus().equals("10300")){
                        Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                    }
                    else if (loginResponse.getStatus().equals("10400")){
                        Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                    }
                    else if (loginResponse.getStatus().equals("10500")){
                        Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                    }



                }




            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
               /* progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);*/
                Dialog.hideProgressBar();

                Log.e("RESPONSE",""+t.getMessage());
            }
        });
    }

    // retrofit FCM TOCKEN UPDATE
    private void fcmTokenUpdate(String userId)
    {

        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().fcmToken(RetrofitClient.BASE_URL+"users/"+userId+"/fcm/token",fcmToken,"ANDROID");
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();

                Log.e("RESPONSE",""+loginResponse.getMessage());


                if ( loginResponse.getStatus().equals("10100")) {

                    Log.e("TOKENS",""+loginResponse.getMessage());
                }
                else if (loginResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
               Log.e("RESPONSE",""+t.getMessage());
            }
        });
    }
}
