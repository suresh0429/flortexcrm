package com.flortexcrm.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.Response.LeadDocTypeSpinnerResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.FileUtils;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLeadDocumentActivity extends AppCompatActivity {

    private int GALLERY = 1;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    ProgressDialog progressDialog;

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtDocumentType)
    TextView txtDocumentType;
    @BindView(R.id.etDocumentType)
    AppCompatSpinner etDocumentType;
    @BindView(R.id.txtDocumentname)
    TextView txtDocumentname;
    @BindView(R.id.etDocumentname)
    EditText etDocumentname;
    @BindView(R.id.txtDocumentpath)
    TextView txtDocumentpath;
    @BindView(R.id.btnUpload)
    Button btnUpload;
    @BindView(R.id.imgeFile)
    ImageView imgeFile;
    @BindView(R.id.txtDocumentPath)
    TextView txtDocumentPath;
    @BindView(R.id.txtSave)
    TextView txtSave;


    ArrayList<SpinnerModel> documenttypeArray;

    String leadId, userId, doctypeId;
    File file;
    Uri uri;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lead_document);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        userId = PrefUtils.getUserId(AddLeadDocumentActivity.this);

        checkInternet = NetworkChecking.isConnected(this);

        if (getIntent() != null) {
            leadId = getIntent().getStringExtra("leadId");
        }
        documentTypeSpinner();
    }

    @OnClick({R.id.close_img, R.id.btnUpload, R.id.txtSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.btnUpload:
                getFile();
                break;
            case R.id.txtSave:
                if (checkInternet) {
                addLeadDocument();
                }else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void getFile() {

        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            finish();
            startActivity(intent);
            return;
        }

        /*Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, 100);*/

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);

    }


    private void documentTypeSpinner() {
        documenttypeArray = new ArrayList<>();

        Call<LeadDocTypeSpinnerResponse> call = RetrofitClient.getInstance().getApi().getLeadDocSpinner(RetrofitClient.BASE_URL + "master/lead/documents/types");

        call.enqueue(new Callback<LeadDocTypeSpinnerResponse>() {
            @Override
            public void onResponse(Call<LeadDocTypeSpinnerResponse> call, Response<LeadDocTypeSpinnerResponse> response) {

                if (response.isSuccessful()) {

                    final List<LeadDocTypeSpinnerResponse.DataBean> sourceTypeBeanList = response.body() != null ? response.body().getData() : null;


                    if (sourceTypeBeanList != null) {
                        for (LeadDocTypeSpinnerResponse.DataBean prioritiesBean : sourceTypeBeanList) {

                            documenttypeArray.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }
                    }

                    // source type
                    SpinnerAdapter spinnerSourceTypeAdapter = new SpinnerAdapter(documenttypeArray, AddLeadDocumentActivity.this);
                    etDocumentType.setAdapter(spinnerSourceTypeAdapter);
                    etDocumentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            doctypeId = sourceTypeBeanList.get(i).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<LeadDocTypeSpinnerResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

            if (null != data.getData()) {
                uri = data.getData();
                Log.e("URI",""+uri);
                file = new File(FileUtils.getPath(AddLeadDocumentActivity.this, uri));
            }

            if ( file.toString().endsWith(".jpg") || file.toString().endsWith(".png")) {

                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                imgeFile.setImageBitmap(bitmap);
                txtDocumentPath.setText(null);

            }
            else {
                txtDocumentPath.setText(file.getName());
                imgeFile.setImageBitmap(null);
            }
        }*/

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(AddLeadDocumentActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgeFile.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AddLeadDocumentActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + file.getAbsolutePath());
            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    // retrofit FCM TOCKEN UPDATE
    private void addLeadDocument() {
        progressDialog.show();
        String docName = etDocumentname.getText().toString();

        if (docName.isEmpty()) {
            progressDialog.cancel();
            etDocumentname.setError("Enter Document Name");
            return;
        }

        RequestBody docType = RequestBody.create(MediaType.parse("text/plain"), doctypeId);
        RequestBody documentName = RequestBody.create(MediaType.parse("text/plain"), docName);

        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("doc", file.getName(), requestFile);

        }

        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().addLeadDocument(RetrofitClient.BASE_URL + "users/" + userId + "/leads/" + leadId + "/documents", body, docType, documentName);
        call.enqueue(new Callback<FcmResponse>() {

            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();

                if (loginResponse.getStatus().equals("10100")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddLeadDocumentActivity.this, LeadDocumentActivity.class);
                    intent.putExtra("leadId", leadId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else if (loginResponse.getStatus().equals("10200")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10300")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10400")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Log.e("RESPONSE", "" + t.getMessage());
            }
        });
    }
}
