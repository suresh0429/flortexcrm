package com.flortexcrm.activity;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.ProductListResponse;
import com.flortexcrm.adapter.AllProductsAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    RecyclerView recyclerViewLProducts;
    TextView toolbar_title, add_products;
    Typeface bold, regular;
    String userId;
    AllProductsAdapter allProductsAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView text_nodata;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(ProductsActivity.this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        add_products = findViewById(R.id.add_products);
        add_products.setTypeface(regular);
        add_products.setOnClickListener(this);
        text_nodata=findViewById(R.id.text_nodata);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recyclerViewLProducts = findViewById(R.id.recyclerViewLProducts);

        getProductsData();
    }

    private void getProductsData() {
        if (Connectivity.isConnected(ProductsActivity.this)) {
            Dialog.showProgressBar(ProductsActivity.this, "Loading Products...");
            Call<ProductListResponse> call = RetrofitClient.getInstance().getApi().getProductList("10", "0");
            call.enqueue(new Callback<ProductListResponse>() {
                @Override
                public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    ProductListResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<ProductListResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                       /* for (ProductListResponse.DataBean address : docsBeanList) {

                            addressModelItems.add(new AddressModelItem(address.getId(), address.getUser_id(), address.getName(), address.getCity(), address.getState(), address.getZip(),
                                    address.getPhone(), address.getAddress(), address.getSet_as_default(), address.getStatus(), address.getEmail(), itemslength, module, customerId, totalPrice, checkoutStatus));
                        }*/

                            allProductsAdapter = new AllProductsAdapter(docsBeanList, ProductsActivity.this, R.layout.row_data_productlist);
                            layoutManager = new LinearLayoutManager(ProductsActivity.this);
                            recyclerViewLProducts.setNestedScrollingEnabled(false);
                            recyclerViewLProducts.setLayoutManager(layoutManager);
                            recyclerViewLProducts.setAdapter(allProductsAdapter);

                        } else if (productListResponse.getStatus().equals("10200")){
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductListResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });
        } else {
            Toast.makeText(ProductsActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }
        if (v == add_products) {

        }
    }

}
