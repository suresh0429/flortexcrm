package com.flortexcrm.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Reciver.AlarmReceiver;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.adapter.AllTaskAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskActivity extends AppCompatActivity implements View.OnClickListener {
    Date nearestDate = null;
    ArrayList<String> dueDateArray = new ArrayList<>();
    List<Date> dateList = new ArrayList<>();
    Date currentdate;

    ImageView close_img;
    RecyclerView recyclerViewTask;
    TextView toolbar_title, add_task;
    Typeface bold, regular;
    String userId;
    AllTaskAdapter allTaskAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView text_nodata;
    SearchView mSearch_task;
    @BindView(R.id.fabSnooz)
    FloatingActionButton fabSnooz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(TaskActivity.this);

        text_nodata = findViewById(R.id.text_nodata);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        add_task = findViewById(R.id.add_task);
        add_task.setTypeface(regular);
        add_task.setOnClickListener(this);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recyclerViewTask = findViewById(R.id.recyclerViewTask);

        mSearch_task = findViewById(R.id.mSearch_task);
        mSearch_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_task.setIconified(false);

            }
        });

        mSearch_task.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                if (query != null) {
                    allTaskAdapter.getFilter().filter(query);
                    allTaskAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        getTaskData();

       fabSnooz.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               getNearestDate(dateList,currentdate);
           }
       });

    }

    private void getTaskData() {

        if (Connectivity.isConnected(TaskActivity.this)) {
            Dialog.showProgressBar(TaskActivity.this, "Loading Tasks...");
            Call<TaskResponse> call = RetrofitClient.getInstance().getApi().getAllTaskList(RetrofitClient.BASE_URL + "users/" + userId + "/tasks?status=2");
            call.enqueue(new Callback<TaskResponse>() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    TaskResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<TaskResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;


                        if ((productListResponse.getStatus().equals("10100"))) {

                            allTaskAdapter = new AllTaskAdapter((ArrayList<TaskResponse.DataBean>) docsBeanList, TaskActivity.this, R.layout.row_data_task);
                            layoutManager = new LinearLayoutManager(TaskActivity.this);
                            recyclerViewTask.setNestedScrollingEnabled(false);
                            recyclerViewTask.setLayoutManager(layoutManager);
                            recyclerViewTask.setAdapter(allTaskAdapter);


                            // snooze Alarm Set
                            for (TaskResponse.DataBean address : docsBeanList) {

                                if (address.getAssignedToEmployees() != null) {
                                    Log.e("ARRAY", "" + address.getAssignedToEmployees().size());

                                    for (TaskResponse.DataBean.AssignedToEmployeesBean assignedToEmployeesBean : address.getAssignedToEmployees()) {

                                        dueDateArray.add(assignedToEmployeesBean.getDueDate());

                                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        try {
                                            Date date = (Date)formatter.parse(assignedToEmployeesBean.getDueDate());
                                            dateList.add(date);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2019-03-21 20:39:52
                                        String currentDateandTime = sdf.format(new Date());
                                        try {
                                            currentdate = sdf.parse(currentDateandTime);//2019-03-21 14:52:00


                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }



                                    }
                                }
                            }


                        } else if (productListResponse.getStatus().equals("10300")) {
                            text_nodata.setVisibility(View.VISIBLE);
                            fabSnooz.setVisibility(View.GONE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<TaskResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });
        } else {
            Toast.makeText(TaskActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {


        if (v == close_img) {
            Intent intent = new Intent(TaskActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        if (v == add_task) {
            Intent intent = new Intent(TaskActivity.this, AddTaskActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }


    }

    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.fabSnooz), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    public void getNearestDate(List<Date> dates, Date targetDate) {

        int index = 0;
        long prevDiff = -1;
        long targetTS = targetDate.getTime();
        for (int i = 0; i < dates.size(); i++) {
            Date date = dates.get(i);
            long currDiff = Math.abs(date.getTime() - targetTS);
            if (prevDiff == -1 || currDiff < prevDiff) {
                prevDiff = currDiff;

                if (targetDate.compareTo(date) < 0){
                    nearestDate = date;
                    index = i;

                    snackBar("You will get Snooze on nearest date", Color.WHITE);

                    alertSheduledNotification(nearestDate);
                    System.out.println("Nearest Date: " + nearestDate);
                }
                else if (targetDate.compareTo(date) > 0){

                    snackBar("No Tasks Found !", Color.WHITE);

                }else if (targetDate.compareTo(date) == 0){

                    snackBar("No Tasks Found !", Color.WHITE);

                }


            }
        }

        System.out.println("Index: " + index);


    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void alertSheduledNotification(Date dueDate) {

        Calendar calendar = Calendar.getInstance();

       // calendar.setTime(dueDate);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Log.e("DATEFORMATS", "" + hours + minutes + seconds + year + month + day);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_ONE_SHOT);


       /* Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, hours);
        cal.add(Calendar.MINUTE, minutes);
        cal.add(Calendar.SECOND, 30);
        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.DAY_OF_MONTH, day);
        System.out.print(cal.getTimeInMillis());*/


        calendar.add(Calendar.MINUTE,-30);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TaskActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
