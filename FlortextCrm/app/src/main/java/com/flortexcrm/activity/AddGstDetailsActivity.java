package com.flortexcrm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.AddGstDetailResponse;
import com.flortexcrm.Response.CitySpinnerresponse;
import com.flortexcrm.Response.CustomerSpinnerResponse;
import com.flortexcrm.Response.GSTResponse;
import com.flortexcrm.Response.SaleTypeResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGstDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtGstno)
    TextView txtGstno;
    @BindView(R.id.etGstno)
    EditText etGstno;
    @BindView(R.id.txtGSTState)
    TextView txtGSTState;
    @BindView(R.id.etGSTState)
    AppCompatSpinner etGSTState;
    @BindView(R.id.txtGSTSalesType)
    TextView txtGSTSalesType;
    @BindView(R.id.etGSTSalesType)
    AppCompatSpinner etGSTSalesType;
    @BindView(R.id.txtPanno)
    TextView txtPanno;
    @BindView(R.id.etPanno)
    EditText etPanno;
    @BindView(R.id.txtIsactive)
    TextView txtIsactive;
    @BindView(R.id.Isactive)
    Switch Isactive;
    @BindView(R.id.txtSave)
    TextView txtSave;
    String userId,stateId,saletypeId,c_id,StateId,Sale_id;
    ArrayList<SpinnerModel> customerTypeArrayList, salutationArrayList, statesArrayList, saletypeArrayList;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gst_details);
        ButterKnife.bind(this);

        checkInternet = NetworkChecking.isConnected(this);

        userId = PrefUtils.getUserId(AddGstDetailsActivity.this);

        c_id=getIntent().getStringExtra("customerId");

        SelectState();
        SelectSaleType();

        getBankDetails();

    }

    private void getBankDetails() {

        if(Connectivity.isConnected(AddGstDetailsActivity.this)) {

            Dialog.showProgressBar(AddGstDetailsActivity.this, "Loading GST Details...");

            Call<GSTResponse> call=RetrofitClient.getInstance().getApi().getGSTDetails(RetrofitClient.BASE_URL + "users/" + userId + "/customers/"+c_id+"/gst-details");
            call.enqueue(new Callback<GSTResponse>() {
                @Override
                public void onResponse(Call<GSTResponse> call, Response<GSTResponse> response) {
                    if (response.isSuccessful());
                    GSTResponse productListResponse = response.body();

                    if (productListResponse.getStatus().equals("10100")){
                        Dialog.hideProgressBar();
                        GSTResponse.DataBean gstData=productListResponse.getData();

                        etGstno.setText(gstData.getGstNo());
                        etPanno.setText(gstData.getPanNo());

                        StateId=gstData.getGstStateId();
                        Sale_id=gstData.getGstSalesTypeId();

                    }
                    else if (productListResponse.getStatus().equals("10200")){
                        Toast.makeText(AddGstDetailsActivity.this, productListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GSTResponse> call, Throwable t) {
                    Toast.makeText(AddGstDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

    private void SelectState() {

        customerTypeArrayList = new ArrayList<>();
        salutationArrayList = new ArrayList<>();
        statesArrayList = new ArrayList<>();


        Call<CustomerSpinnerResponse> call = RetrofitClient.getInstance().getApi().getSpinnersCustomers(RetrofitClient.BASE_URL + "users/" + userId + "/customers/create");

        call.enqueue(new Callback<CustomerSpinnerResponse>() {
            @Override
            public void onResponse(Call<CustomerSpinnerResponse> call, Response<CustomerSpinnerResponse> response) {

                if (response.isSuccessful()) {

                    final List<CustomerSpinnerResponse.DataBean.CustomersTypesBean> customersTypesBeanList = response.body() != null ? response.body().getData().getCustomersTypes() : null;
                    final List<CustomerSpinnerResponse.DataBean.SalutationsBean> salutationsBeanList = response.body() != null ? response.body().getData().getSalutations() : null;
                    final List<CustomerSpinnerResponse.DataBean.StatesBean> statesBeanList = response.body() != null ? response.body().getData().getStates() : null;

                    final CustomerSpinnerResponse.DataBean dataBeans = response.body() != null ? response.body().getData() : null;

//                    txtCustomerCode.setText("" + dataBeans.getCustomerCode());

                    for (CustomerSpinnerResponse.DataBean.CustomersTypesBean prioritiesBean : customersTypesBeanList) {

                        customerTypeArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                    }

                    for (CustomerSpinnerResponse.DataBean.SalutationsBean tasksStatusBean : salutationsBeanList) {

                        salutationArrayList.add(new SpinnerModel(tasksStatusBean.getId(), tasksStatusBean.getName()));
                    }


                    for (CustomerSpinnerResponse.DataBean.StatesBean tasksTypesBean : statesBeanList) {

                        statesArrayList.add(new SpinnerModel(tasksTypesBean.getId(), tasksTypesBean.getName()));
                    }

                    // type
                    SpinnerAdapter tasktypeAdapter = new SpinnerAdapter(statesArrayList, AddGstDetailsActivity.this);
                    etGSTState.setAdapter(tasktypeAdapter);

                    for (int i=0;i<statesArrayList.size();i++){
                        if (statesArrayList.get(i).getId().equalsIgnoreCase(StateId)){
                            etGSTState.setSelection(i);
                        }
                    }
//                    etGSTState.setSelection(statesArrayList.indexOf(StateId));
                    etGSTState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            stateId = statesArrayList.get(i).getId();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();
//                            selectCitySpinners(stateId);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<CustomerSpinnerResponse> call, Throwable t) {

            }
        });

    }

    private void SelectSaleType() {

        saletypeArrayList=new ArrayList<>();

        Call<SaleTypeResponse> call=RetrofitClient.getInstance().getApi().getSpinnersSaleType(RetrofitClient.BASE_URL+"master/gst_types");
        call.enqueue(new Callback<SaleTypeResponse>() {
            @Override
            public void onResponse(Call<SaleTypeResponse> call, Response<SaleTypeResponse> response) {

                if (response.isSuccessful()) {

                    final List<SaleTypeResponse.DataBean> saleTypesBeanList = response.body() != null ? response.body().getData() : null;

                    if (saleTypesBeanList != null) {

                        for (SaleTypeResponse.DataBean prioritiesBean : saleTypesBeanList) {

                            saletypeArrayList.add(new SpinnerModel(prioritiesBean.getId(),prioritiesBean.getName()));
                        }
                    }

                    SpinnerAdapter cityAdapter = new SpinnerAdapter(saletypeArrayList, AddGstDetailsActivity.this);
                    etGSTSalesType.setAdapter(cityAdapter);
                    for (int i=0;i<saletypeArrayList.size();i++){
                        if (saletypeArrayList.get(i).getId().equalsIgnoreCase(Sale_id)){
                            etGSTSalesType.setSelection(i);
                        }
                    }
                    etGSTSalesType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            saletypeId=saletypeArrayList.get(position).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<SaleTypeResponse> call, Throwable t) {

            }
        });



    }


    @OnClick({R.id.close_img, R.id.txtSave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.txtSave:
                if (checkInternet) {
                    addGstDetailsData();
                }else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void addGstDetailsData() {

        Call<AddGstDetailResponse> call=RetrofitClient.getInstance().getApi().addGstDetails(RetrofitClient.BASE_URL +"users/" + userId +"/customers/" + c_id +"/gst-details",etGstno.getText().toString(),saletypeId,etPanno.getText().toString());
        call.enqueue(new Callback<AddGstDetailResponse>() {
            @Override
            public void onResponse(Call<AddGstDetailResponse> call, Response<AddGstDetailResponse> response) {

                if (response.isSuccessful());
                AddGstDetailResponse addGstDetailResponse=response.body();

                if (addGstDetailResponse.getStatus().equals("10100")){

                    Toast.makeText(AddGstDetailsActivity.this, addGstDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(AddGstDetailsActivity.this,CustomersActivity.class);
//                    intent.putExtra("customerId",c_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                else if (addGstDetailResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(),addGstDetailResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (addGstDetailResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(),addGstDetailResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (addGstDetailResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(),addGstDetailResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddGstDetailResponse> call, Throwable t) {
                Toast.makeText(AddGstDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
