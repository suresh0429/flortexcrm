package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.BankDetailResponse;
import com.flortexcrm.Response.GSTResponse;
import com.flortexcrm.adapter.BankDetailsAdapter;
import com.flortexcrm.adapter.GSTAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GSTDetailsActivity extends AppCompatActivity {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.add_GstDetails)
    TextView addGstDetails;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;

    Typeface bold,regular;
    String userId,customerId,s_id,sale_id;
    GSTAdapter gstAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView txt_nodata,txtActive,txtName,txtRole,txtMobile,emailSwitch,smsSwitch,edit_GstDetails;
    GSTResponse.DataBean gstData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gstdetails);
        ButterKnife.bind(this);
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        toolbarTitle.setTypeface(bold);
        addGstDetails.setTypeface(regular);

        txt_nodata=findViewById(R.id.txt_nodata);
        smsSwitch=findViewById(R.id.smsSwitch);
        emailSwitch=findViewById(R.id.emailSwitch);
        txtMobile=findViewById(R.id.txtMobile);
        txtRole=findViewById(R.id.txtRole);
        txtName=findViewById(R.id.txtName);
        txtActive=findViewById(R.id.txtActive);

        edit_GstDetails=findViewById(R.id.edit_GstDetails);

        edit_GstDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(GSTDetailsActivity.this,EditGstDetailsActivity.class);
                intent.putExtra("gst_no",gstData.getGstNo());
                intent.putExtra("state_id",s_id);
                intent.putExtra("sale_id",sale_id);
                intent.putExtra("pan_no",gstData.getPanNo());
                intent.putExtra("customerId",gstData.getCustomersId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        userId= PrefUtils.getUserId(GSTDetailsActivity.this);


        if (getIntent() != null){
            customerId = getIntent().getStringExtra("customerId");
        }


        getBankDetails();

    }

    private void getBankDetails()
    {
        if(Connectivity.isConnected(GSTDetailsActivity.this))
        {
            Dialog.showProgressBar(GSTDetailsActivity.this, "Loading GST Details...");
            Call<GSTResponse> call = RetrofitClient.getInstance().getApi().getGSTDetails(RetrofitClient.BASE_URL + "users/" + userId + "/customers/"+customerId+"/gst-details");
            call.enqueue(new Callback<GSTResponse>() {
                @Override
                public void onResponse(Call<GSTResponse> call, Response<GSTResponse> response) {
                    // progress.setVisibility(View.GONE);

                    if (response.isSuccessful()) {
                        GSTResponse productListResponse = response.body();
                        if ((productListResponse.getStatus().equals("10100"))) {

                            Dialog.hideProgressBar();
                            gstData=productListResponse.getData();

                            smsSwitch.setText(gstData.getUpdatedOn());
                            emailSwitch.setText(gstData.getCreatedOn());
                            txtMobile.setText(gstData.getPanNo());
                            txtRole.setText(gstData.getGstNo());
                            txtName.setText(gstData.getGstCode());

                            s_id=gstData.getGstStateId();
                            sale_id=gstData.getGstSalesTypeId();


                            if (gstData.getIsActive().equals("1")){
                                txtActive.setText("Active");
                            }else {
                                txtActive.setText("In Active");
                            }


                        } else if ((productListResponse.getStatus().equals("10200"))){

                            txt_nodata.setVisibility(View.VISIBLE);

                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<GSTResponse> call, Throwable t) {
                    Dialog.hideProgressBar();
                }
            });


        }
        else
        {
            Toast.makeText(GSTDetailsActivity.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.close_img, R.id.add_GstDetails})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.add_GstDetails:
                Intent intent = new Intent(GSTDetailsActivity.this, AddGstDetailsActivity.class);
                intent.putExtra("customerId",customerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }
    }
}
