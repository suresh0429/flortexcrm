package com.flortexcrm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.TaskDeliveryResponse;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskDeliveryActivity extends AppCompatActivity {
    EditText edt_anmae,edt_tname,edt_truckno,edt_lrno,edt_remark;
    public static AppCompatSpinner dstatus_spinner,mode_spinner;
    Button btn_save;
    String[] i_status = { "Select Installation status", "COMPLETED", "PENDING", "PARTIAL"};
    String[] mode = { "Select Mode", "Paid", "Yet to Pay"};
    ImageView close_img;
    String modetext,statustext,userId,taskId,tasktype_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskdelivery_activity);

        btn_save=findViewById(R.id.btn_save);

        edt_anmae=findViewById(R.id.edt_anmae);
        edt_tname=findViewById(R.id.edt_tname);
        edt_truckno=findViewById(R.id.edt_truckno);
        edt_lrno=findViewById(R.id.edt_lrno);
        edt_remark=findViewById(R.id.edt_remark);

        dstatus_spinner=findViewById(R.id.dstatus_spinner);
        mode_spinner=findViewById(R.id.mode_spinner);

        userId = PrefUtils.getUserId(TaskDeliveryActivity.this);

        if (getIntent() != null) {

            taskId = getIntent().getStringExtra("taskId");
            tasktype_id=getIntent().getStringExtra("taskTypeId");
        }


        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        deliverSpinner();
        modeSpinner();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTask();
            }
        });
    }

    private void deliverSpinner() {

        ArrayAdapter aa = new ArrayAdapter(TaskDeliveryActivity.this,android.R.layout.simple_spinner_item,i_status);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dstatus_spinner.setAdapter(aa);
        dstatus_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statustext=dstatus_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void modeSpinner() {
        ArrayAdapter aa = new ArrayAdapter(TaskDeliveryActivity.this,android.R.layout.simple_spinner_item,mode);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mode_spinner.setAdapter(aa);
        mode_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                modetext=mode_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void addTask() {

        String person = edt_anmae.getText().toString();
        String transportname = edt_tname.getText().toString();
        String truckNme = edt_truckno.getText().toString();
        String lrNo = edt_lrno.getText().toString();
        String remark = edt_remark.getText().toString();

        if (person.isEmpty()){
            edt_anmae.setError("Enter person");
            edt_anmae.requestFocus();
            return;
        }
        if (transportname.isEmpty()){
            edt_tname.setError("Enter Transport Name");
            edt_tname.requestFocus();
            return;
        }
        if (truckNme.isEmpty()){
            edt_truckno.setError("Enter Truck Name");
            edt_truckno.requestFocus();
            return;
        }
        if (lrNo.isEmpty()){
            edt_lrno.setError("Enter LR No.");
            edt_lrno.requestFocus();
            return;
        }
        if (remark.isEmpty()){
            edt_remark.setError("Enter Remark");
            edt_remark.requestFocus();
            return;
        }


        if(Connectivity.isConnected(TaskDeliveryActivity.this)) {

            Dialog.showProgressBar(TaskDeliveryActivity.this, "Loading......");
            Call<TaskDeliveryResponse> call = RetrofitClient.getInstance().getApi().AddDeliveryTask(RetrofitClient.BASE_URL + "/users/" + userId + "/tasks/" + taskId + "/mark-completed", person, transportname, truckNme, lrNo, modetext, statustext, remark);
            call.enqueue(new Callback<TaskDeliveryResponse>() {
                @Override
                public void onResponse(Call<TaskDeliveryResponse> call, Response<TaskDeliveryResponse> response) {
                    if (response.isSuccessful()) ;
                    TaskDeliveryResponse taskDeliveryResponse = response.body();

                    if (taskDeliveryResponse.getStatus().equals("10100")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(TaskDeliveryActivity.this, taskDeliveryResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(TaskDeliveryActivity.this, TaskActivity.class);
                        intent.putExtra("taskId", taskId);
                        intent.putExtra("taskTypeId", tasktype_id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<TaskDeliveryResponse> call, Throwable t) {
                    Dialog.hideProgressBar();
                    Toast.makeText(TaskDeliveryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

}
