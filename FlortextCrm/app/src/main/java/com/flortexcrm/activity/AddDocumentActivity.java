package com.flortexcrm.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.utility.FileUtils;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDocumentActivity extends AppCompatActivity {

    private int GALLERY = 1;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    ProgressDialog progressDialog;

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtDocumentname)
    TextView txtDocumentname;
    @BindView(R.id.etDocumentname)
    EditText etDocumentname;
    @BindView(R.id.txtDocumentpath)
    TextView txtDocumentpath;
    @BindView(R.id.btnUpload)
    Button btnUpload;
    @BindView(R.id.imgeFile)
    ImageView imgeFile;
    @BindView(R.id.txtDocumentPath)
    TextView txtDocumentPath;
    @BindView(R.id.txtSave)
    TextView txtSave;
    String customerId, userId;
    File file;
    Uri uri;
    private boolean checkInternet;

    //    MultipartBody.Part body = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_document);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        checkInternet = NetworkChecking.isConnected(this);

        userId = PrefUtils.getUserId(AddDocumentActivity.this);

        if (getIntent() != null) {
            customerId = getIntent().getStringExtra("customerId");
        }
    }

    @OnClick({R.id.close_img, R.id.btnUpload, R.id.txtSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.btnUpload:
                getFile();
                break;
            case R.id.txtSave:
                if (checkInternet) {
                    addDocument();
                }else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void getFile() {

        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            finish();
            startActivity(intent);
            return;
        }

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(AddDocumentActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgeFile.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AddDocumentActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + file.getAbsolutePath());
            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void addDocument() {
        progressDialog.show();
        String docName = etDocumentname.getText().toString();

        if (docName.isEmpty()) {
            progressDialog.cancel();
            etDocumentname.setError("Enter Document Name");
            return;
        }

        // RequestBody docType = RequestBody.create(MediaType.parse("text/plain"), doctypeId);
        RequestBody documentName = RequestBody.create(MediaType.parse("text/plain"), docName);

        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("doc", file.getName(), requestFile);
        }

        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().addDocument(RetrofitClient.BASE_URL + "users/" + userId + "/customers/" + customerId + "/documents", body, documentName);
        call.enqueue(new Callback<FcmResponse>() {

            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();

                if (loginResponse.getStatus().equals("10100")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddDocumentActivity.this, DocumentsActivity.class);
                    intent.putExtra("customerId", customerId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else if (loginResponse.getStatus().equals("10200")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10300")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10400")) {
                    progressDialog.cancel();
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Log.e("RESPONSE", "" + t.getMessage());
            }
        });
    }
}
