package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBankDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtBankname)
    TextView txtBankname;
    @BindView(R.id.etBankname)
    EditText etBankname;
    @BindView(R.id.txtBankaccno)
    TextView txtBankaccno;
    @BindView(R.id.etBankaccno)
    EditText etBankaccno;
    @BindView(R.id.txtBankbranch)
    TextView txtBankbranch;
    @BindView(R.id.etBankbranch)
    EditText etBankbranch;
    @BindView(R.id.txtIfsccode)
    TextView txtIfsccode;
    @BindView(R.id.etIfsccode)
    EditText etIfsccode;
    @BindView(R.id.txtIsactive)
    TextView txtIsactive;
    @BindView(R.id.Isactive)
    Switch Isactive;
    @BindView(R.id.txtSave)
    TextView txtSave;

    Typeface bold, regular;
    String userId, userName,customerId;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(AddBankDetailsActivity.this);
        userName = PrefUtils.getUserName(AddBankDetailsActivity.this);

        checkInternet = NetworkChecking.isConnected(this);

        if (getIntent() != null){
            customerId = getIntent().getStringExtra("customerId");
        }

        toolbarTitle.setTypeface(bold);
        txtSave.setOnClickListener(this);
        closeImg.setOnClickListener(this);
    }

    @OnClick({R.id.close_img, R.id.txtSave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;


            case R.id.txtSave:
                if (checkInternet) {
                    addBankDetails();
                }else {
                    Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    //  Bankdetails
    private void addBankDetails() {

        if (etBankname.getText().toString().isEmpty()){
            etBankname.setError("Enter Bank Name");
            return;
        }
        if (etBankaccno.getText().toString().isEmpty()){
            etBankaccno.setError("Enter acc no.");
            return;
        }
        if (etBankbranch.getText().toString().isEmpty()){
            etBankbranch.setError("Enter Bank branch");
            return;
        }
        if (etIfsccode.getText().toString().isEmpty()){
            etIfsccode.setError("Enter IFSC code");
            return;
        }


        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().addBankDetails(RetrofitClient.BASE_URL+"users/"+userId+"/customers/"+customerId+"/bank-details",
                etBankname.getText().toString(),etBankaccno.getText().toString(),etBankbranch.getText().toString(),etIfsccode.getText().toString());
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();

                if ( loginResponse.getStatus().equals("10100")) {

                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddBankDetailsActivity.this,BankDetailsActivity.class);
                    intent.putExtra("customerId",customerId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
                else if (loginResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
                else if (loginResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Log.e("RESPONSE",""+t.getMessage());
            }
        });
    }


}
