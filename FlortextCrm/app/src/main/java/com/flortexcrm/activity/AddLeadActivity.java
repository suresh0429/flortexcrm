package com.flortexcrm.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.AddLeadResponse;
import com.flortexcrm.Response.GetCustomerResponse;
import com.flortexcrm.Response.LeadSpinnerResponse;
import com.flortexcrm.Response.SourceCustomerResponse;
import com.flortexcrm.adapter.FilterCustomerAdapter;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.adapter.SpinnerIndustriesAdapter;
import com.flortexcrm.adapter.SpinnerRatingAdapter;
import com.flortexcrm.adapter.SpinnerSourceTypeAdapter;
import com.flortexcrm.model.CustomerModel;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLeadActivity extends AppCompatActivity implements View.OnClickListener {

    TextView toolbar_title;
    Typeface bold, regular;
    String userId, userName;
    ImageView close_img;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtLeadcode)
    TextView txtLeadcode;
    @BindView(R.id.etleadcode)
    EditText etleadcode;
    @BindView(R.id.txtOwner)
    TextView txtOwner;
    @BindView(R.id.etOwner)
    EditText etOwner;
    @BindView(R.id.txtSourceType)
    TextView txtSourceType;
    @BindView(R.id.etSource_Type)
    AppCompatSpinner etSourceType;
    @BindView(R.id.txtSourceCustomer)
    TextView txtSourceCustomer;
    @BindView(R.id.etSource_Customer)
    AppCompatSpinner etSourceCustomer;
    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.etStatus)
    EditText etStatus;
    @BindView(R.id.txtMode)
    TextView txtMode;
    @BindView(R.id.etMode)
    EditText etMode;
    @BindView(R.id.txtCustomerName)
    TextView txtCustomerName;
    @BindView(R.id.txtSubject)
    TextView txtSubject;
    @BindView(R.id.etSubject)
    EditText etSubject;
    @BindView(R.id.txtIndustry)
    TextView txtIndustry;
    @BindView(R.id.etIndustry)
    AppCompatSpinner etIndustry;
    @BindView(R.id.txtRating)
    TextView txtRating;
    @BindView(R.id.etRating)
    AppCompatSpinner etRating;
    @BindView(R.id.txtAddLead)
    TextView txtAddLead;

    TextView customer_name;
    AlertDialog dialog;
    ArrayList<CustomerModel> spinnerModels = new ArrayList<CustomerModel>();
    FilterCustomerAdapter filterCustomerAdapter;

    ArrayList<SpinnerModel> sourceCustomerArray, customerArray;
    String sourceCustomerId, sourcetypeId, statusId, industryId, modeId, ratingId, c_id;
    //AutoCompleteTextView customer_name;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lead);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(AddLeadActivity.this);
        userName = PrefUtils.getUserName(AddLeadActivity.this);

        checkInternet = NetworkChecking.isConnected(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        //customer_name = findViewById(R.id.customer_name);
        customer_name = findViewById(R.id.customer_name);
        customer_name.setOnClickListener(this);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        txtAddLead.setOnClickListener(this);

        txtLeadcode.setTypeface(bold);
        etleadcode.setTypeface(regular);
        txtOwner.setTypeface(bold);
        etOwner.setTypeface(regular);
        txtSourceType.setTypeface(bold);
        txtSourceCustomer.setTypeface(bold);
        txtStatus.setTypeface(bold);
        txtMode.setTypeface(bold);
        txtCustomerName.setTypeface(bold);

        txtSubject.setTypeface(bold);
        etSubject.setTypeface(regular);
        txtIndustry.setTypeface(bold);
        txtRating.setTypeface(bold);

        etStatus.setText("OPEN");
        etMode.setText("EXISTING CUSTOMER");


        //autoComplete(customer_name, sourcetypeId);
        selectLeadSpinners();
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }

        if (v == txtAddLead) {

            if (checkInternet) {

                String leadcode = etleadcode.getText().toString();
                String owner = etOwner.getText().toString();

                String subject = etSubject.getText().toString();
                //String industry = etIndustry.getText().toString();
                // String rating = etRating.getText().toString();

                if (leadcode.isEmpty()) {
                    etleadcode.setError("Enter Lead Code");
                    return;
                }
                if (owner.isEmpty()) {
                    etOwner.setError("Enter Owner");
                    return;
                }

                if (sourceCustomerId == null) {

                    Toast.makeText(getApplicationContext(), "Please Select Source Customer*", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (c_id == null) {

                    Toast.makeText(getApplicationContext(), "Please Select Customer Name*", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (subject.isEmpty()) {
                    etSubject.setError("Enter subject");
                    return;
                } else {

                    addLead(leadcode, subject);
                }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

       /* if (v == customer_name) {
            customerDialog();
        }*/

    }


    private void selectLeadSpinners() {


        Call<LeadSpinnerResponse> call = RetrofitClient.getInstance().getApi().getSpinnersLead(RetrofitClient.BASE_URL + "users/" + userId + "/leads/create");

        call.enqueue(new Callback<LeadSpinnerResponse>() {
            @Override
            public void onResponse(Call<LeadSpinnerResponse> call, Response<LeadSpinnerResponse> response) {

                if (response.isSuccessful()) {

                    final List<LeadSpinnerResponse.DataBean.SourceTypeBean> sourceTypeBeanList = response.body() != null ? response.body().getData().getSource_type() : null;
                    final List<LeadSpinnerResponse.DataBean.IndustriesBean> industriesBeanList = response.body() != null ? response.body().getData().getIndustries() : null;
                    final List<LeadSpinnerResponse.DataBean.RatingsBean> ratingsBeanList = response.body() != null ? response.body().getData().getRatings() : null;

                    final String leadCode = response.body() != null ? response.body().getData().getLead_code() : null;
                    etleadcode.setText(leadCode);
                    etOwner.setText(userName);

                    // source type
                    SpinnerSourceTypeAdapter spinnerSourceTypeAdapter = new SpinnerSourceTypeAdapter(sourceTypeBeanList, AddLeadActivity.this);
                    etSourceType.setAdapter(spinnerSourceTypeAdapter);
                    etSourceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            sourcetypeId = sourceTypeBeanList.get(i).getId();
                            // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();
                            autoComplete(sourcetypeId);
                            Log.d("SourceTypeId", sourcetypeId);
                            customer_name.setText("Select Customer");
                            SourceCustomerSpinner(sourcetypeId);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    // Industry
                    SpinnerIndustriesAdapter spinnerIndustriesAdapter = new SpinnerIndustriesAdapter(industriesBeanList, AddLeadActivity.this);
                    etIndustry.setAdapter(spinnerIndustriesAdapter);
                    etIndustry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            industryId = industriesBeanList.get(i).getName();
                            // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                    // Rating
                    SpinnerRatingAdapter spinnerRatingAdapter = new SpinnerRatingAdapter(ratingsBeanList, AddLeadActivity.this);
                    etRating.setAdapter(spinnerRatingAdapter);
                    etRating.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            ratingId = ratingsBeanList.get(i).getName();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                } else {

                    //View parentLayout = findViewById(android.R.id.content);
                    // Snackbar.make(btnSerSubmit, getResources().getString(R.string.slowInternetconnection), Snackbar.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<LeadSpinnerResponse> call, Throwable t) {

            }
        });
    }

    private void SourceCustomerSpinner(String sourcetypeId) {
        sourceCustomerArray = new ArrayList<>();

        Call<SourceCustomerResponse> call = RetrofitClient.getInstance().getApi().getSourceCutomer(RetrofitClient.BASE_URL + "master/customers?customers_type_id=" + sourcetypeId + "&search=&user_id=" + userId);

        call.enqueue(new Callback<SourceCustomerResponse>() {
            @Override
            public void onResponse(Call<SourceCustomerResponse> call, Response<SourceCustomerResponse> response) {

                if (response.isSuccessful()) {

                    final List<SourceCustomerResponse.DataBean> sourceTypeBeanList = response.body() != null ? response.body().getData() : null;


                    if (sourceTypeBeanList != null) {
                        for (SourceCustomerResponse.DataBean prioritiesBean : sourceTypeBeanList) {

                            sourceCustomerArray.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }

                        etSourceCustomer.setVisibility(View.VISIBLE);
                        txtSourceCustomer.setVisibility(View.VISIBLE);
                    } else {

                        etSourceCustomer.setVisibility(View.GONE);
                        txtSourceCustomer.setVisibility(View.GONE);
                    }

                    // source type
                    SpinnerAdapter spinnerSourceTypeAdapter = new SpinnerAdapter(sourceCustomerArray, AddLeadActivity.this);
                    etSourceCustomer.setAdapter(spinnerSourceTypeAdapter);
                    etSourceCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            sourceCustomerId = sourceTypeBeanList.get(i).getName();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                } else {

                    //View parentLayout = findViewById(android.R.id.content);
                    // Snackbar.make(btnSerSubmit, getResources().getString(R.string.slowInternetconnection), Snackbar.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<SourceCustomerResponse> call, Throwable t) {

            }
        });
    }


    private void autoComplete(String sourcetypeId) {

        spinnerModels.clear();
        Call<GetCustomerResponse> customerResponseCall = RetrofitClient.getInstance().getApi().getCustomers(RetrofitClient.BASE_URL + "master/customers?customers_type_id=" + "&search=" + "&user_id=" + userId);
        customerResponseCall.enqueue(new Callback<GetCustomerResponse>() {
            @Override
            public void onResponse(Call<GetCustomerResponse> call, Response<GetCustomerResponse> response) {

                if (response.isSuccessful()) ;
                GetCustomerResponse getCustomerResponse = response.body();
                if (getCustomerResponse.getStatus().equals("10100")) {

                    final List<GetCustomerResponse.DataBean> sourceTypeBeanList = response.body() != null ? response.body().getData() : null;

                    if (sourceTypeBeanList != null) {
                        for (GetCustomerResponse.DataBean prioritiesBean : sourceTypeBeanList) {

                            spinnerModels.add(new CustomerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }
                    }

                    customer_name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customerDialog();
                        }
                    });


                } else if (getCustomerResponse.getStatus().equals("10200")) {
                    Toast.makeText(AddLeadActivity.this, getCustomerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (getCustomerResponse.getStatus().equals("10300")) {
                    Toast.makeText(AddLeadActivity.this, getCustomerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetCustomerResponse> call, Throwable t) {
                Toast.makeText(AddLeadActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    private void customerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddLeadActivity.this);
        LayoutInflater inflater = AddLeadActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.customer_filter_dialog, null);

        TextView filter_customer_title = dialog_layout.findViewById(R.id.filter_customer_title);
        filter_customer_title.setTypeface(regular);

        final SearchView filter_customer_search = dialog_layout.findViewById(R.id.filter_customer_search);
        EditText searchEditText = filter_customer_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTypeface(regular);

        RecyclerView filter_customer_recyclerview = dialog_layout.findViewById(R.id.filter_customer_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        filter_customer_recyclerview.setLayoutManager(layoutManager);

        filterCustomerAdapter = new FilterCustomerAdapter(spinnerModels, AddLeadActivity.this, R.layout.row_filter_customer_list);

        filter_customer_recyclerview.setAdapter(filterCustomerAdapter);
        filter_customer_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter_customer_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                customer_name.setText("Select Customer");
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        filter_customer_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                filterCustomerAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setCustomerName(String customerName, String customerId) {
        dialog.dismiss();
        filterCustomerAdapter.notifyDataSetChanged();
        customer_name.setText(customerName);
        c_id = customerId;
        Log.d("CITYDETAIL:", customerName + "/n" + customerId);
    }


    // retrofit add lead
    private void addLead(String leadcode, String subject) {


        Dialog.showProgressBar(AddLeadActivity.this, "Loading......");
        Call<AddLeadResponse> call = RetrofitClient.getInstance().getApi().addLead(RetrofitClient.BASE_URL + "users/" + userId + "/leads", leadcode, "OPEN",
                c_id, sourcetypeId, sourceCustomerId, subject, industryId, ratingId, "Yes", "Yes");
        call.enqueue(new Callback<AddLeadResponse>() {
            @Override
            public void onResponse(Call<AddLeadResponse> call, Response<AddLeadResponse> response) {

                Dialog.hideProgressBar();
                AddLeadResponse loginResponse = response.body();

                assert loginResponse != null;
                if ((loginResponse.getStatus().equals("10100"))) {

                    Toast.makeText(AddLeadActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AddLeadActivity.this, LeadsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (loginResponse.getStatus().equals("10200")) {
                    Toast.makeText(AddLeadActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddLeadResponse> call, Throwable t) {
               /* progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);*/
                Dialog.hideProgressBar();
            }
        });
    }

}
