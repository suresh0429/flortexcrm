package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.CitySpinnerresponse;
import com.flortexcrm.Response.CustomerSpinnerResponse;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddContactPersonActivity extends AppCompatActivity implements View.OnClickListener {
    Typeface bold, regular;
    String userId, userName;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtRole)
    TextView txtRole;
    @BindView(R.id.etRole)
    AppCompatSpinner etRole;
    @BindView(R.id.txtSalutation)
    TextView txtSalutation;
    @BindView(R.id.etSalutation)
    AppCompatSpinner etSalutation;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.txtMobile)
    TextView txtMobile;
    @BindView(R.id.eMobile)
    EditText eMobile;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.txtEmailNotify)
    TextView txtEmailNotify;
    @BindView(R.id.emailNotify)
    Switch emailNotify;
    @BindView(R.id.txtSmsNotify)
    TextView txtSmsNotify;
    @BindView(R.id.SmsNotify)
    Switch SmsNotify;
    @BindView(R.id.txtIsactive)
    TextView txtIsactive;
    @BindView(R.id.Isactive)
    Switch Isactive;
    @BindView(R.id.txtSave)
    TextView txtSave;
    String customer_id;

    ArrayList<SpinnerModel> salutationArrayList, roleArrayList;
    String roleID, salutationId, activeStatus, smsOptStatus, emailoptStatus;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_person);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(AddContactPersonActivity.this);
        userName = PrefUtils.getUserName(AddContactPersonActivity.this);

        checkInternet = NetworkChecking.isConnected(this);

        toolbarTitle.setTypeface(bold);
        selectRole();
        selectSalutaionSpinners();

        customer_id = getIntent().getStringExtra("customerId");

        Isactive.setChecked(false);
        activeStatus = "0";
        Isactive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    activeStatus = "1";
                } else {
                    activeStatus = "0";
                }
            }
        });

        emailNotify.setChecked(false);
        emailoptStatus = "No";
        emailNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    emailoptStatus = "Yes";
                } else {
                    emailoptStatus = "No";
                }
            }
        });

        SmsNotify.setChecked(false);
        smsOptStatus = "No";
        SmsNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    smsOptStatus = "Yes";
                } else {
                    smsOptStatus = "No";
                }
            }
        });
    }

    private void selectRole() {

        roleArrayList = new ArrayList<>();

        Call<CitySpinnerresponse> call = RetrofitClient.getInstance().getApi().getSpinnersCity(RetrofitClient.BASE_URL + "master/roles");
        call.enqueue(new Callback<CitySpinnerresponse>() {
            @Override
            public void onResponse(Call<CitySpinnerresponse> call, Response<CitySpinnerresponse> response) {

                if (response.isSuccessful()) {

                    final List<CitySpinnerresponse.DataBean> customersTypesBeanList = response.body() != null ? response.body().getData() : null;

                    if (customersTypesBeanList != null) {

                        for (CitySpinnerresponse.DataBean prioritiesBean : customersTypesBeanList) {

                            roleArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                        }

                    }
                    // city
                    SpinnerAdapter cityAdapter = new SpinnerAdapter(roleArrayList, AddContactPersonActivity.this);
                    etRole.setAdapter(cityAdapter);
                    etRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            roleID = roleArrayList.get(i).getId();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<CitySpinnerresponse> call, Throwable t) {

            }
        });

    }

    private void selectSalutaionSpinners() {


        salutationArrayList = new ArrayList<>();


        Call<CustomerSpinnerResponse> call = RetrofitClient.getInstance().getApi().getSpinnersCustomers(RetrofitClient.BASE_URL + "users/" + userId + "/customers/create");

        call.enqueue(new Callback<CustomerSpinnerResponse>() {
            @Override
            public void onResponse(Call<CustomerSpinnerResponse> call, Response<CustomerSpinnerResponse> response) {

                if (response.isSuccessful()) {

                    final List<CustomerSpinnerResponse.DataBean.SalutationsBean> salutationsBeanList = response.body() != null ? response.body().getData().getSalutations() : null;

                    final CustomerSpinnerResponse.DataBean dataBeans = response.body() != null ? response.body().getData() : null;


                    for (CustomerSpinnerResponse.DataBean.SalutationsBean tasksStatusBean : salutationsBeanList) {

                        salutationArrayList.add(new SpinnerModel(tasksStatusBean.getId(), tasksStatusBean.getName()));
                    }


                    // status
                    SpinnerAdapter statusAdapter = new SpinnerAdapter(salutationArrayList, AddContactPersonActivity.this);
                    etSalutation.setAdapter(statusAdapter);
                    etSalutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            salutationId = salutationArrayList.get(i).getId();
                            // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<CustomerSpinnerResponse> call, Throwable t) {

            }
        });
    }

    private void addContactPerson() {


        /*if (etEmail.getText().toString().isEmpty()){
            etEmail.setError("Enter Email");
            return;
        }*/
        if (etName.getText().toString().isEmpty()) {
            etName.setError("Enter Name");
            return;
        }
        if (eMobile.getText().toString().length() < 10) {
            eMobile.setError("Enter Mobile");
            return;
        }

        Dialog.showProgressBar(AddContactPersonActivity.this, "Adding Contact Person...");
        Call<FcmResponse> call = RetrofitClient.getInstance().getApi().addPostContactPerson(RetrofitClient.BASE_URL + "users/" + userId + "/customers/" + customer_id + "/contact-persons", roleID,
                salutationId, etName.getText().toString(), eMobile.getText().toString(), etEmail.getText().toString(), emailoptStatus, smsOptStatus, activeStatus
        );
        call.enqueue(new Callback<FcmResponse>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {

                FcmResponse loginResponse = response.body();
                Dialog.hideProgressBar();

                if (loginResponse.getStatus().equals("10100")) {

                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddContactPersonActivity.this, ContactPersonActivity.class);

                    intent.putExtra("customerId", customer_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else if (loginResponse.getStatus().equals("10200")) {
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10300")) {
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else if (loginResponse.getStatus().equals("10400")) {
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                Log.e("RESPONSE", "" + t.getMessage());
                Dialog.hideProgressBar();
            }
        });

    }


    @OnClick({R.id.close_img, R.id.txtSave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.txtSave:
                if (checkInternet) {
                    addContactPerson();
                } else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
