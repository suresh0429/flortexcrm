package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.LeadsResponse;
import com.flortexcrm.adapter.AllLeadsAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    RecyclerView recyclerViewLeads;
    TextView toolbar_title, add_leads;
    Typeface bold, regular;
    String userId;
    AllLeadsAdapter allLeadsAdapter;
    RecyclerView.LayoutManager layoutManager;
    SearchView mSearch_lead;
    TextView text_nodata;
    List<LeadsResponse.DataBean> docsBeanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads);
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(LeadsActivity.this);
        text_nodata=findViewById(R.id.text_nodata);


        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        add_leads = findViewById(R.id.add_leads);
        add_leads.setTypeface(regular);
        add_leads.setOnClickListener(this);

        mSearch_lead = findViewById(R.id.mSearch_lead);
        mSearch_lead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_lead.setIconified(false);

            }
        });
        mSearch_lead.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                if (query != null) {
                    allLeadsAdapter.getFilter().filter(query);
                    allLeadsAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });


        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recyclerViewLeads = findViewById(R.id.recyclerViewLeads);
        getLeadsData();
    }

    private void getLeadsData() {
        if (Connectivity.isConnected(LeadsActivity.this)) {
            Dialog.showProgressBar(LeadsActivity.this, "Loading Leads...");
            Call<LeadsResponse> call = RetrofitClient.getInstance().getApi().getAllLeadsList(RetrofitClient.BASE_URL + "users/" + userId + "/leads");
            call.enqueue(new Callback<LeadsResponse>() {
                @Override
                public void onResponse(Call<LeadsResponse> call, Response<LeadsResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    LeadsResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                         docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {
                            mSearch_lead.setVisibility(View.VISIBLE);
                            text_nodata.setVisibility(View.GONE);
                       /* for (ProductListResponse.DataBean address : docsBeanList) {

                            addressModelItems.add(new AddressModelItem(address.getId(), address.getUser_id(), address.getName(), address.getCity(), address.getState(), address.getZip(),
                                    address.getPhone(), address.getAddress(), address.getSet_as_default(), address.getStatus(), address.getEmail(), itemslength, module, customerId, totalPrice, checkoutStatus));
                        }*/

                            allLeadsAdapter = new AllLeadsAdapter((ArrayList<LeadsResponse.DataBean>) docsBeanList, LeadsActivity.this, R.layout.row_data_leads);
                            layoutManager = new LinearLayoutManager(LeadsActivity.this);
                            recyclerViewLeads.setNestedScrollingEnabled(false);
                            recyclerViewLeads.setLayoutManager(layoutManager);
                            recyclerViewLeads.setAdapter(allLeadsAdapter);

                        } else if (productListResponse.getStatus().equals("10200")){
                            mSearch_lead.setVisibility(View.GONE);
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }
                        else if (productListResponse.getStatus().equals("10300")){
                            mSearch_lead.setVisibility(View.GONE);
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }
                        else if (productListResponse.getStatus().equals("10400")){
                            mSearch_lead.setVisibility(View.GONE);
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }
                        else if (productListResponse.getStatus().equals("10500")){
                            mSearch_lead.setVisibility(View.GONE);
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<LeadsResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });
        } else {
            Toast.makeText(LeadsActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }
        if (v == add_leads) {
            Intent intent = new Intent(LeadsActivity.this, AddLeadActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}


