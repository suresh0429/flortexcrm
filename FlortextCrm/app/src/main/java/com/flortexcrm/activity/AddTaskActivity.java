package com.flortexcrm.activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.AddTaskPostResponse;
import com.flortexcrm.Response.CreateTaskResponse;
import com.flortexcrm.adapter.SpinnerAdapter;
import com.flortexcrm.model.SpinnerModel;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTaskActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Sample";
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private static final String STATE_TEXTVIEW = "STATE_TEXTVIEW";
    private SwitchDateTimeDialogFragment dateTimeFragment;
    int cyear, cmonth, cday, millisecond, second, minute, hour, hourofday;

    ArrayList<SpinnerModel> preriotyArrayList, emplyoyeeArrayList, taskstatusArrayList, taskTypeArrayList;
    TextView toolbar_title;
    Typeface bold, regular;
    String userId, userName;
    String[] listItems, listitemsId;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    ImageView close_img;
    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.txtAssign)
    TextView txtAssign;
    @BindView(R.id.etoptions)
    EditText etoptions;
    @BindView(R.id.txtTaskSubject)
    TextView txtTaskSubject;
    @BindView(R.id.etTaskSubject)
    EditText etTaskSubject;
    @BindView(R.id.txtTaskDetails)
    TextView txtTaskDetails;
    @BindView(R.id.etTaskDetails)
    EditText etTaskDetails;
    @BindView(R.id.txtDueDate)
    TextView txtDueDate;
    @BindView(R.id.etDueDate)
    EditText etDueDate;
    @BindView(R.id.txtPriority)
    TextView txtPriority;
    @BindView(R.id.etPriority)
    AppCompatSpinner etPriority;
    @BindView(R.id.txtType)
    TextView txtType;
    @BindView(R.id.etType)
    AppCompatSpinner etType;
    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.etStatus)
    AppCompatSpinner etStatus;
    @BindView(R.id.txtAddTask)
    TextView txtAddTask;

    String itemId = "";
    String statusId, typeId, priorityId;
    String selectedDate;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        ButterKnife.bind(this);

        checkInternet = NetworkChecking.isConnected(this);

        Calendar c = Calendar.getInstance();
        cyear = c.get(Calendar.YEAR);//calender year starts from 1900 so you must add 1900 to the value recevie.i.e., 1990+112 = 2012
        cmonth = c.get(Calendar.MONTH);//this is april so you will receive  3 instead of 4.
        cday = c.get(Calendar.DAY_OF_MONTH);
        millisecond = c.get(Calendar.MILLISECOND);
        second = c.get(Calendar.SECOND);
        minute = c.get(Calendar.MINUTE);
        //12 hour format
        hour = c.get(Calendar.HOUR);
        //24 hour format
        hourofday = c.get(Calendar.HOUR_OF_DAY);


        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        userId = PrefUtils.getUserId(AddTaskActivity.this);
        userName = PrefUtils.getUserName(AddTaskActivity.this);

        if (getIntent() != null) {
            selectedDate = getIntent().getStringExtra("SelectedDate");

            Log.e("RecDate", "" + selectedDate);

        }

        if (selectedDate != null) {
            etDueDate.setText("" + selectedDate);
        } else {
            etDueDate.setText("");
        }

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        txtAddTask.setOnClickListener(this);


        selectTaskpinners();
        timeDatePicker(savedInstanceState);

    }

    private void selectTaskpinners() {

        preriotyArrayList = new ArrayList<>();
        taskstatusArrayList = new ArrayList<>();
        emplyoyeeArrayList = new ArrayList<>();
        taskTypeArrayList = new ArrayList<>();

        Call<CreateTaskResponse> call = RetrofitClient.getInstance().getApi().getSpinnersTask(RetrofitClient.BASE_URL + "users/" + userId + "/tasks/create");

        call.enqueue(new Callback<CreateTaskResponse>() {
            @Override
            public void onResponse(Call<CreateTaskResponse> call, Response<CreateTaskResponse> response) {

                if (response.isSuccessful()) {

                    final List<CreateTaskResponse.DataBean.EmployeesBean> employeesBeanList = response.body() != null ? response.body().getData().getEmployees() : null;
                    final List<CreateTaskResponse.DataBean.TasksTypesBean> tasksTypesBeanList = response.body() != null ? response.body().getData().getTasks_types() : null;
                    final List<CreateTaskResponse.DataBean.TasksStatusBean> tasksStatusBeanList = response.body() != null ? response.body().getData().getTasks_status() : null;
                    final List<CreateTaskResponse.DataBean.PrioritiesBean> prioritiesBeanList = response.body() != null ? response.body().getData().getPriorities() : null;


                    for (CreateTaskResponse.DataBean.PrioritiesBean prioritiesBean : prioritiesBeanList) {

                        preriotyArrayList.add(new SpinnerModel(prioritiesBean.getId(), prioritiesBean.getName()));
                    }

                    for (CreateTaskResponse.DataBean.TasksStatusBean tasksStatusBean : tasksStatusBeanList) {

                        taskstatusArrayList.add(new SpinnerModel(tasksStatusBean.getId(), tasksStatusBean.getName()));
                    }


                    for (CreateTaskResponse.DataBean.TasksTypesBean tasksTypesBean : tasksTypesBeanList) {

                        taskTypeArrayList.add(new SpinnerModel(tasksTypesBean.getId(), tasksTypesBean.getName()));
                    }

                    List<String> employeeList = new ArrayList<>();
                    List<String> employeeListId = new ArrayList<>();
                    for (CreateTaskResponse.DataBean.EmployeesBean employeesBean : employeesBeanList) {

                        employeeList.add(employeesBean.getName());
                        employeeListId.add(employeesBean.getId());


                    }


                    // Priority
                    SpinnerAdapter preriotyAdapter = new SpinnerAdapter(preriotyArrayList, AddTaskActivity.this);
                    etPriority.setAdapter(preriotyAdapter);
                    etPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            priorityId = preriotyArrayList.get(i).getId();
                            // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    // status
                    SpinnerAdapter statusAdapter = new SpinnerAdapter(taskstatusArrayList, AddTaskActivity.this);
                    etStatus.setAdapter(statusAdapter);
                    etStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            statusId = taskstatusArrayList.get(i).getId();
                            // Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                    // type
                    SpinnerAdapter tasktypeAdapter = new SpinnerAdapter(taskTypeArrayList, AddTaskActivity.this);
                    etType.setAdapter(tasktypeAdapter);
                    etType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            typeId = taskTypeArrayList.get(i).getId();
                            //Toast.makeText(AddLeadActivity.this, "Company Name: " + companyName, Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                    // Employee

                    listitemsId = new String[employeeList.size()];
                    listitemsId = employeeListId.toArray(listitemsId);

                    listItems = new String[employeeList.size()];
                    listItems = employeeList.toArray(listItems);

                    checkedItems = new boolean[listItems.length];
                    checkedItems = new boolean[listitemsId.length];


                    etoptions.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {

                            view.setClickable(false);
                            view.setEnabled(false);

                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(AddTaskActivity.this);
                            mBuilder.setTitle(R.string.dialog_title);
                            mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {

                                    if (isChecked) {
                                        mUserItems.add(position);
                                    } else {
                                        mUserItems.remove((Integer.valueOf(position)));
                                    }
                                }
                            });

                            mBuilder.setCancelable(false);
                            mBuilder.setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    String item = "";

                                    for (int i = 0; i < mUserItems.size(); i++) {
                                        item = item + listItems[mUserItems.get(i)];
                                        if (i != mUserItems.size() - 1) {
                                            item = item + ", ";
                                        }
                                    }


                                    for (int i = 0; i < mUserItems.size(); i++) {
                                        itemId = itemId + listitemsId[mUserItems.get(i)];
                                        if (i != mUserItems.size() - 1) {
                                            itemId = itemId + ", ";
                                        }
                                    }


                                    etoptions.setText(item);
                                    view.setClickable(true);
                                    view.setEnabled(true);
                                }
                            });

                            mBuilder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    view.setClickable(true);
                                    view.setEnabled(true);
                                }
                            });

                            mBuilder.setNeutralButton(R.string.clear_all_label, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    for (int i = 0; i < checkedItems.length; i++) {
                                        checkedItems[i] = false;
                                        mUserItems.clear();
                                        etoptions.setText("");
                                        view.setClickable(true);
                                        view.setEnabled(true);
                                    }
                                }
                            });

                            AlertDialog mDialog = mBuilder.create();
                            mDialog.show();
                        }
                    });


                } else {

                    //View parentLayout = findViewById(android.R.id.content);
                    // Snackbar.make(btnSerSubmit, getResources().getString(R.string.slowInternetconnection), Snackbar.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<CreateTaskResponse> call, Throwable t) {

            }
        });
    }

    // time and Date picker
    private void timeDatePicker(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            // Restore value from saved state
            etDueDate.setText(savedInstanceState.getCharSequence(STATE_TEXTVIEW));
        }


        // Construct SwitchDateTimePicker
        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel),
                    getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d-MM-yyyy HH:mm", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setHighlightAMPMSelection(false);


        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // Set listener for date
        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {


                etDueDate.setText(myDateFormat.format(date));


            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                etDueDate.setText("");
            }
        });

        etDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Re-init each time
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(cyear, cmonth, cday, hourofday, minute).getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }

        if (v == txtAddTask) {
            if (checkInternet) {
            addPostTask();
            }else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void addPostTask() {

        String assignto = etoptions.getText().toString();
        String taskSubject = etTaskSubject.getText().toString();
        String taskDetails = etTaskDetails.getText().toString();
        final String dueDate = etDueDate.getText().toString();
        String prierity = etPriority.getSelectedItem().toString();
        String type = etType.getSelectedItem().toString();
        String status = etStatus.getSelectedItem().toString();


        if (assignto.isEmpty()) {
            etoptions.setError("Please select Some Options");
            return;
        }
        if (taskSubject.isEmpty()) {
            etTaskSubject.setError("Enter Task Subject");
            return;
        }
        if (taskDetails.isEmpty()) {
            etTaskDetails.setError("Enter Task Details");
            return;
        }

        if (dueDate.isEmpty()) {
            etDueDate.setError("Enter Date");
            return;
        }


        Dialog.showProgressBar(AddTaskActivity.this, "Loading...");
        Call<AddTaskPostResponse> call = RetrofitClient.getInstance().getApi().addTaskPost(RetrofitClient.BASE_URL + "users/" + userId + "/tasks", itemId, taskSubject, taskDetails, priorityId, typeId, statusId, dueDate);
        call.enqueue(new Callback<AddTaskPostResponse>() {
            @Override
            public void onResponse(Call<AddTaskPostResponse> call, Response<AddTaskPostResponse> response) {

                Dialog.hideProgressBar();
                AddTaskPostResponse addTaskPostResponse = response.body();

                assert addTaskPostResponse != null;
                if (addTaskPostResponse.getStatus() != null) {

                    if ((addTaskPostResponse.getStatus().equals("10100"))) {

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddTaskActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Success!");
                        sweetAlertDialog.setContentText("");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @TargetApi(Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        Intent intent = new Intent(AddTaskActivity.this, TaskActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);



                                    }
                                });
                        sweetAlertDialog.show();


                    } else {
                   /* int color = Color.RED;
                    snackBar(loginResponse.getMessage(), color);*/
                        Toast.makeText(AddTaskActivity.this, addTaskPostResponse.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<AddTaskPostResponse> call, Throwable t) {
                Dialog.hideProgressBar();
            }
        });

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the current textView
        savedInstanceState.putCharSequence(STATE_TEXTVIEW, etDueDate.getText());
        super.onSaveInstanceState(savedInstanceState);
    }


}
