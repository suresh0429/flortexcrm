package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.LeadProductsResponse;
import com.flortexcrm.adapter.LeadProductAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadProductsActivity extends AppCompatActivity {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.add_BankDetails)
    TextView addBankDetails;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.recyclerViewLProducts)
    RecyclerView recyclerViewLProducts;

    Typeface bold,regular;
    String userId,leadId;
    TextView text_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_products);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        toolbarTitle.setTypeface(bold);
        addBankDetails.setTypeface(regular);
        userId= PrefUtils.getUserId(LeadProductsActivity.this);
        leadId = getIntent().getStringExtra("leadId");

        text_nodata=findViewById(R.id.text_nodata);

        getLeadProductData();
    }

    private void getLeadProductData()
    {
        if(Connectivity.isConnected(LeadProductsActivity.this))
        {
            Dialog.showProgressBar(LeadProductsActivity.this, "Loading Products...");
            Call<LeadProductsResponse> call = RetrofitClient.getInstance().getApi().getLeadProductList(RetrofitClient.BASE_URL + "users/" + userId + "/leads/"+leadId+"/products");
            call.enqueue(new Callback<LeadProductsResponse>() {
                @Override
                public void onResponse(Call<LeadProductsResponse> call, Response<LeadProductsResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    LeadProductsResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<LeadProductsResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                            LeadProductAdapter leadProductAdapter = new LeadProductAdapter( LeadProductsActivity.this,docsBeanList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(LeadProductsActivity.this);
                            recyclerViewLProducts.setNestedScrollingEnabled(false);
                            recyclerViewLProducts.setLayoutManager(layoutManager);
                            recyclerViewLProducts.setAdapter(leadProductAdapter);

                        } else if (productListResponse.getStatus().equals("10300")){
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<LeadProductsResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });


        }
        else
        {
            Toast.makeText(LeadProductsActivity.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.close_img, R.id.add_BankDetails})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.add_BankDetails:

              Intent intent=new Intent(LeadProductsActivity.this,AddProductActivity.class);
              intent.putExtra("leadId",leadId);
              startActivity(intent);
                break;
        }
    }
}
