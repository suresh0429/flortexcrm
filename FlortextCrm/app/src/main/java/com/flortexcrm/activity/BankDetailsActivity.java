package com.flortexcrm.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.BankDetailResponse;
import com.flortexcrm.adapter.BankDetailsAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.add_BankDetails)
    TextView addBankDetails;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.bankRecycler)
    RecyclerView bankRecycler;

    Typeface bold,regular;
    String userId,customerId;
    BankDetailsAdapter bankDetailsAdapter;
    RecyclerView.LayoutManager layoutManager;
    TextView txt_nodata;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details2);
        ButterKnife.bind(this);

        checkInternet = NetworkChecking.isConnected(this);

        closeImg.setOnClickListener(this);
        addBankDetails.setOnClickListener(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        toolbarTitle.setTypeface(bold);
        addBankDetails.setTypeface(regular);

        userId= PrefUtils.getUserId(BankDetailsActivity.this);

        txt_nodata=findViewById(R.id.txt_nodata);

        if (getIntent() != null){
            customerId = getIntent().getStringExtra("customerId");
        }

        if (checkInternet) {
        getBankDetails();
        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBankDetails()
    {
        if(Connectivity.isConnected(BankDetailsActivity.this))
        {
            Dialog.showProgressBar(BankDetailsActivity.this, "Loading Bank Details...");
            Call<BankDetailResponse> call = RetrofitClient.getInstance().getApi().getBankDetails(RetrofitClient.BASE_URL + "users/" + userId + "/customers/"+customerId+"/bank-details");
            call.enqueue(new Callback<BankDetailResponse>() {
                @Override
                public void onResponse(Call<BankDetailResponse> call, Response<BankDetailResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    BankDetailResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<BankDetailResponse.DataBean> docsBeanList = Collections.singletonList(response.body() != null ? response.body().getData() : null);

                        if ((productListResponse.getStatus().equals("10100"))) {


                            bankDetailsAdapter = new BankDetailsAdapter(BankDetailsActivity.this, docsBeanList);
                            layoutManager = new LinearLayoutManager(BankDetailsActivity.this);
                            bankRecycler.setNestedScrollingEnabled(false);
                            bankRecycler.setLayoutManager(layoutManager);
                            bankRecycler.setAdapter(bankDetailsAdapter);

                        } else if (productListResponse.getStatus().equals("10200")){
                            txt_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }
                        else if (productListResponse.getStatus().equals("10300")){
                            txt_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }
                        else if (productListResponse.getStatus().equals("10400")){
                            txt_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<BankDetailResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });


        }
        else
        {
            Toast.makeText(BankDetailsActivity.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick({R.id.close_img, R.id.add_BankDetails})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_img:
                finish();
                break;
            case R.id.add_BankDetails:
                if (checkInternet) {
                Intent intent = new Intent(BankDetailsActivity.this, AddBankDetailsActivity.class);
                intent.putExtra("customerId",customerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                }else {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


}
