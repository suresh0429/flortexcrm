package com.flortexcrm.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Reciver.AlarmReceiver;
import com.flortexcrm.Response.NotificationCount;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.fragments.AccountFragment;
import com.flortexcrm.fragments.HomeFragment;
import com.flortexcrm.fragments.NotificationFragment;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.PrefUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    boolean notificationValue = true;
    String userId;
    ArrayList<String> dueDateArray = new ArrayList<>();
    List<Date> dateList = new ArrayList<>();
    Handler mHandler;
    int notificationKey;
    BottomNavigationView navigation;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       /* this.mHandler = new Handler();
         m_Runnable.run();*/

        userId = PrefUtils.getUserId(HomeActivity.this);

        if (getIntent().getExtras() != null){


            notificationKey = getIntent().getIntExtra("notificationKey",0);
            Log.e("NOTIFICATION",""+notificationKey);
        }


       // mTextMessage = (TextView) findViewById(R.id.message);
         navigation = (BottomNavigationView) findViewById(R.id.navigation);
         navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        if (notificationKey == 2){
            getSupportActionBar().setTitle("Notifications");
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);
            getSupportActionBar().show();
            NotificationFragment notificationFragment = new NotificationFragment();
            FragmentManager manager3 =getSupportFragmentManager();
            manager3.beginTransaction().replace(R.id.continer,notificationFragment,notificationFragment.getTag()).commit();
            navigation.getMenu().getItem(notificationKey).setChecked(true);
        }
        else {
            // default fragment after launch
            getSupportActionBar().setTitle("Home");
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);
            getSupportActionBar().show();
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager manager1 = getSupportFragmentManager();
            manager1.beginTransaction().replace(R.id.continer, homeFragment, homeFragment.getTag()).commit();
            //  navigation.getMenu().getItem(notificationKey).setChecked(true);

        }



        getNotificationCount(navigation);
       // getTaskData();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    // default fragment after launch
                    getSupportActionBar().setTitle("Home");
                    getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                    getSupportActionBar().setCustomView(R.layout.abs_layout);
                    getSupportActionBar().show();
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentManager manager1 =getSupportFragmentManager();
                    manager1.beginTransaction().replace(R.id.continer,homeFragment,homeFragment.getTag()).commit();
                    return true;
                case R.id.navigation_dashboard:
                    getSupportActionBar().setTitle("Account");
                    getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                    getSupportActionBar().setCustomView(R.layout.abs_layout);
                    getSupportActionBar().show();
                    AccountFragment accountFragment = new AccountFragment();
                    FragmentManager manager2 =getSupportFragmentManager();
                    manager2.beginTransaction().replace(R.id.continer,accountFragment,accountFragment.getTag()).commit();
                    return true;
                case R.id.navigation_notifications:

                    getSupportActionBar().setTitle("Notifications");
                    getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                    getSupportActionBar().setCustomView(R.layout.abs_layout);
                    getSupportActionBar().show();
                    NotificationFragment notificationFragment = new NotificationFragment();
                    FragmentManager manager3 =getSupportFragmentManager();
                    manager3.beginTransaction().replace(R.id.continer,notificationFragment,notificationFragment.getTag()).commit();

                    removeBadge(navigation,R.id.navigation_notifications);

                    return true;
            }
            return false;
        }
    };

   /* private final Runnable m_Runnable = new Runnable()
    {
        public void run()

        {
           // Toast.makeText(HomeActivity.this,"in runnable",Toast.LENGTH_SHORT).show();

            HomeActivity.this.mHandler.postDelayed(m_Runnable,20000);
           // getTaskData();
        }

    };*/


    private void getNotificationCount(final BottomNavigationView navigation) {
        if (Connectivity.isConnected(HomeActivity.this)) {

            Call<NotificationCount> call = RetrofitClient.getInstance().getApi().getNotificationCount(RetrofitClient.BASE_URL + "users/" + userId + "/counts");
            call.enqueue(new Callback<NotificationCount>() {
                @Override
                public void onResponse(Call<NotificationCount> call, Response<NotificationCount> response) {

                    NotificationCount productListResponse = response.body();

                    if (response.isSuccessful()) {

                        if ((productListResponse.getStatus().equals("10100"))) {

                            BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) navigation.getChildAt(0);
                            View v = bottomNavigationMenuView.getChildAt(2);
                            BottomNavigationItemView itemView = (BottomNavigationItemView) v;

                            View badge = LayoutInflater.from(HomeActivity.this)
                                    .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);


                            if (!productListResponse.getData().getNotificationCount().equalsIgnoreCase("0")){

                                TextView tv = badge.findViewById(R.id.notification_badge);
                                tv.setText(productListResponse.getData().getNotificationCount());
                                itemView.addView(badge);
                            }






                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<NotificationCount> call, Throwable t) {

                }
            });


        } else {
            Toast.makeText(HomeActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
