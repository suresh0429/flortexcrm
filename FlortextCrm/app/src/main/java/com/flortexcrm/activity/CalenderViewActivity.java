package com.flortexcrm.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.adapter.EventsAdapter;
import com.flortexcrm.model.CalendarModel;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalenderViewActivity extends AppCompatActivity {
    private final String TAG = "CalenderViewActivity";
    @BindView(R.id.listEvents)
    ListView listEvents;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private boolean shouldShow = false;
    long timeinmillis;
    List<Event> events;

    TextView toolbar_title;
    Typeface bold, regular;
    String userId, userName, strDate, strTime, strData;

    @BindView(R.id.close_img)
    ImageView closeImg;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;
    @BindView(R.id.compactcalendar_view)
    CompactCalendarView compactcalendarView;
    @BindView(R.id.prev_button)
    ImageButton prevButton;
    @BindView(R.id.monthYearText)
    TextView monthYearText;
    @BindView(R.id.next_button)
    ImageButton nextButton;
    Event ev1;
    Date currentDate;
    private boolean checkInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_view);
        ButterKnife.bind(this);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));

        checkInternet = NetworkChecking.isConnected(this);

        userId = PrefUtils.getUserId(CalenderViewActivity.this);
        userName = PrefUtils.getUserName(CalenderViewActivity.this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        monthYearText.setText("" + dateFormatForMonth.format(compactcalendarView.getFirstDayOfCurrentMonth()));

        if (checkInternet) {
        getCalenderView();
        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCalenderView() {


        // Use constants provided by Java Calendar class
        compactcalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        if (Connectivity.isConnected(CalenderViewActivity.this)) {
            Dialog.showProgressBar(CalenderViewActivity.this, "Loading Calender...");
            Call<TaskResponse> call = RetrofitClient.getInstance().getApi().getAllTaskList(RetrofitClient.BASE_URL + "users/" + userId + "/tasks");
            call.enqueue(new Callback<TaskResponse>() {
                @Override
                public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    TaskResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<TaskResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                            for (TaskResponse.DataBean dataBean : docsBeanList) {


                                List<TaskResponse.DataBean.AssignedToEmployeesBean> assignedToEmployeesBeanList = dataBean.getAssignedToEmployees();

                                if (assignedToEmployeesBeanList != null) {
                                    for (TaskResponse.DataBean.AssignedToEmployeesBean assignedToEmployeesBean : assignedToEmployeesBeanList) {

                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date date = null;
                                        try {
                                            date = sdf.parse(assignedToEmployeesBean.getDueDate());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        timeinmillis = date.getTime();
                                    }
                                }


                                ev1 = new Event(Color.YELLOW, timeinmillis, dataBean.getTaskSubject()+"_"+dataBean.getId()+"_"+dataBean.getCreatedOn()+"_"+dataBean.getStatus());
                                compactcalendarView.addEvent(ev1);
                            }
                            calenderView();

                        } else if ((productListResponse.getStatus().equals("10300"))) {

                            Toast.makeText(CalenderViewActivity.this, productListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            calenderView();


                        }
                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<TaskResponse> call, Throwable t) {

                    Dialog.hideProgressBar();
                }
            });


        } else {
            Toast.makeText(CalenderViewActivity.this, "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("RestrictedApi")
    private void calenderView() {
        DateFormat dateFormat = new SimpleDateFormat("d-MM-yyyy");
        Date date = new Date();
        strDate = dateFormat.format(date);
        events = compactcalendarView.getEvents(date);
        Log.e("events", "" + events);


        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        Date currentTime = Calendar.getInstance().getTime();
        strTime = timeFormat.format(currentTime);

        strData = strDate + " " + strTime;

        Log.e("StrDay", "" + strData);

        if (!events.isEmpty()) {

            getevents(events);

            fab.setVisibility(View.VISIBLE);

        } else {

           /* Intent intent = new Intent(CalenderViewActivity.this, AddTaskActivity.class);
            intent.putExtra("SelectedDate", strData);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
            listEvents.setAdapter(null);
            fab.setVisibility(View.VISIBLE);
        }

        // define a listener to receive callbacks when certain events happen.
        compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDayClick(Date dateClicked) {
                monthYearText.setText("" + dateFormatForMonth.format(dateClicked));
                events = compactcalendarView.getEvents(dateClicked);
                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);

                Date newDate = new Date();
                DateFormat dateFormat = new SimpleDateFormat("d-MM-yyyy");
                String currentdString = dateFormat.format(newDate);
                try {
                    currentDate = dateFormat.parse(currentdString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                strDate = dateFormat.format(dateClicked);

                DateFormat timeFormat = new SimpleDateFormat("HH:mm");
                Date currentTime = Calendar.getInstance().getTime();
                strTime = timeFormat.format(currentTime);

                strData = strDate + " " + strTime;

                Log.e("StrDay", "" + strData);
                Log.e("CHECK", "" + currentDate + "\n" + dateClicked);

                if (currentDate.compareTo(dateClicked) > 0) {
                    fab.setVisibility(View.GONE);
                    Log.e("CMPDATE", "less");
                    listEvents.setAdapter(null);

                } else if (currentDate.compareTo(dateClicked) < 0) {

                    fab.setVisibility(View.VISIBLE);
                    Log.e("CMPDATE", "greater");

                    if (!events.isEmpty()) {

                        getevents(events);

                        fab.setVisibility(View.VISIBLE);

                    } else {

                        /*Intent intent = new Intent(CalenderViewActivity.this, AddTaskActivity.class);
                        intent.putExtra("SelectedDate", strData);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);*/
                        listEvents.setAdapter(null);
                        fab.setVisibility(View.VISIBLE);
                    }

                } else if (currentDate.compareTo(dateClicked) == 0) {

                    fab.setVisibility(View.VISIBLE);
                    Log.e("CMPDATE", "equal");

                    if (!events.isEmpty()) {

                        getevents(events);

                        fab.setVisibility(View.VISIBLE);

                    } else {

                        /*Intent intent = new Intent(CalenderViewActivity.this, AddTaskActivity.class);
                        intent.putExtra("SelectedDate", strData);
                        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);*/
                        listEvents.setAdapter(null);
                        fab.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                monthYearText.setText("" + dateFormatForMonth.format(firstDayOfNewMonth));
                listEvents.setAdapter(null);
                fab.setVisibility(View.GONE);
            }
        });
    }

    private void getevents(List<Event> events) {

        final List<CalendarModel> eventsArray = new ArrayList<CalendarModel>();
        for (Event event : events) {

            String cc = String.valueOf(event.getData());
            StringTokenizer strToken = new StringTokenizer(cc, "_");
            String event_data = strToken.nextToken();
            String t_id = strToken.nextToken();
            String update_id = strToken.nextToken();
            String status = strToken.nextToken();

            Log.d("CheckData",cc+"/n"+event_data+"/n"+t_id);
            //eventsArray.add(String.valueOf(event.getData()));
            eventsArray.add(new CalendarModel(event_data,t_id,update_id,status));
        }



        EventsAdapter adapter=new EventsAdapter(this,R.layout.list_view_items,eventsArray);
        listEvents.setAdapter(adapter);

        listEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(CalenderViewActivity.this,TaskDetailsActivity.class);
                intent.putExtra("taskId",eventsArray.get(position).getT_id());
                Log.d("IIIII",eventsArray.get(position).getT_id());
                startActivity(intent);

            }
        });
    }


    @SuppressLint("RestrictedApi")
    @OnClick({R.id.prev_button, R.id.close_img, R.id.next_button, R.id.fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.prev_button:
                compactcalendarView.scrollLeft();
                listEvents.setAdapter(null);
                fab.setVisibility(View.GONE);
                break;
            case R.id.next_button:
                compactcalendarView.scrollRight();
                listEvents.setAdapter(null);
                fab.setVisibility(View.GONE);
                break;
            case R.id.fab:
                Intent intent = new Intent(CalenderViewActivity.this, AddTaskActivity.class);
                intent.putExtra("SelectedDate", strData);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.close_img:
                finish();
                break;
        }
    }


}
