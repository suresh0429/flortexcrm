package com.flortexcrm.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.StockResponse;
import com.flortexcrm.adapter.AllStocksAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StocksActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView close_img;
    RecyclerView recyclerViewStocks;
    TextView toolbar_title;
    Typeface bold,regular;
    String userId;
    AllStocksAdapter allStocksAdapter;
    RecyclerView.LayoutManager layoutManager;
    SearchView mSearch_stock;
    TextView text_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stocks);

        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        userId= PrefUtils.getUserId(StocksActivity.this);
        toolbar_title = findViewById(R.id.toolbar_title);
        text_nodata=findViewById(R.id.text_nodata);
        toolbar_title.setTypeface(bold);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        recyclerViewStocks = findViewById(R.id.recyclerViewStocks);
        mSearch_stock = findViewById(R.id.mSearch_stock);
        mSearch_stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_stock.setIconified(false);

            }
        });
        mSearch_stock.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                allStocksAdapter.getFilter().filter(query);
                allStocksAdapter.notifyDataSetChanged();
                return true;
            }
        });


        getStocksData();
    }

    private void getStocksData()
    {
        if(Connectivity.isConnected(StocksActivity.this))
        {
            Dialog.showProgressBar(StocksActivity.this, "Loading Stocks...");
            Call<StockResponse> call = RetrofitClient.getInstance().getApi().getAllStockList(RetrofitClient.BASE_URL + "stock/" + "hyderbad");
            call.enqueue(new Callback<StockResponse>() {
                @Override
                public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                    Dialog.hideProgressBar();
                    StockResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        List<StockResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                       /* for (ProductListResponse.DataBean address : docsBeanList) {

                            addressModelItems.add(new AddressModelItem(address.getId(), address.getUser_id(), address.getName(), address.getCity(), address.getState(), address.getZip(),
                                    address.getPhone(), address.getAddress(), address.getSet_as_default(), address.getStatus(), address.getEmail(), itemslength, module, customerId, totalPrice, checkoutStatus));
                        }*/

                            allStocksAdapter = new AllStocksAdapter((ArrayList<StockResponse.DataBean>) docsBeanList, StocksActivity.this, R.layout.row_data_stocks);
                            layoutManager = new LinearLayoutManager(StocksActivity.this);
                            recyclerViewStocks.setNestedScrollingEnabled(false);
                            recyclerViewStocks.setLayoutManager(layoutManager);
                            recyclerViewStocks.setAdapter(allStocksAdapter);

                        } else if (productListResponse.getStatus().equals("10300")){
                            text_nodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<StockResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });
        }
        else
        {
            Toast.makeText(StocksActivity.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if(v==close_img)
        {
            finish();
        }

    }



}
