package com.flortexcrm.utility;

import android.app.Service;
import android.text.Annotation;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.Response.LoginErrorResponse;


import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
    public static LoginErrorResponse parseError(Response<?> response) {
        Converter<ResponseBody, LoginErrorResponse> converter =
                RetrofitClient.retrofit()
                        .responseBodyConverter(LoginErrorResponse.class, (java.lang.annotation.Annotation[]) new Annotation[0]);

        LoginErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new LoginErrorResponse();
        }

        return error;
    }
}
