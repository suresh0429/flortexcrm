package com.flortexcrm.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.R;
import com.flortexcrm.Response.NotificationsResponse;
import com.flortexcrm.adapter.NotificationAdapter;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.Dialog;
import com.flortexcrm.utility.PrefUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {
    View view;
    @BindView(R.id.notificationRecycler)
    RecyclerView notificationRecycler;
    Unbinder unbinder;

    String userId, taskId;
    @BindView(R.id.txt_nodata)
    TextView txtNodata;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);

        userId = PrefUtils.getUserId(getActivity());

        getTaskDetailsData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getTaskDetailsData() {
        if (Connectivity.isConnected(getActivity())) {
            Dialog.showProgressBar(getActivity(), "Loading Notifications...");
            Call<NotificationsResponse> call = RetrofitClient.getInstance().getApi().getAllNotifications(RetrofitClient.BASE_URL + "users/" + userId + "/notifications");
            call.enqueue(new Callback<NotificationsResponse>() {
                @Override
                public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                    // progress.setVisibility(View.GONE);
                    Dialog.hideProgressBar();
                    NotificationsResponse productListResponse = response.body();

                    if (response.isSuccessful()) {
                        List<NotificationsResponse.DataBean> docsBeanList = response.body() != null ? response.body().getData() : null;

                        if ((productListResponse.getStatus().equals("10100"))) {
                            NotificationAdapter notificationAdapter = new NotificationAdapter(getContext(), docsBeanList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

                            if (notificationRecycler != null) {
                                notificationRecycler.setNestedScrollingEnabled(false);
                                notificationRecycler.setLayoutManager(layoutManager);
                                notificationRecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
                                notificationRecycler.setAdapter(notificationAdapter);
                            }

                        } else if (productListResponse.getStatus().equals("10200")) {
                            notificationRecycler.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        } else if (productListResponse.getStatus().equals("10300")) {
                            notificationRecycler.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        } else if (productListResponse.getStatus().equals("10400")) {
                            notificationRecycler.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        } else if (productListResponse.getStatus().equals("10500")) {
                            notificationRecycler.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                            // Snackbar.make(addressList, "No address Found !", Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        // error case
                        switch (response.code()) {
                            case 404:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            case 500:
                                // Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                            default:
                                //Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<NotificationsResponse> call, Throwable t) {
               /* progress.setVisibility(View.GONE);
                Snackbar.make(addressList, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();
*/
                    Dialog.hideProgressBar();
                }
            });


        } else {
            Toast.makeText(getActivity(), "No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }
}
