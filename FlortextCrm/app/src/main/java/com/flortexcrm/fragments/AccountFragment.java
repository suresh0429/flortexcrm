package com.flortexcrm.fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flortexcrm.R;
import com.flortexcrm.activity.LoginActivity;
import com.flortexcrm.utility.NetworkChecking;
import com.flortexcrm.utility.PrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AccountFragment extends Fragment implements View.OnClickListener {
    View view;
    TextView text_logout;
    Typeface regular, bold;
    SweetAlertDialog sd;
    private boolean checkInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_account, container, false);

        regular = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.bold));
        text_logout = view.findViewById(R.id.text_logout);
        text_logout.setOnClickListener(this);
        text_logout.setTypeface(bold);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == text_logout) {
            checkInternet = NetworkChecking.isConnected(getActivity());
            if (checkInternet) {
                sd = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
                sd.setTitleText("Are you sure?");
                sd.setContentText("Do You Want To Logout From Flortex!");
                sd.setCancelText("No");
                sd.setConfirmText("Yes");
                sd.showCancelButton(true);

                sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        PrefUtils.clearLoginPrefs(getContext());
                        startActivity(intent);
                    }
                });


                sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                });
                sd.show();
            } else {
                Toast.makeText(getActivity(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
