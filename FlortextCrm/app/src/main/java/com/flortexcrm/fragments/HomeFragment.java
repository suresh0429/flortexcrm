package com.flortexcrm.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flortexcrm.R;
import com.flortexcrm.adapter.HomeCategoryAdapter;
import com.flortexcrm.model.HomeCategoryModel;

public class HomeFragment extends Fragment
{

    View view;
    RecyclerView recyclerViewCategory;
    HomeCategoryAdapter homeCategoryAdapter;
    GridLayoutManager gridLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.fragment_home, container, false);

        recyclerViewCategory=view.findViewById(R.id.recyclerViewCategory);

        HomeCategoryModel[] data = new HomeCategoryModel[] {
                new HomeCategoryModel("Calender", R.drawable.ic_calender),
                new HomeCategoryModel("Leads", R.drawable.ic_leads),
                new HomeCategoryModel("Task",R.drawable.ic_task),
                new HomeCategoryModel("Customers", R.drawable.ic_customer),
                new HomeCategoryModel("Stock",R.drawable.ic_stock)
        };

        homeCategoryAdapter = new HomeCategoryAdapter(data, HomeFragment.this,R.layout.row_home_category);
        recyclerViewCategory.setNestedScrollingEnabled(false);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerViewCategory.setLayoutManager(gridLayoutManager);
        recyclerViewCategory.setAdapter(homeCategoryAdapter);

        return view;
    }

}
