package com.flortexcrm.Apis;

import android.content.Context;

import com.flortexcrm.Response.AddGstDetailResponse;
import com.flortexcrm.Response.AddLeadProductResponse;
import com.flortexcrm.Response.AddLeadResponse;
import com.flortexcrm.Response.AddTaskPostResponse;
import com.flortexcrm.Response.BankDetailResponse;
import com.flortexcrm.Response.CitySpinnerresponse;
import com.flortexcrm.Response.ContactPersonResponse;
import com.flortexcrm.Response.CreateTaskResponse;
import com.flortexcrm.Response.CustomerResponse;
import com.flortexcrm.Response.CustomerSpinnerResponse;
import com.flortexcrm.Response.DocumentsResponse;
import com.flortexcrm.Response.FcmResponse;
import com.flortexcrm.Response.GSTResponse;
import com.flortexcrm.Response.GetCustomerResponse;
import com.flortexcrm.Response.LeadDocTypeSpinnerResponse;
import com.flortexcrm.Response.LeadDocumentResponse;
import com.flortexcrm.Response.LeadProductsResponse;
import com.flortexcrm.Response.LeadSpinnerResponse;
import com.flortexcrm.Response.LeadsResponse;
import com.flortexcrm.Response.LoginResponse;
import com.flortexcrm.Response.NotificationCount;
import com.flortexcrm.Response.NotificationsResponse;
import com.flortexcrm.Response.ProductListResponse;
import com.flortexcrm.Response.ProductsResponse;
import com.flortexcrm.Response.SaleTypeResponse;
import com.flortexcrm.Response.SourceCustomerResponse;
import com.flortexcrm.Response.StockResponse;
import com.flortexcrm.Response.TaskDateResponse;
import com.flortexcrm.Response.TaskDeliveryResponse;
import com.flortexcrm.Response.TaskDetailResponse;
import com.flortexcrm.Response.TaskInstallationResponse;
import com.flortexcrm.Response.TaskOtherResponse;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.Response.TaskReviewResponse;
import com.flortexcrm.utility.PrefUtils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;



public interface Api {

    Context context = null;

    String userId= PrefUtils.getUserId(context);

    @FormUrlEncoded
    @POST("verify-login")
    Call<LoginResponse> userLogin(@Field("email") String email, @Field("password") String password,
                                  @Field("device_id") String device_id, @Field("device_name") String device_name,
                                  @Field("platform") String platform);

    @GET("products")
    Call<ProductListResponse> getProductList(@Query("perpage") String perpage, @Query("page") String page);



    @GET
    Call<LeadsResponse> getAllLeadsList(@Url String url);

    @GET
    Call<TaskResponse> getAllTaskList(@Url String url);

    @GET
    Call<StockResponse> getAllStockList(@Url String url);

    @GET
    Call<CustomerResponse> getAllCustomerList(@Url String url);

    @GET
    Call<LeadSpinnerResponse> getSpinnersLead(@Url String url);

    @GET
    Call<CreateTaskResponse> getSpinnersTask(@Url String url);


    @FormUrlEncoded
    @POST()
    Call<AddTaskPostResponse> addTaskPost(@Url String apiname,
                                          @Field("assigned_to") String assigned_to, @Field("task_subject") String task_subject,
                                          @Field("task_details") String task_details, @Field("priority_id") String priority_id,
                                          @Field("task_type_id") String task_type_id, @Field("task_status_id") String task_status_id,
                                          @Field("started_on") String started_on);


    @GET
    Call<TaskDetailResponse> getAllTaskDetailsList(@Url String url);

    @FormUrlEncoded
    @POST()
    Call<TaskDateResponse> updateTaskDate(@Url String apiName,
                                          @Field("started_on") String started_on,
                                          @Field("employee_id") String employee_id,
                                          @Field("reason") String reason);

    @FormUrlEncoded
    @POST()
    Call<TaskReviewResponse> updateTaskReview(@Url String apiName,
                                              @Field("comment") String comment);
    @FormUrlEncoded
    @POST()
    Call<TaskDateResponse> assignTak(@Url String apiName, @Field("started_on") String duedate, @Field("assigned_to") String assigned_to);

    @GET
    Call<NotificationsResponse> getAllNotifications(@Url String url);

    @GET
    Call<NotificationCount> getNotificationCount(@Url String url);

    @GET
    Call<ContactPersonResponse> getContactPersonList(@Url String  url);

    @GET
    Call<BankDetailResponse> getBankDetails(@Url String  url);

    @GET
    Call<DocumentsResponse> getDocuments(@Url String  url);

    @GET
    Call<CustomerSpinnerResponse> getSpinnersCustomers(@Url String  url);

    @GET
    Call<CitySpinnerresponse> getSpinnersCity(@Url String  url);

    @GET
    Call<SaleTypeResponse> getSpinnersSaleType(@Url String  url);

    @GET
    Call<GSTResponse> getGSTDetails(@Url String  url);

    @GET
    Call<LeadDocumentResponse> getLeadDocuments(@Url String  url);

    @GET
    Call<LeadProductsResponse> getLeadProductList(@Url String  url);

    @GET
    Call<SourceCustomerResponse> getSourceCutomer(@Url String  url);

    @FormUrlEncoded
    @POST()
    Call<AddLeadResponse> addLead(@Url String apiName, @Field("lead_code") String lead_code, @Field("status") String status
            , @Field("customer_id") String customer_id, @Field("source_type_id") String source_type_id
            , @Field("source_person_id") String source_person_id, @Field("subject") String subject
            , @Field("industry_id") String industry_id, @Field("rating_id") String rating_id
            , @Field("email_opt") String email_opt, @Field("sms_opt") String sms_opt);

    @GET
    Call<ProductsResponse> getProductsName(@Url String  url);

    @FormUrlEncoded
    @POST()
    Call<FcmResponse> fcmToken(@Url String  url,@Field("token") String tocken,@Field("platform") String platform);


    @Multipart
    @POST()
    Call<FcmResponse> addLeadDocument(@Url String  url,
                                      @Part MultipartBody.Part file, @Part("document_type_id") RequestBody document_type_id,
                                      @Part("document_name") RequestBody document_name);

    @GET
    Call<LeadDocTypeSpinnerResponse> getLeadDocSpinner(@Url String url);



    @Multipart
    @POST()
    Call<FcmResponse> addPostCustomer(@Url String  url,
                                      //@Part MultipartBody.Part file,
                                      @Part("customer_code") RequestBody customer_code,
                                      @Part("salutation") RequestBody salutation,
                                      @Part("name") RequestBody name,
                                      @Part("customers_type_id") RequestBody customers_type_id,
                                      @Part("email") RequestBody email,
                                      @Part("mobile") RequestBody mobile,
                                      @Part("landline") RequestBody landline,
                                      @Part("state_id") RequestBody state_id,
                                      @Part("city_id") RequestBody city_id,
                                      @Part("area") RequestBody area,
                                      @Part("street") RequestBody street,
                                      @Part("zip_code") RequestBody zip_code,
                                      @Part("landmark") RequestBody landmark,
                                      @Part("is_active") RequestBody is_active,
                                      @Part("email_opt") RequestBody email_opt,
                                      @Part("sms_opt") RequestBody sms_opt);
    @FormUrlEncoded
    @POST()
    Call<FcmResponse> addPostContactPerson(@Url String url,
                                           @Field("role_id") String role_id,
                                           @Field("salutation") String salutation,
                                           @Field("name") String name,
                                           @Field("mobile") String mobile,
                                           @Field("email") String email,
                                           @Field("email_opt") String email_opt,
                                           @Field("sms_opt") String sms_opt,
                                           @Field("is_primary") String is_primary);

    @FormUrlEncoded
    @POST()
    Call<AddGstDetailResponse> addGstDetails(@Url String url,
                                             @Field("gst_no") String gst_no,
                                             @Field("gst_sales_type_id") String gst_sales_type_id,
                                             @Field("pan_no") String pan_no);


    @FormUrlEncoded
    @POST()
    Call<FcmResponse> addBankDetails(@Url String url,
                                           @Field("bank_name") String bank_name,
                                           @Field("bank_acc_no") String bank_acc_no,
                                           @Field("bank_branch") String bank_branch,
                                           @Field("ifsc_code") String ifsc_code);

//    @FormUrlEncoded
//    @POST()
//    Call<FcmResponse> addDocument(@Url String url,
//                                  @Part MultipartBody.Part file,
//                                  @Part("document_name") RequestBody document_name);

    @Multipart
    @POST()
    Call<FcmResponse> addDocument(@Url String  url,
                                      @Part MultipartBody.Part file,
                                      @Part("document_name") RequestBody document_name);

    @GET
    Call<GetCustomerResponse> getCustomers(@Url String url);

    @FormUrlEncoded
    @POST()
    Call<AddLeadProductResponse> addLeadProduct(@Url String url,
                                                @Field("id") String id,
                                                @Field("products_id") String products_id,
                                                @Field("rate") String rate,
                                                @Field("quantity") String quantity,
                                                @Field("box") String box);

    @FormUrlEncoded
    @POST()
    Call<TaskInstallationResponse> AddInstallationTask(@Url String url,
                                                       @Field("layer_name") String layer_name,
                                                       @Field("material_details") String material_details,
                                                       @Field("installation_status") String installation_status,
                                                       @Field("installation_remark") String installation_remark);

    @FormUrlEncoded
    @POST()
    Call<TaskDeliveryResponse> AddDeliveryTask(@Url String url,
                                               @Field("auto_person") String auto_person,
                                               @Field("tansport_name") String tansport_name,
                                               @Field("truck_no") String truck_no,
                                               @Field("lr_no") String lr_no,
                                               @Field("mode") String mode,
                                               @Field("delivery_status") String delivery_status,
                                               @Field("delivery_remark") String delivery_remark);

    @FormUrlEncoded
    @POST()
    Call<TaskOtherResponse> AddOtherTask(@Url String url,@Field("remark") String remark);


    @GET()
    Call<FcmResponse> deleteLeadProduct(@Url String url);


    @POST()
    Call<FcmResponse> markUnread(@Url String url);
}

