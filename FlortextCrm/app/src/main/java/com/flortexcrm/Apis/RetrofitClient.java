package com.flortexcrm.Apis;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    // LIVE URL
    public static final String BASE_URL = "https://flortex.in/crm/api/v1/";
    public static final String BASE_IMAGE_URL = "https://flortex.in/crm/";


    // TESTING URL
   /* public static final String BASE_URL = "http://flortex.in/testing/api/v1/";
    public static final String BASE_IMAGE_URL = "http://flortex.in/testing/";
*/

    public static RetrofitClient mInstance;
    private static Retrofit retrofit;

    private RetrofitClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request = chain.request().newBuilder()
                                        .addHeader("Accept", "Application/JSON")
                                        .build();
                                return chain.proceed(request);
                            }
                        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public static Retrofit retrofit() {
        return retrofit();
    }


    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
