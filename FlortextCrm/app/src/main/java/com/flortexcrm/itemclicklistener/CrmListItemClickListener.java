package com.flortexcrm.itemclicklistener;

import android.view.View;

public interface CrmListItemClickListener
{
    void onItemClick(View v, int pos);
}
