package com.flortexcrm.Service;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.flortexcrm.Apis.RetrofitClient;
import com.flortexcrm.Reciver.AlarmReceiver;
import com.flortexcrm.Response.TaskResponse;
import com.flortexcrm.utility.Connectivity;
import com.flortexcrm.utility.PrefUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBackgroundService extends Service {
    String userId;
    ArrayList<String> dueDateArray = new ArrayList<>();

    public MyBackgroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        Toast.makeText(this, "Invoke background service onCreate method.", Toast.LENGTH_LONG).show();
        ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

        /*This schedules a runnable task every second*/
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {

              // getTaskData();
            }
        }, 0, 2, TimeUnit.SECONDS);

        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Invoke background service onStartCommand method.", Toast.LENGTH_LONG).show();



        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Invoke background service onDestroy method.", Toast.LENGTH_LONG).show();
    }

    private void getTaskData() {
        userId = PrefUtils.getUserId(this);

        if (Connectivity.isConnected(this)) {
//            Dialog.showProgressBar(MainActivity.this, "Loading Tasks...");
            Call<TaskResponse> call = RetrofitClient.getInstance().getApi().getAllTaskList(RetrofitClient.BASE_URL + "users/" + userId + "/tasks");
            call.enqueue(new Callback<TaskResponse>() {
                @Override
                public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                    // progress.setVisibility(View.GONE);
                    // Dialog.hideProgressBar();
                    TaskResponse productListResponse = response.body();

                    if (response.isSuccessful()) {

                        //  List<TaskResponse.DataBean.AssignedToEmployeesBean> docsBeanList = response.body() != null ? response.body().getData(): null;

                        if ((productListResponse.getStatus().equals("10100"))) {

                            for (TaskResponse.DataBean address : productListResponse.getData()) {

                                for (TaskResponse.DataBean.AssignedToEmployeesBean assignedToEmployeesBean : address.getAssignedToEmployees()) {

                                    dueDateArray.add(assignedToEmployeesBean.getDueDate());

                                }
                            }

                            dateComparisions();


                        }

                    }
                }

                @Override
                public void onFailure(Call<TaskResponse> call, Throwable t) {

                }
            });
        }

    }


    private void dateComparisions() {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2019-03-21 20:39:52
        String currentDateandTime = sdf.format(new Date());

        try {
            Date currentdate = sdf.parse(currentDateandTime);//2019-03-21 14:52:00

            System.out.println(currentdate + "----");

            boolean exist = false;
            int i = 0;
            for (String duedate : dueDateArray) {

                Date futuredate = sdf.parse(duedate);
                System.out.println(futuredate + "FUTURE");

                if (currentdate.compareTo(futuredate) == 0) {

                    //  0 comes when two date are same,
                    //  1 comes when date1 is higher then date2
                    // -1 comes when date1 is lower then date2

                    Log.e("DATE", "" + "TRUE");

                    alertSheduledNotification(futuredate);

                    exist = true;
                    break;

                } else {

                    Log.e("DATEFALSE", "" + "False");
                }

                i++;
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void alertSheduledNotification(Date dueDate) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dueDate);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

       // Log.e("DATEFORMATS", "" + hours + minutes + seconds + year + month + day);


        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

       /* Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, hours);
        cal.add(Calendar.MINUTE, minutes);
        cal.add(Calendar.SECOND, 30);
        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.DAY_OF_MONTH, day);
        System.out.print(cal.getTimeInMillis());*/


        calendar.setTime(dueDate);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);


    }
}
